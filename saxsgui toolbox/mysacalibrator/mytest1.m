% reads data and tryies to fit the parameters
% parx contains fixed parameters or zeroes
% mask contains zeros on fixed and ones on free positions
% qe ... q values; il ... intensities to fit; it ... intensities calculated
% par(1)=X0, par(2)=Y0, par(3)=RX, par(4)=RY
global qe il it %% ee 
global mask parx
xyz=load('testsph2.dat');
qe=xyz(:,1);
il=xyz(:,2);
x1=min(qe);
x2=max(qe);
y1=min(il);
y2=max(il);
x0=(x1+x2)/2;
rx0=(x2-x1)/2;
y0=(y1+y2)/2;
ry0=(y2-y1)/2;
ipar=[x0 y0 rx0 ry0];
%ipar=[500 520 160 160];
%mssph03(ipar);
fgr=figure;
%plot(qe,il,'b');
plot(qe,il,'bo');
hold on
%plot(qe,it,'m');
%plot(qe,it,'mo');
parx=[0 0 0 0];
mask=[1 1 1 1];
opar=msleastsq('mssph03',ipar);
opar
%mssph03(opar);
%plot(qe,it,'r');
mscircle1(opar);