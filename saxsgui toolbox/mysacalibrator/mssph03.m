% mssph03 calculates a sphere in a plane with 3 parameters
% parameters can be fixed using globals mask and parx
% parx contains fixed parameters or zeroes
% mask contains zeros on fixed and ones on free positions
% qe ... q values; il ... intensities to fit; it ... intensities calculated
% par(1)=X0, par(2)=Y0, par(3)=RX, par(4)=RY
function delta=mssph03(par)
%%global alam imx qe il it ql yl ee 
global qe il it %% ee 
global mask parx
par=par.*mask+parx;
x0=par(1); y0=par(2); rx=par(3); ry=par(4);
dXY=[qe-x0, il-y0];
T=[1, 0; 0, 1];
dXY = dXY*T;
dx=dXY(:,1)/rx;
dy=dXY(:,2)/ry;
delta= dx.*dx + dy.*dy -1;
%err=delta'*delta;
