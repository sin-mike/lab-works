% This is a hack to SAXSGUI.
% Milos Steinhart, Milky Way 13, 28. 06. 2007
% It should be started, after data are loaded and displayed
% A hack inserted into SAXSGUI exports data in global sex
% here we want to perform semiautomatically both SAXS calibrations
% Be careful !!! 
% Image displays arrays so that the first, line-index is vertical

% Load the data 
global sex
global qe il
global mask parx
global opar
olda=gca;
pixels=sex.pixels;              % direct assignment
opixels=pixels;                 % save the original value
myfgr=figure;
ilda=image(pixels);
set(get(ilda,'parent'),'ydir','normal'); axis square;

% Eliminate the inside usually primary beam
title('Eliminate the primary beam (LD right button)');
but=1;
while but==1
  [cx,cy,but]=ginput(1); cx=round(cx); cy=round(cy);
end   
cxld=cx; cyld=cy
title('Eliminate the primary beam (RU right button)');
but=1;
while but==1
  [cx,cy,but]=ginput(1); cx=round(cx); cy=round(cy);
end   
cxru=cx; cyru=cy;
pixels(cyld:cyru,cxld:cxru)=0; % !switch indices due to image properties! 

% Delimit the desired calibration circle 
ilda=image(pixels);
set(get(ilda,'parent'),'ydir','normal'); axis square;
title('Delimit the calibration ring (LD right button)');
but=1;
while but==1
  [cx,cy,but]=ginput(1); cx=round(cx); cy=round(cy);
end   
cxld=cx; cyld=cy;
title('Delimit the calibration ring (RU right button)');
but=1;
while but==1
  [cx,cy,but]=ginput(1); cx=round(cx); cy=round(cy);
end   
cxru=cx; cyru=cy;
[nrw ncl]=size(pixels);
msk=zeros(nrw,ncl);
msk(cyld:cyru,cxld:cxru)=1;
pixels=pixels.*msk;
ilda=image(pixels);
set(get(ilda,'parent'),'ydir','normal'); axis square;

% select some intensity interval should be done interactively !?
if 1<2
hitre=pixels<100;
lotre=pixels>50;
patre=hitre.*lotre;
pixels=pixels.*patre;
end
ilda=image(pixels);
set(get(ilda,'parent'),'ydir','normal');
[qe,il]=find(pixels);
%prd=[qe il];
mytest2
ilda=image(opixels);
set(get(ilda,'parent'),'ydir','normal');
mscircle3(opar);
axis square
%axes=olda;
%mscircle1(opar);