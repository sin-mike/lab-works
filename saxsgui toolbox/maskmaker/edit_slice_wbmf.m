function edit_circle_wbmf(nodummy)
% --- Executes on button mousemove in editing mode. Updating continuously
global xtemp ytemp jj
if nargin==1 % this is just for touching the file (for executable)
    return
end

point1 = get(gca,'CurrentPoint');
xrange=get(gca,'XLim');
yrange=get(gca,'YLim');
%original design
cen = [xtemp(1),ytemp(1)];
firstp = [xtemp(2),ytemp(2)];
lastp= [xtemp(end-1),ytemp(end-1)];
radius=sqrt(sum((firstp(1:2)-cen(1:2)).^2));
a1=atan2(firstp(2)-cen(2),firstp(1)-cen(1)); % angle of 1st edge of slice
a2=atan2(lastp(2)-cen(2),lastp(1)-cen(1)); % angle of last (2nd) edge of slice
% atan2 generates results from -pi to +pi with 0 being horizontal, to the
% right...if one wants to find the angle between the two one has to jump
% through some hoops

if (a1<=a2)
    a=a2-a1;
else
    a=2*pi+(a2-a1);
end

if jj(1)==1 || jj(1)==max(size(xtemp))% the center was clicked
    xtemp1=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemp1=min(yrange(2),max(yrange(1),point1(1,2)));
    xdiff=xtemp1-xtemp(1);
    ydiff=ytemp1-ytemp(1);
    cen = [xtemp1,ytemp1];           % calculate locations
    firstp= [xtemp(2)+xdiff,ytemp(2)+ydiff];
    lastp= [xtemp(end-1)+xdiff,ytemp(end-1)+ydiff];
    radius=sqrt(sum((firstp(1:2)-cen(1:2)).^2));
    a1=atan2(firstp(2)-cen(2),firstp(1)-cen(1)); % angle of 1st edge of slice
    a2=atan2(lastp(2)-cen(2),lastp(1)-cen(1)); % angle of last (2nd) edge of slice
elseif jj(1)==2  % the first point of the arc was clicked 
    xtemp2=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemp2=min(yrange(2),max(yrange(1),point1(1,2)));
    cen=[xtemp(1),ytemp(1)];           % calculate locations
    firstp=[xtemp2,ytemp2];
    lastp=[xtemp(end-1),ytemp(end-1)];
    radius=sqrt(sum((firstp(1:2)-cen(1:2)).^2));
    a1=atan2(firstp(2)-cen(2),firstp(1)-cen(1));
    a2=a2;
elseif jj(1)==max(size(xtemp))-1
    xtemp(end-1)=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemp(end-1)=min(yrange(2),max(yrange(1),point1(1,2)));
    cen = [xtemp(1),ytemp(1)];% calculate locations
    firstp= [xtemp(2),ytemp(2)];
    lastp=[xtemp(end-1),ytemp(end-1)];
    radius=sqrt(sum((lastp(1:2)-cen(1:2)).^2));
    a1=a1; % angle of 1st edge of slice
    a2=atan2(lastp(2)-cen(2),lastp(1)-cen(1)); % angle of last (2nd) edge of slice
else
    xtemptemp=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemptemp=min(yrange(2),max(yrange(1),point1(1,2)));
    cen = [xtemp(1),ytemp(1)];% calculate locations
    radius=sqrt(sum(([xtemptemp,ytemptemp]-cen(1:2)).^2));
    a1=a1; % angle of 1st edge of slice
    a2=a2; % angle of last (2nd) edge of slice
end
if (a1<=a2)
    a=a2-a1;
else
    a=2*pi+(a2-a1);
end

ang=[a1:a/180:a1+a];
xtemp=[cen(1),cen(1)+radius*cos(ang),cen(1)];
ytemp=[cen(2),cen(2)+radius*sin(ang),cen(2)];
