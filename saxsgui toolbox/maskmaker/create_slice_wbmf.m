function create_slice_wbmf(nodummy)
global point
if nargin==1 % this is just for touching the file (for executable)
    return
end
% this routine builds the slice during creation

c=get(gca,'Children');
delete(c(1)); % this deletes the old figure
temppoint=[point;get(gca,'CurrentPoint')]; %this is present location of the cursor
cen = temppoint(1,1:2);              % calculate locations
firstp=temppoint(2,1:2);
if size(point(:,1))==1 %if only 1 point has been entered so far then draw a line
    lastp=firstp;
else
    lastp=temppoint(3,1:2);
end


radius=sqrt(sum((firstp(1:2)-cen(1:2)).^2));
a1=atan2(firstp(2)-cen(2),firstp(1)-cen(1)); % angle of 1st edge of slice
a2=atan2(lastp(2)-cen(2),lastp(1)-cen(1)); % angle of last (2nd) edge of slice
% atan2 generates results from -pi to +pi with 0 being horizontal, to the
% right...if one wants to find the angle between the two one has to jump
% through some hoops

if (a1<=a2)
    a=a2-a1;
else
    a=2*pi+(a2-a1);
end

ang=[a1:a/180:a1+a]; %draw 181 points on the arc
xvalues=[cen(1),cen(1)+radius*cos(ang),cen(1)]; % add the center position at the end to make it a true slice
yvalues=[cen(2),cen(2)+radius*sin(ang),cen(2)];
h=plot(xvalues,yvalues,'r-','linewidth',2); 

