function varargout = maskmakergui(varargin)
% MASKMAKERGUI M-file for maskmakergui.fig
%      MASKMAKERGUI, by itself, creates a new MASKMAKERGUI or raises the existing
%      singleton*.
%
%      H = MASKMAKERGUI returns the handle to a new MASKMAKERGUI or the handle to
%      the existing singleton*.
%
%      MASKMAKERGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MASKMAKERGUI.M with the given input arguments.
%
%      MASKMAKERGUI('Property','Value',...) creates a new MASKMAKERGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before maskmakergui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to maskmakergui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help maskmakergui

% Modified by GUIDE v2.5 01-Dec-2008 23:18:46
% Modified by KJ 01-Dec-2008 in order to include zoom function buttons.
% Note: A RONI must be created in a non-zoomed state, but can be
% manipulated in a zoomed state

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @maskmakergui_OpeningFcn, ...
    'gui_OutputFcn',  @maskmakergui_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before maskmakergui is made visible.
function maskmakergui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to maskmakergui (see VARARGIN)

% Choose default command line output for maskmakergui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using maskmakergui.
if strcmp(get(hObject,'Visible'),'off')
    if  ~isempty(varargin)
        imagesc(1:length(varargin{1}(1,:)),1:length(varargin{1}(:,1)),varargin{1},'Parent',handles.imageaxis)
        set(handles.imageaxis,'UserData',varargin(1))
        set(handles.imageaxis,'YDir','normal');
        set(handles.slider,'Max',max(max(varargin{1}))) % set slider value
        set(handles.slider,'Min',1) % set slider value
        set(handles.slider,'String',max(max(varargin{1})))
        set(handles.slider,'Value',max(max(varargin{1})))
        set(handles.slidertopvalue,'String',max(max(varargin{1})))
        set(handles.slidertopvalue,'Value',max(max(varargin{1})))
    end
end
ConstructROIstructure(10) % making space for up to 10 ROI's
updateROIgui(handles)

% UIWAIT makes maskmakergui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%*******************************************
% --- Executes on button press in deletemask.
function deletemask_Callback(hObject, eventdata, handles)
% hObject    handle to deletemask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(findobj(gcf,'Type','uicontrol'),'Enable','On') % this makes all controls inactive
set(handles.createmask,'UserData',[])
img=get(handles.imageaxis,'UserData');
imagesc(img{1})
set(handles.imageaxis,'UserData',img);
set(handles.imageaxis,'YDir','normal');
top=max(max(img{1}));
set(handles.slidertopvalue, 'String',num2str(top));
set(handles.slider, 'Value', top)
set(handles.imageaxis, 'CLim', [0 top])
updateROIlines(handles)

%*******************************************
% --- Executes on button press in createmask.
function createmask_Callback(hObject, eventdata, handles)
% hObject    handle to deletemask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% please keep in mind that outside=1 means that the outside is good..i.e.
% exclude the interior.
%loop through all ROI that are created and that are activated
global ROIstruct

statusline=status('Calculating MASK. Please be patient',get(hObject,'Parent'));
imgin=get(handles.imageaxis,'UserData');
imgin=imgin{1}*0+1;
imgout=imgin;
for ii=1:length(ROIstruct) % stepping thru all the possible regions of interest
    imgtemp=imgin;
    if ROIstruct(ii).created==1 && ROIstruct(ii).active==1 %if created and active
        typeofROI=ROIRosettaStone(ROIstruct(ii).type);
        switch typeofROI  %executes different relevant functions that create the masks.
            case {'Rectangle','Polygon'}
                imgtemp=CreateOneMaskFromLine(imgin,ROIstruct(ii).values); %this find the interior of a polygon
            case 'Circle'
                imgtemp=CreateOneMaskFromCircle(imgin,ROIstruct(ii).values);
            case 'Slice'
                imgtemp=CreateOneMaskFromSlice(imgin,ROIstruct(ii).values);
        end
        if isempty(imgtemp)
            delete(statusline)
        end
        if ROIstruct(ii).outside
            imggood=imgtemp;
        else
            imggood=~imgtemp;
        end
        imgout=imgout.*imggood; % combining the individual masks. Mask consists of 0s and 1s.
        delete(statusline)
        eval(['statusline=status(''Calculating MASK..RONI(',num2str(ii),'). Please be patient'',get(hObject,''Parent''));'])
    end
end
imgout=(imgout>=1);
set(handles.createmask,'UserData',imgout) %saving the mask on the "create mask" button
img=get(handles.imageaxis,'UserData');
warning off MATLAB:divideByZero
imagesc((img{1}.*imgout)./imgout) % here the mask in changed to 1's and NAN's which is necessary.
warning on MATLAB:divideByZero
set(handles.imageaxis,'UserData',img);
set(handles.imageaxis,'YDir','normal');
set(handles.slidertopvalue, 'String','1');
set(handles.slider, 'Value', 1)
set(handles.imageaxis, 'CLim', [0 1])
delete(statusline)
set(findobj(gcf,'Type','uicontrol'),'Enable','off') % this makes all controls inactive
set(findobj(gcf,'Style','Text'),'Enable','on') % this turns the text back on
set(handles.deletemask,'Enable','on') % this sets the "deletemask" button
set(handles.savemask,'Enable','on')




%*******************************************
% --- Executes on button press in loadmask.
function savemask_Callback(hObject, eventdata, handles)
% hObject    handle to deletemask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%saves a created mask in a file
set(findobj(gcf,'Type','uicontrol'),'Enable','On') % this makes all controls inactive
mask=get(handles.createmask,'UserData');
[filename, pathname, filterindex] = uiputfile('mask*.mat', 'Save MASK-file');
if isequal(filename,0) || isequal(pathname,0)
    errordlg('No mask saved')
    return
else
    save(fullfile(pathname, filename),'mask')
end
updateROIlines(handles)

%*******************************************
% --- Executes on button press in saverois.
function saverois_Callback(hObject, eventdata, handles)
global ROIstruct
% hObject    handle to deletemask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% saves the full set of Region of Interest (ROIstruct) in a file
[filename, pathname, filterindex] = uiputfile('RONI*.mat', 'Save RONI-file');
if isequal(filename,0) || isequal(pathname,0)
    errordlg('No RONIs saved')
    return
else
    save(fullfile(pathname, filename),'ROIstruct')
end

%*******************************************
% --- Executes on button press in saverois.
function loadrois_Callback(hObject, eventdata, handles)
global ROIstruct
% hObject    handle to deletemask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%loads an already saves Region of Interests file
[filename, pathname, filterindex] = uigetfile('*.mat', 'Pick an RONI-file');
if isequal(filename,0) || isequal(pathname,0)
    errordlg('No RONIs loaded')
    return
else
    load(fullfile(pathname, filename))
    if ~exist('ROIstruct')
        errordlg('The chosen file does not contain RONIs from MaskMaker')
        return
    end
    %should also put in an error check on the size of the image. Later
    updateROIgui(handles)
    updateROIlines(handles)
end

% --- Executes on button press in CreateROI.
function CreateROI_Callback(hObject, eventdata, handles, ROInumber)
global ROIstruct
global point
% hObject    handle to CreateROI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%This function was originally intended to only create a ROI..now it can
%also be used to delete a region - in a toggle-fashion

zoom(gcf,'off')
createroihandle=getcreateroihandle(handles,ROInumber); % this is a fix for an early naming error in the GUI.

if strcmp(get(createroihandle,'String'),'Delete')
    manipulateROI(ROInumber,'delete')
    updateROIgui(handles)
else
    %first clearing the old one
    manipulateROI(ROInumber,'deactivate')
    updateROIgui(handles)
    updateROIlines(handles)
    
    %then begin for real
    
    manipulateROI(ROInumber,'create')
    manipulateROI(ROInumber,'activate')
    typeofROI=eval(['get(handles.typeroi',num2str(ROInumber),',''Value'')']); % this returns a number
    typeofROI=ROIRosettaStone(typeofROI); % this returns the text
    manipulateROI(ROInumber,typeofROI)
    
    updateROIgui(handles)
    
    switch typeofROI
        case 'Rectangle'
            statusline=status('Draw the rectangle, by clicking and dragging on the image'...
                ,get(hObject,'Parent'));
            kk = waitforbuttonpress;
            point1 = get(gca,'CurrentPoint');    % button down detected
            finalRect = rbbox;                   % return figure units
            point2 = get(gca,'CurrentPoint');    % button up detected
            point1 = point1(1,1:2);              % extract x and y
            point2 = point2(1,1:2);
            p1 = min(point1,point2);             % calculate locations
            offset = abs(point1-point2);         % and dimensions
            x = [p1(1) p1(1)+offset(1) p1(1)+offset(1) p1(1) p1(1)];
            y = [p1(2) p1(2) p1(2)+offset(2) p1(2)+offset(2) p1(2)];
            
        case 'Circle'
            
            statusline=status('Click on: Center of circle',get(hObject,'Parent'));
            kk = waitforbuttonpress;
            point = get(gca,'CurrentPoint');    % button down detected
            point=point(1:end-1,:);
            delete(statusline)
            hold on
            h=plot(point(:,1),point(:,2),'r-','linewidth',2);
            
            set(gcf,'Renderer','zbuffer')
            set(gcf,'DoubleBuffer','on') % this aids with drawings
            set(gcf,'KeyPressFcn','kk=1;') %probably not necessary...but kept just in case
            set(gcf,'WindowButtonDownFcn','')
            set(gcf,'WindowButtonMotionFcn','create_circle_wbmf')
            
            statusline=status('Click on: Edge of circle',get(hObject,'Parent'));
            pause(0.5) %needed to let the program
            kk=waitforbuttonpress;
            point=[point;get(gca,'CurrentPoint')];
            point=point(1:end-1,:);
            set(gcf,'WindowButtonDownFcn','')
            set(gcf,'WindowButtonMotionFcn','')
            set(gcf,'KeyPressFcn','')
            set(gcf,'DoubleBuffer','off')
            hold off
            x = point(1,1:2);              % here x=center and y=radius
            y = x*0+sqrt(sum((point(2,1:2)-point(1,1:2)).^2));
            
        case 'Polygon'
            statusline=status('Click on: Each corner of the CONVEX polygon. Press keyboard to finish.',get(hObject,'Parent'));
            
            kk=waitforbuttonpress;
            point=get(gca,'CurrentPoint');
            point=point(1:end-1,:);
            hold on
            h=plot(point(:,1),point(:,2),'r-','linewidth',2);
            set(gcf,'Renderer','zbuffer') % this aids with the speed of the plotting
            set(gcf,'DoubleBuffer','on') % this aids with drawings
            set(gcf,'KeyPressFcn','kk=1;') %probably not necessary...but kept just in case
            
            %set(gcf,'WindowButtonDownFcn','create_poly_wbdf')
            set(gcf,'WindowButtonMotionFcn','create_poly_wbmf')

            while kk==0
                
                kk=waitforbuttonpress;
                % this was included on March 16 since there was trouble
                % with the WindowButtonDownFcn being called. So now we call
                % it explicitly if there is a left-mouse click i.e. normal
                if kk==0 && strcmp(get(gcf,'Selectiontype'),'normal')
                    create_poly_wbdf
                end
                if (kk==1)
                    point(end+1,:)=point(1,:);
                end
                hold on
                h=plot(point(:,1),point(:,2),'r:');
            end
            set(gcf,'WindowButtonDownFcn','')
            set(gcf,'WindowButtonMotionFcn','')
            set(gcf,'KeyPressFcn','')
            set(gcf,'DoubleBuffer','off')
            hold off
            
            x = point(:,1)';
            y = point(:,2)';
            
        case 'Slice'
            
            %entering first points
            statusline=status('Click on: Center of Slice',get(hObject,'Parent'));
            kk = waitforbuttonpress;
            point = get(gca,'CurrentPoint');    % button down detected
            point=point(1:end-1,:);
            delete(statusline)
            hold on
            h=plot(point(:,1),point(:,2),'r-','linewidth',2);
            
            %making sure that a line is drawn as the mouse moves around
            set(gcf,'Renderer','zbuffer')
            set(gcf,'DoubleBuffer','on') % this aids with drawings
            set(gcf,'KeyPressFcn','kk=1;') %probably not necessary...but kept just in case
            set(gcf,'WindowButtonDownFcn','')
            set(gcf,'WindowButtonMotionFcn','create_slice_wbmf')
            
            %entering second point
            statusline=status('Click on: 1st corner of Slice',get(hObject,'Parent'));
            kk = waitforbuttonpress; % button down detected
            point=[point;get(gca,'CurrentPoint')];
            point=point(1:end-1,:);
            delete(statusline)
            hold on
            % plot line to 1st point and make
            create_slice_wbmf;
            
            % entering third and last point
            statusline=status('Click on: 2nd corner of circle',get(hObject,'Parent'));
            
            kk=waitforbuttonpress;
            point=[point;get(gca,'CurrentPoint')];
            point=point(1:end-1,:);
            %adding first point at the end to ensure it is closed
            point=[point;point(1,:)];
            set(gcf,'WindowButtonDownFcn','')
            set(gcf,'WindowButtonMotionFcn','')
            set(gcf,'KeyPressFcn','')
            set(gcf,'DoubleBuffer','off')
            hold off
            
            x = point(:,1)';
            y = point(:,2)';
    end
    delete(statusline)
    AddROIvalues(ROInumber,x,y)
end
updateROIlines(handles)

function create_poly_wbdf
global point
zoom(gcf,'off')
point=[point;get(gca,'CurrentPoint')];
% for some reason the get(gca,'CurrentPoint') adds 2
% lines...I only need one...so hence the
% point=point(1:end-1,:) is needed
point=point(1:end-1,:);

function create_poly_wbmf
global point
% this routine updates the figure during creating
c=get(gca,'Children');
delete(c(1));
temppoint=[point;get(gca,'CurrentPoint')];
h=plot(temppoint(1:end-1,1),temppoint(1:end-1,2),'r-','linewidth',2);



% --- Outputs from this function are returned to the command line.
function varargout = maskmakergui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in active1.
function active1_Callback(hObject, eventdata, handles,ROInumber)
% hObject    handle to active1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of active1

if get(hObject,'Value')==0
    manipulateROI(ROInumber,'deactivate') %toggles activate button (on)
else
    manipulateROI(ROInumber,'activate') %toggles activate button (off)
end
updateROIgui(handles)
updateROIlines(handles)

% --- Executes during object creation, after setting all properties.
function typeroi1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to typeroi1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on selection change in typeroi1.
function typeroi1_Callback(hObject, eventdata, handles,ROInumber)
global ROIstruct
% hObject    handle to typeroi1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns typeroi1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from typeroi1


chosentype=get(hObject,'Value'); %this is a number indicating which type of ROI has been chosen

createroihandle=getcreateroihandle(handles,ROInumber);
if strcmp(get(createroihandle,'String'),'Delete') % this means there is already a ROI defined
    %changing the type of ROI deletes the present ROI.so we ask the user to
    %confirm
    answer=questdlg('Changing the Type of ROI will erase the present ROI. Continue?','Erase ROI warning');
    if strcmp(answer,'Yes')
        %deleting old
        manipulateROI(ROInumber,'delete')
        % and finding the new type
        chosentype=get(hObject,'Value'); %this is a number
        
    else
        %finding the old value
        chosentype=ROIstruct(ROInumber).type;
    end
end
typeofROI=ROIRosettaStone(chosentype); % this is a text string
manipulateROI(ROInumber,typeofROI)
updateROIgui(handles)
updateROIlines(handles)

% --- Executes on button press in outside1.
function outside1_Callback(hObject, eventdata, handles,ROInumber)
% hObject    handle to outside1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of outside1

%accept input if created
if get(hObject,'Value')==1
    manipulateROI(ROInumber,'excludeinterior')
else
    manipulateROI(ROInumber,'excludeexterior')
end
updateROIgui(handles)
updateROIlines(handles)

% --- Executes on button press in editroi1.
function editroi1_Callback(hObject, eventdata, handles,ROInumber)
global ROIstruct
persistent statusline
% hObject    handle to dragroi1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ROIstruct(ROInumber).editing==1 %the editing has been activated before
    set(findobj(gcf,'Type','uicontrol'),'Enable','On') % this makes all controls active again
    typeofROI=ROIRosettaStone(ROIstruct(ROInumber).type); % this returns the text
    switch typeofROI   %and now to get the new data
        case 'Rectangle'
            x=get(ROIstruct(ROInumber).handle,'Xdata');
            y=get(ROIstruct(ROInumber).handle,'Ydata');
        case 'Circle'
            px=get(ROIstruct(ROInumber).handle,'Xdata');
            py=get(ROIstruct(ROInumber).handle,'Ydata');
            x = [px(1) py(1)];             % here x=center and y=radius
            y =  x*0+sqrt(sum((px(2)-px(1)).^2+(py(2)-py(1)).^2));
        case 'Polygon'
            x=get(ROIstruct(ROInumber).handle,'Xdata');
            y=get(ROIstruct(ROInumber).handle,'Ydata');
        case 'Slice'
            px=get(ROIstruct(ROInumber).handle,'Xdata');
            py=get(ROIstruct(ROInumber).handle,'Ydata');
            x = [px(1) px(2) px(end-1)];             % here the coordinates of the first, second and last
            y = [py(1) py(2) py(end-1)];
    end
    AddROIvalues(ROInumber,x,y) %and now save new data to ROIstruct
    manipulateROI(ROInumber,'uneditable')
    updateROIgui(handles)
    updateROIlines(handles)
    statusline=findobj(get(gcf,'Children'),'Tag','statusline');
    delete(statusline)
else
    % start editable state
    %only do if it is created
    if ROIstruct(ROInumber).created==1 % this means there is a ROI defined
        %first clearing the old one
        manipulateROI(ROInumber,'editable')
        updateROIgui(handles)
        set(findobj(gcf,'Type','uicontrol'),'Enable','off') % this makes all controls inactive
        set(findobj(gcf,'Style','Text'),'Enable','on') % this turns the text back on
        set(hObject,'Enable','on') % this sets the "edit" button
        editingROI(hObject, eventdata, handles,ROInumber)
    else
        warndlg('ROI not defined!')
    end
end


function editingROI(hObject, eventdata, handles,ROInumber)
global ROIstruct x y xtemp ytemp jj kk

% this funciton edits the individual points of a ROI
% it presently supports editing of rectangles,polygons,circle and splices.

set(ROIstruct(ROInumber).handle,'Selected','on')
statusline=status('Click on the point you want to move. ',get(hObject,'Parent'));
typeofROI=ROIRosettaStone(ROIstruct(ROInumber).type); % this returns the text
zoom(gcf,'off')
while ROIstruct(ROInumber).editing==1;
    switch typeofROI   %and now to get the new data
        case {'Rectangle', 'Polygon','Circle','Slice'}
            x=get(ROIstruct(ROInumber).handle,'Xdata');
            y=get(ROIstruct(ROInumber).handle,'Ydata');
            xtemp=x;
            ytemp=y;
            kk=waitforbuttonpress; % button down detected
            pause(0.5)     %this pause allows pressing the apply button to be registered.
            if ROIstruct(ROInumber).editing==0
                x=ROIstruct(ROInumber).values(1,:);
                y=ROIstruct(ROInumber).values(2,:);
                break
            end
            
            point1 = get(gca,'CurrentPoint');
            point1 = point1(1,1:2);
            %find closest point to click
            
            dist=sqrt((point1(1)-x).^2+(point1(2)-y).^2);
            jj=find(dist==min(dist));
            if (max(size(xtemp))==2)
                jj=jj(1)
            end
            xtemp(jj)=point1(1);
            ytemp(jj)=point1(2);
            set(gcf,'Renderer','zbuffer') % this aids with the speed of the plotting
            set(gcf,'DoubleBuffer','on') % this aids with drawings
            switch typeofROI
                case 'Rectangle'
                    set(gcf,'WindowButtonMotionFcn','edit_rect_wbmf') % adjusts the figure during editing
                    set(gcf,'WindowButtonDownFcn','edit_rect_wbdf') %controls what happens when no longer editing
                case 'Polygon'
                    set(gcf,'WindowButtonMotionFcn','edit_poly_wbmf') % adjusts the figure during editing
                    set(gcf,'WindowButtonDownFcn','edit_poly_wbdf') %controls what happens when no longer editing
                case 'Circle'
                    set(gcf,'WindowButtonMotionFcn','edit_circle_wbmf') % adjusts the figure during editing
                    set(gcf,'WindowButtonDownFcn','edit_circle_wbdf') %controls what happens when no longer editing
                case 'Slice'
                    set(gcf,'WindowButtonMotionFcn','edit_slice_wbmf') % adjusts the figure during editing
                    set(gcf,'WindowButtonDownFcn','edit_slice_wbdf') %controls what happens when no longer editing
            end
            delete(statusline)
            statusline=status('Click on the new position ',get(hObject,'Parent'));
            
            kk=-1;
            while kk<0
                hold on
                set(ROIstruct(ROInumber).handle,'Xdata',xtemp);
                set(ROIstruct(ROInumber).handle,'Ydata',ytemp);
                drawnow
            end
            set(ROIstruct(ROInumber).handle,'Xdata',x);
            set(ROIstruct(ROInumber).handle,'Ydata',y);
            drawnow
    end
    set(gcf,'WindowButtonMotionFcn','')
    set(gcf,'DoubleBuffer','off')
    set(gcf,'WindowButtonDownFcn','')
    hold off
    statusline=findobj(get(gcf,'Children'),'Tag','statusline');
    delete(statusline)
    statusline=status('Choose a point to edit or press "Apply" ',get(hObject,'Parent'));
    
end
set(gcf,'WindowButtonMotionFcn','')
set(gcf,'DoubleBuffer','off')
set(gcf,'WindowButtonDownFcn','')
hold off
statusline=findobj(get(gcf,'Children'),'Tag','statusline');
delete(statusline)
AddROIvalues(ROInumber,x,y) %and now save new data to ROIstruct

% --- Executes on button mousemove in editing mode.
function edit_poly_wbmf
global xtemp ytemp jj
% this routine updates the figure during editing
point1 = get(gca,'CurrentPoint');
xrange=get(gca,'XLim');
yrange=get(gca,'YLim');
xtemp(jj)=min(xrange(2),max(xrange(1),point1(1,1)));
ytemp(jj)=min(yrange(2),max(yrange(1),point1(1,2)));

% --- Executes on button click in editing mode.
function edit_poly_wbdf
global x y jj kk
% this routine updates the figure during editing
kk=1; %this serves as a flag for the updating to stop
zoom(gcf,'off')
point1 = get(gca,'CurrentPoint');
xrange=get(gca,'XLim');
yrange=get(gca,'YLim');
x(jj)=min(xrange(2),max(xrange(1),point1(1,1)));
y(jj)=min(yrange(2),max(yrange(1),point1(1,2)));

% --- Executes on button press in dragroi1.
function dragroi1_Callback(hObject, eventdata, handles,ROInumber)
global ROIstruct
persistent statusline
% hObject    handle to dragroi1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom(gcf,'off')
if ROIstruct(ROInumber).draggable==1 %the draggable has been activated before
    set(findobj(gcf,'Type','uicontrol'),'Enable','On') % this makes all controls active again
    typeofROI=ROIRosettaStone(ROIstruct(ROInumber).type); % this returns the text
    switch typeofROI   %and now to get the new data
        case 'Rectangle'
            x=get(ROIstruct(ROInumber).handle,'Xdata');
            y=get(ROIstruct(ROInumber).handle,'Ydata');
        case 'Circle'
            px=get(ROIstruct(ROInumber).handle,'Xdata');
            py=get(ROIstruct(ROInumber).handle,'Ydata');
            x = [px(1) py(1)];             % here x=center and y=radius
            y =  x*0+sqrt(sum((px(2)-px(1)).^2+(py(2)-py(1)).^2));
        case 'Polygon'
            x=get(ROIstruct(ROInumber).handle,'Xdata');
            y=get(ROIstruct(ROInumber).handle,'Ydata');
        case 'Slice'
            px=get(ROIstruct(ROInumber).handle,'Xdata');
            py=get(ROIstruct(ROInumber).handle,'Ydata');
            x = [px(1) px(2) px(end-1)];             % here the coordinates of the first, second and last
            y = [py(1) py(2) py(end-1)];
    end
    AddROIvalues(ROInumber,x,y) %and now save new data to ROIstruct
    manipulateROI(ROInumber,'undraggable')
    updateROIgui(handles)
    updateROIlines(handles)
    delete(statusline)
else
    % start draggable state
    %only do if it is created
    
    
    if ROIstruct(ROInumber).created==1 % this means there is a ROI defined
        %first clearing the old one
        manipulateROI(ROInumber,'draggable')
        updateROIgui(handles)
        set(findobj(gcf,'Type','uicontrol'),'Enable','off') % this makes all controls inactive
        set(findobj(gcf,'Style','Text'),'Enable','on') % this turns the text back on
        set(hObject,'Enable','on') % this sets the "drag" button
        draggable(ROIstruct(ROInumber).handle)
        set(ROIstruct(ROInumber).handle,'Selected','on')
        statusline=status('Drag the RONI of interest! When finished press "Apply" ',get(hObject,'Parent'));
    else
        warndlg('RONI not defined!')
    end
end


%***********************************************************************
function createroihandle=getcreateroihandle(handles,ROInumber);
%this function is needed because there was a screwup in naming the first
%creatroi button
if ROInumber==1
    createroihandle=handles.CreateROI;
else
    eval(['createroihandle=handles.createroi',num2str(ROInumber),';']);
end

%***********************************************************************
function updateROIgui(handles)
global ROIstruct
%first zoom buttons
h=zoom(gcf);
if strcmp(get(h,'Enable'),'on')
    set(handles.ZoomOn,'BackgroundColor',[0,1,0])
    set(handles.ZoomOff,'BackgroundColor',[0.941,0.941,0.941])
else
    set(handles.ZoomOff,'BackgroundColor',[0,1,0])
    set(handles.ZoomOn,'BackgroundColor',[0.941,0.941,0.941])
end

for ii=1:length(ROIstruct)
    eval(['set(handles.active',num2str(ii),',''Value'',ROIstruct(ii).active);'])
    eval(['set(handles.outside',num2str(ii),',''Value'',ROIstruct(ii).outside);'])
    eval(['set(handles.typeroi',num2str(ii),',''Value'',ROIstruct(ii).type);'])
    createroihandle=getcreateroihandle(handles,ii);
    eval(['set(handles.dragroi',num2str(ii),',''BackgroundColor'',get(handles.savemask,''BackgroundColor''));'])
    eval(['set(handles.dragroi',num2str(ii),',''String'',''Drag'');'])
    eval(['set(handles.editroi',num2str(ii),',''BackgroundColor'',get(handles.savemask,''BackgroundColor''));'])
    eval(['set(handles.editroi',num2str(ii),',''String'',''Edit'');'])
    if ROIstruct(ii).created==1
        set(createroihandle,'BackgroundColor','red')
        set(createroihandle,'String','Delete')
        eval(['set(handles.active',num2str(ii),',''Enable'',''On'');'])
        eval(['set(handles.outside',num2str(ii),',''Enable'',''On'');'])
        eval(['set(handles.dragroi',num2str(ii),',''Enable'',''On'');'])
        eval(['set(handles.editroi',num2str(ii),',''Enable'',''On'');'])
        if ROIstruct(ii).draggable==1
            eval(['set(handles.dragroi',num2str(ii),',''BackgroundColor'',''yellow'');'])
            eval(['set(handles.dragroi',num2str(ii),',''String'',''Apply'');'])
        elseif ROIstruct(ii).editing==1
            eval(['set(handles.editroi',num2str(ii),',''BackgroundColor'',''yellow'');'])
            eval(['set(handles.editroi',num2str(ii),',''String'',''Apply'');'])
        end
    else
        %if it is not created then active,outisde,edit and draggable should
        %not be allowed
        eval(['set(handles.active',num2str(ii),',''Enable'',''Off'');'])
        eval(['set(handles.outside',num2str(ii),',''Enable'',''Off'');'])
        eval(['set(handles.dragroi',num2str(ii),',''Enable'',''Off'');'])
        eval(['set(handles.editroi',num2str(ii),',''Enable'',''Off'');'])
        set(createroihandle,'BackgroundColor',get(handles.savemask,'BackgroundColor'))
        set(createroihandle,'String','Create')
        %and if zoom is on then it is not allowed to create ROI's
        %         if strcmp(get(h,'Enable'),'on')
        %             set(createroihandle,'Enable', 'Off')
        %         else
        %             set(createroihandle,'Enable', 'On')
        %         end
    end
end

%***********************************************************************
function updateROIlines(handles)
global ROIstruct
%recreating original image
img=get(handles.imageaxis,'UserData');
imagesc(img{1})
set(handles.imageaxis,'UserData',img);
set(handles.imageaxis,'YDir','normal');
top = fix(str2num(get(handles.slidertopvalue, 'String')));
set(handles.imageaxis, 'CLim', [0 top])

for ii=1:length(ROIstruct)
    eval(['activehandle=handles.active',num2str(ii),';'])
    createroihandle=getcreateroihandle(handles,ii);
    % this is true if active is indicated
    if 1==get(activehandle,'Value') && strcmp(get(createroihandle,'String'),'Delete')% true if Active is ticked off and it is created..then plot
        eval(['typehandle=handles.typeroi',num2str(ii),';'])
        typeofROI=get(typehandle,'Value');
        typeofROI=ROIRosettaStone(typeofROI);
        switch typeofROI
            case 'Rectangle'
                
                x=ROIstruct(ii).values(1,:);
                y=ROIstruct(ii).values(2,:);
                hold on
                axis manual
                ROIstruct(ii).handle=plot(x,y,'r--','linewidth',1) ;
                hold off
            case 'Circle'
                center=ROIstruct(ii).values(1,:); %this is the center
                radius=ROIstruct(ii).values(2,1); % this is the radius
                ang=[0:1:360]*2*pi/360;
                xvalues=[center(1),center(1)+radius*cos(ang)];
                yvalues=[center(2),center(2)+radius*sin(ang)];
                hold on
                axis manual
                ROIstruct(ii).handle=plot(xvalues,yvalues,'r--','linewidth',1);
                hold off
            case 'Polygon'
                x=ROIstruct(ii).values(1,:);
                y=ROIstruct(ii).values(2,:);
                hold on
                axis manual
                ROIstruct(ii).handle=plot(x,y,'r--','linewidth',1) ;
                hold off
            case 'Slice'
                cen=ROIstruct(ii).values(:,1); %this is the center
                firstp=ROIstruct(ii).values(:,2); % this is the radius
                lastp=ROIstruct(ii).values(:,3);
                
                radius=sqrt(sum((firstp(1:2)-cen(1:2)).^2));
                a1=atan2(firstp(2)-cen(2),firstp(1)-cen(1)); % angle of 1st edge of slice
                a2=atan2(lastp(2)-cen(2),lastp(1)-cen(1)); % angle of last (2nd) edge of slice
                % atan2 generates results from -pi to +pi with 0 being horizontal, to the
                % right...if one wants to find the angle between the two one has to jump
                % through some hoops
                
                if (a1<=a2)
                    a=a2-a1;
                else
                    a=2*pi+(a2-a1);
                end
                
                ang=[a1:1/360*2*pi:a1+a];
                xvalues=[cen(1),cen(1)+radius*cos(ang),cen(1)];
                yvalues=[cen(2),cen(2)+radius*sin(ang),cen(2)];
                hold on
                axis manual
                ROIstruct(ii).handle=plot(xvalues,yvalues,'r--','linewidth',1);
                hold off
        end
    end
end

%***********************************************************************
function drawROIline(ROInumber)
global ROIstruct
x=ROIstruct(ROInumber).values(1,:);
y=ROIstruct(ROInumber).values(2,:);
hold on
axis manual
ROIstruct(ROInumber).handle=plot(x,y,'r','linewidth',1)
hold off

% --- Executes during object creation, after setting all properties.
function slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background, change
%       'usewhitebg' to 0 to use default.  See ISPC and COMPUTER.
usewhitebg = 1;
if usewhitebg
    set(hObject,'BackgroundColor',[.9 .9 .9]);
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

% --- Executes on slider movement.
function slider_Callback(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of
%        slider

% Callback for slider controlling top of color scale.
% top_text calls this too.
top = fix(get(hObject, 'Value'));
set(hObject, 'Value', top)
set(handles.slidertopvalue, 'String', num2str(top))
set(handles.imageaxis, 'CLim', [0 top])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes during object creation, after setting all properties.
function slidertopvalue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slidertopvalue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function slidertopvalue_Callback(hObject, eventdata, handles)
% hObject    handle to slidertopvalue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of slidertopvalue as text
%        str2double(get(hObject,'String')) returns contents of slidertopvalue as a double

% Callback for manual input of slider controlling top of color scale.
% top_text calls this too.
top = fix(str2num(get(hObject, 'String')));
set(hObject, 'String', num2str(top))
set(handles.slider, 'Value', top)
set([handles.imageaxis], 'CLim', [0 top])


function ConstructROIstructure(nROI)
global ROIstruct
for ii=1:nROI
    ROIstruct(ii).created=0;
    ROIstruct(ii).active=0;
    ROIstruct(ii).outside=0;
    ROIstruct(ii).type= 1;
    ROIstruct(ii).editing=0;
    ROIstruct(ii).draggable=0;
    ROIstruct(ii).values=[];
    ROIstruct(ii).handle=[];
end

function AddROIvalues(ii,xvalues,yvalues)
global ROIstruct
ROIstruct(ii).values=[xvalues;yvalues];

function manipulateROI(n,argin)
global ROIstruct
switch argin
    case 'editable'
        ROIstruct(n).editing=1;
    case 'uneditable'
        ROIstruct(n).editing=0;
    case 'draggable'
        ROIstruct(n).draggable=1;
    case 'undraggable'
        ROIstruct(n).draggable=0;
    case 'create'
        ROIstruct(n).created=1;
    case 'delete'
        ROIstruct(n).created=0;
        ROIstruct(n).active=0;
        ROIstruct(n).outside=0;
        ROIstruct(n).type= 1;
        ROIstruct(n).editing=0;
        ROIstruct(n).values=[];
    case 'activate'
        ROIstruct(n).active=1;
    case 'deactivate'
        ROIstruct(n).active=0;
    case 'excludeinterior'
        ROIstruct(n).outside=1;
    case 'excludeexterior' %this is the default
        ROIstruct(n).outside=0;
    case 'Rectangle'
        typeROI=ROIRosettaStone(argin);
        ROIstruct(n).type=typeROI;
    case 'Circle'
        typeROI=ROIRosettaStone(argin);
        ROIstruct(n).type=typeROI;
    case 'Polygon'
        typeROI=ROIRosettaStone(argin);
        ROIstruct(n).type=typeROI;
    case 'Slice'
        typeROI=ROIRosettaStone(argin);
        ROIstruct(n).type=typeROI;
end


function [functionname,ROInumber]=determineROInumber(callback_name)
%This function takes the callback_name and finds the actual ROI number and
%function type.

underscore_position=strfind(callback_name,'_');
%here we assume that the ROI number precedes the underscore character
tmp=callback_name(underscore_position-3,underscore_position-1);
% this one removes the non-number values from the string
tmp=[num2str(str2num(tmp(1))),num2str(str2num(tmp(2))),num2str(str2num(tmp(3)))];
ROInumber=str2num(tmp);
% now need to find the functionname
% assuming it is before the
% underscore and the ROI number
tmplength=length(tmp);
functionname=callback_name(1:undescore_position-tmplength-1);


function argout=ROIRosettaStone(argin)
if isstr(argin)
    if strcmp(argin,'Rectangle'), argout=1; end
    if strcmp(argin,'Circle'), argout=2; end
    if strcmp(argin,'Polygon'), argout=3; end
    if strcmp(argin,'Slice'), argout=4; end
else
    if argin==1, argout='Rectangle'; end
    if argin==2, argout='Circle'; end
    if argin==3, argout='Polygon'; end
    if argin==4, argout='Slice'; end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STATUS BAR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function h_out = status(message, figure, seconds)
% Display a status line message, returning its handle.
% If you the caller don't specify SECONDS, then you must delete the status
% text control via its handle.
if nargin == 2
    h = figstatus(message, figure);
elseif nargin == 3
    h = figstatusbrief(message, seconds, figure);
end
if nargout
    h_out = h;
end



% --- Executes on button press in CloseGUI.
function CloseGUI_Callback(hObject, eventdata, handles)
% hObject    handle to CloseGUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(gcf,'WindowButtonMotionFcn','')
set(gcf,'DoubleBuffer','off')
set(gcf,'WindowButtonDownFcn','')
hold off
close(gcf)


% --- Executes on button press in ZoomOn.
function ZoomOn_Callback(hObject, eventdata, handles)
% hObject    handle to ZoomOn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom(gcf,'on')
updateROIgui(handles)


% --- Executes on button press in ZoomOff.
function ZoomOff_Callback(hObject, eventdata, handles)
% hObject    handle to ZoomOff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom(gcf,'off')
updateROIgui(handles)


% --- Executes on button press in ZoomOut.
function ZoomOut_Callback(hObject, eventdata, handles)
% hObject    handle to ZoomOut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom(gcf,'out')
updateROIgui(handles)



