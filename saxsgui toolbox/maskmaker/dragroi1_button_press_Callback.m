% --- Executes on pressing in a ROI in the window (called as buttonpressfcn.
function dragroi1_button_press_Callback(ROInumber)
global ROIstruct
% hObject    handle to dragroi1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Will only drag for now...later it may also edit points

if ROIstruct(ROInumber).created==1 % this means there is a ROI defined
    disp('button pressed')
    set(ROIstruct(ROInumber).handle,'Selected','on')
    draggable(ROIstruct(ROInumber).handle)
    waitforbuttonpress
    set(gcf,'WindowButtonUpFcn','disp(''up'')')
    draggable(ROIstruct(ROInumber).handle,'off') %switch off draggable
%     typeofROI=ROIRosettaStone(ROIstruct(ROInumber).type); % this returns the text
%     switch typeofROI   %and now to get the new data
%         case 'Rectangle'
%             x=get(ROIstruct(ROInumber).handle,'Xdata');
%             y=get(ROIstruct(ROInumber).handle,'Ydata');   
%         case 'Circle'
%             x=get(ROIstruct(ROInumber).handle,'Xdata');
%             y=get(ROIstruct(ROInumber).handle,'Ydata');
%         case 'Polygon'
%             x=get(ROIstruct(ROInumber).handle,'Xdata');
%             y=get(ROIstruct(ROInumber).handle,'Ydata');
%     end
%     AddROIvalues(ROInumber,x,y) %and now save new data to ROIstruct
%     manipulateROI(ROInumber,'undraggable')
    %updateROIgui(handles)
    %updateROIlines(handles)
else
    warndlg('ROI not defined!') % should never happen
end
disp('done')

