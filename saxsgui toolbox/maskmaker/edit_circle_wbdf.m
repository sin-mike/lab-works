function edit_circle_wbdf(nodummy)
% --- Executes on button down in editing mode.
global x y jj kk
if nargin==1 % this is just for touching the file (for executable)
    return
end
kk=1; %this serves as a flag for the updating to stop
point1 = get(gca,'CurrentPoint');
xrange=get(gca,'XLim');
yrange=get(gca,'YLim');
xtemp=x;
ytemp=y;
if jj(1)==1 % if the center of the circle is clicked
    xtemp(1)=min(xrange(2),max(xrange(1),point1(1,1))); %make sure new point is withing window boundaries
    ytemp(1)=min(yrange(2),max(yrange(1),point1(1,2))); %make sure new point is withing window boundaries
    center = [xtemp(1),ytemp(1)] ;             % calculate locations
    radius = sqrt((x(2)-x(1)).^2+(y(2)-y(1)).^2);
end
if jj(1)>1 %if the perimeter is clicked
    xtemp(2)=min(xrange(2),max(xrange(1),point1(1,1))); %make sure new point is withing window boundaries
    ytemp(2)=min(yrange(2),max(yrange(1),point1(1,2)));%make sure new point is withing window boundaries
    center = [xtemp(1),ytemp(1)];           % calculate locations
    radius = sqrt((xtemp(2)-xtemp(1)).^2+(ytemp(2)-ytemp(1)).^2);
end
ang=[0:1:360]*2*pi/360; %360  point on the circle
x=[center(1),center(1)+radius*cos(ang)];
y=[center(2),center(2)+radius*sin(ang)];
