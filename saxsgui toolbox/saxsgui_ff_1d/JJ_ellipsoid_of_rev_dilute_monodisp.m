function Icalc=JJ_ellipsoid_of_rev_dilute_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for dilute monodisperse
% Ellipsoid of Revolution 
% Description end
% Number of Parameters: 4
% parameter 1: Amp : Amplitude of function 
% parameter 2: R : Primary Radius 
% parameter 3: Epsilon : Ellipticity
% parameter 4: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of ellipsoid
% of revolution- The averaging over orientations is performed numerically
% Taken from J.S.Pedersen, ch 16. case 5, eq. 27
%

values=varargin{1};
Amp=values(1);
R=values(2);
Epsilon=values(3);
Backg=values(4);

alphastep=0.01;
alpha=[0:alphastep:pi/2];
r=Rel(R,Epsilon,alpha);
sumoveralpha=0;
for ii=1:length(alpha)
    F=F1(q,r(ii));
    sumoveralpha=sumoveralpha+sin(alpha(ii))*abs(F.^2)*alphastep;
end

Icalc=Amp*sumoveralpha+Backg;

function Fout=F1(q,R)
Fout=3*(sin(q*R)-q*R.*cos(q*R))./((q*R).^3);

function Relout=Rel(R,Epsilon,alpha)
Relout=R*sqrt((sin(alpha).^2+Epsilon^2*cos(alpha).^2));
