function Icalc=JJ_cylinder_dilute_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for dilute monodisperse dispersion
% of Cylinders 
% Description end
% Number of Parameters: 4
% parameter 1: Amp : Amplitude of function 
% parameter 2: R : Radius of cylinder
% parameter 3: L : Length of cylinder
% parameter 4: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of solid 
% cylinders
% - The averaging over orientations is performed numerically
% Taken from J.S.Pedersen, ch 16. case 11, eq. 30
%

values=varargin{1};
Amp=values(1);
R=values(2);
L=values(3);
Backg=values(4);

alphastep=0.01;
alpha=[alphastep:alphastep:pi/2-alphastep];

sumoveralpha=0;
for ii=1:length(alpha) 
    F=FCyl(q,R,L,alpha(ii));
    sumoveralpha=sumoveralpha+sin(alpha(ii))*abs(F.^2)*alphastep;
end
Icalc=Amp*sumoveralpha+Backg;

function Fout=FCyl(q,R,L,alpha)
Fout=2*(besselj(1,q*R*sin(alpha)).*sin(q*L*cos(alpha/2)))./(q.^2*R*sin(alpha)*L*cos(alpha/2));

