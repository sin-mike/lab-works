function Icalc=JJ_cylinder_elliptical_dilute_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for dilute monodisperse dispersion
% of Cylinders with elliptical cross section
% Description end
% Number of Parameters: 5
% parameter 1: Amp : Amplitude of function 
% parameter 2: a : 1st crosssection semiaxis
% parameter 3: b : 2nd crosssection semiaxis
% parameter 4: L : Length of cylinder
% parameter 5: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of solid 
% cylinders with elliptical cross section
% - The averaging over orientations is performed numerically
% Taken from J.S.Pedersen, ch 16. case 12, eq. 31
%

values=varargin{1};
Amp=values(1);
a=values(2);
b=values(3);
L=values(4);
Backg=values(5);

alphastep=0.01;
alpha=[alphastep:alphastep:pi/2];
phistep=0.01;
phi=[0:phistep:pi/2];

sumoveralpha=0;
for ii=1:length(alpha) 
    sumoverphi=0;
    for jj=1:length(phi) 
        F=FCyl(q,Rel(a,b,phi(jj)),L,alpha(ii));
        sumoverphi=sumoverphi+abs(F.^2)*phistep;
    end
    sumoveralpha=sumoveralpha+sumoverphi*alphastep*2/pi*sin(alpha(ii));
end
Icalc=Amp*sumoveralpha+Backg;

function Fout=FCyl(q,Rel,L,alpha)
Fout=2*(Besselj(1,q*Rel*sin(alpha)).*sin(q*L*cos(alpha/2)))./(q.^2*Rel*sin(alpha)*L*cos(alpha/2));

function Relout=Rel(a,b,phi)
Relout=sqrt(a^2*sin(phi)^2+b^2*cos(phi^2));
