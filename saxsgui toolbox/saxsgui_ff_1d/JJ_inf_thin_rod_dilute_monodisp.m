function Icalc=JJ_inf_thin_rod_dilute_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for dilute monodisperse dispersion
% of infinitely long thin rods
% Description end
% Number of Parameters: 3
% parameter 1: Amp : Amplitude of function
% parameter 2: L : Length of Rod
% parameter 3: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of infinitely
% long thin rods
% 
% Taken from J.S.Pedersen, ch 16. case 15, eq. 34-35
%

values=varargin{1};
Amp=values(1);
L=values(2);
Backg=values(3);

Icalc=Amp*(2*Si(q*L)./(q*L)-4*(sin(q*L/2)./(q*L)).^2)+Backg;

function Siout=Si(x)
Siout=0*x;
for ii=1:length(x)
    
    tstep=x(ii)/500;
    t=tstep:tstep:x(ii);
    temp1=sin(t)./t;
    Siout(ii)=(1+sum(temp1))*tstep;
end



