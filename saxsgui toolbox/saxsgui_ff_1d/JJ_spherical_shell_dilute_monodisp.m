function Icalc=JJ_spherical_shell_dilute_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for monodisperse hard spherical shell
% Description end
% Number of Parameters: 4
% parameter 1: Amp : Amplitude of function 
% parameter 2: R1 : Outer Radius of Shell 
% parameter 3: R2 : Inner Radius of Shell
% parameter 4: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of hard
% shells. Taken from J.S.Pedersen, ch 16. case 2, eq. 22
%  

values=varargin{1};
Amp=values(1);
R1=values(2);
R2=values(3);
Backg=values(4);

F2=(V(R1).*F1(q,R1)-V(R2).*F1(q,R2))/(V(R1)-V(R2));
Icalc=Amp*abs(F2.^2)+Backg;

function Fout=F1(q,R)
Fout=3*(sin(q*R)-q*R.*cos(q*R))./((q*R).^3);

function Vout=V(R)
Vout=4*pi*R^3/3;
