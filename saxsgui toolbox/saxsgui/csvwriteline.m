function outfile = csvwriteline(hline, filename)
%CSVWRITELINE Write line data to a comma-separated-values file.
%   CSVWRITELINE(HLINE, FILENAME) writes the data points of the line whose
%   handle is HLINE to a text file named FILENAME as comma separated
%   values, that is an x,y pair on each line.

if ~ (nargin == 2 && ishandle(hline) && isequal(get(hline, 'Type'), 'line') ...
        && ischar(filename) && ndims(filename) == 2 && size(filename, 1) == 1)
    error('I need a handle to a line object and a filename string.')
end

X = get(hline, 'XData');
Y = get(hline, 'YData');
S = get(hline, 'UserData');


E=S.relerr.*Y;                 
DX=0*E;
%setting points where we have NaN's  zero.
E(isnan(E)) = 0.0;
Y(isnan(Y)) = 0.0;
comment1=sprintf('%s',S.raw_filename(1:end));
comment2='';
%
if length(Y)==length(E)         
    outfile=writeradfile('CSVXYdY',filename,comment1,comment2,X,Y,E,DX);
else                            
    outfile=writeradfile('CSVXY',filename,comment1,comment2,X,Y,E,DX);
end                             

