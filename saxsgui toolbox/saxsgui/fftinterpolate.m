function [X,Y]=fftinterpolate(x,y,x0,filter_width)

% Do the fourier transform
d = fft(y);
m = length(y);
M = floor((m+1)/2);

mm=1:length(d);
d(3:end)=d(3:end)./((mm(3:end)).^0.2);
a0 = d(1)/m; 
an = 2*real(d(2:M))/m;
a6 = d(M+1)/m;
bn = -2*imag(d(2:M))/m;
 % now do the interpolation

maxx=length(y);
X = 0:maxx/1000:maxx;
n = 1:length(an);
Y = a0 + an*cos(2*pi*n'*X/maxx) ...
       + bn*sin(2*pi*n'*X/maxx) ...
       + a6*cos(2*pi*6*X/maxx); 
 

