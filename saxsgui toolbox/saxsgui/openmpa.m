function s = openmpa(pathname)
%OPENMPA Open an MPA image file, display the image, return a saxs object.
%   This function extends the OPEN function for *.mpa files.
%
%   It also enables opening MPA files from the MATLAB file browser.
%   Alas, when called from the file browser it does NOT add a 
%   saxs variable to the workspace.  Hm.

s = mparead(pathname);
image(s)
