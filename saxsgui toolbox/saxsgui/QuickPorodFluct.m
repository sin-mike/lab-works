function Icalc=QuickPorodFluct(q,I,varargin)
% Description
% Quick Porod with Fluctuation term
% Description end
% Number of Parameters: 3
% parameter 1: Kp : Porod constant Kp
% parameter 2: famp : Amplitude fluctuation
% parameter 3: ftype : Fluctuation type (1D/2D/3D) --> Ruland j. app.
% cryst, 1971, 4, 70
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Quick Porod with Fluctuation term


global fitpar
values=varargin{1};
Kp=values(1);
if numel(values)==1
    famp=0;
    ftype=3;
else
    famp=values(2);
    ftype=values(3);
end
switch ftype
    case 1
        fluct=famp./q.^2;
    case 2
        fluct=pi*famp./q;
    case 3
        fluct=famp;
end

Icalc=Kp./q.^4+fluct;
%clear waste
%--------------------------------------------------------------------------
