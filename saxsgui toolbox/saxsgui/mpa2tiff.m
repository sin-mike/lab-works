function  mpa2tiff(mpa_pathname,tiff_pathname)
%MPA2TIFF convert an mpa file to a tiff file 
%      mpa2tiff('mpa_pathname','tiff_pathname'); loads, converts and save the mpa file
%   with name mpa_pathname to a tiff file name tiff_pathname.
%
%   If the second argument is 'same', then the tiff-file is given the same
%   name as the mpa file.
%   
%   If there is no argument the function prompts for an input mpa-file.
%   
%   If there is only one argument the function prompts foo an output file with 
%   a .tif as extension
%
%   


if nargin==0
    [mpafile,mpapath]=uigetfile('*.mpa','Pick an mpa-file');
    mpa_pathname=[mpapath,mpafile]
end
if nargin<2
    [mpapath,mpafile,mpaext]=fileparts(mpa_pathname);
    tiff_pathname=fullfile(mpapath,[mpafile,'.tif']);
    [tiffile,tiffpath]=uiputfile('*.tif','Save images as a tif-file',tiff_pathname);
    tiff_pathname=[tiffpath,tiffile]
else
    if strcmp(tiff_pathname,'same')  % flag to use the same filename root
        tiff_pathname=mpa_pathname
    end
    % make sure that tiff file has a tif extension
    [tiffpath,tifffile,tiffext]=fileparts(tiff_pathname);
    tiff_pathname=fullfile(tiffpath,[tifffile,'.tif']);
end
try
    S=mparead(mpa_pathname);    
catch
    error('Unable to obtain SAXS image from MPA-file')
end
saxs2tiff(S,tiff_pathname)
    
    

