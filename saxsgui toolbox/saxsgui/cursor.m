function cursor(fig, shape)
%CURSOR Set the pointer shape within a figure to full crosshairs.
%   CURSOR sets full crosshairs for the current figure.
%
%   CURSOR(FIG) sets full crosshairs for the specified figure.
%
%   CURSOR('shape') sets the pointer shape in the figure to
%   'shape'.  See possible values by executing the command
%   SET(GCF, 'Pointer').  Use 'default' to restore default
%   pointer shapes.
%
%   CURSOR(FIG, 'shape') specifies both arguments.

switch nargin
case 0
    fig = gcf;
    shape = 'fullcrosshair';
case 1
    if ischar(fig)  % then not really a figure handle
        shape = fig;
        fig = gcf;
    else  % assume it is a figure handle
        shape = 'fullcrosshair';
    end
end

set(fig, 'Pointer', shape)