function Icalc=Debye(q,phi,varargin)
% Description
% This function calculates a 2D scattering function according to the Debye equation 
% Description end
% Number of Parameters: 3
% parameter 1: A : intensity scaling factor
% parameter 2: lc : Correlation length
% parameter 3: fluct : An optional fluctuation term addition. 0 if not used.
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can put additional info
% 
%function written by Brian R. Pauw, 2007.

i=1;
values=varargin{1};
A=values(i);i=i+1;
lc=values(i);i=i+1;
fluct=values(i);i=i+1;

%this is the Debye fitting function, that describes the decay at
%intermediate angles.
Icalc=A.*lc.^3./((1+q.^2.*lc^2).^2)+fluct;