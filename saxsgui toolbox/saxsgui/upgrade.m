function succeeded = upgrade(zippath)
%UPGRADE Upgrade Molecular Metrology MATLAB software.
%   UPGRADE('zipfilename') upgrades a molmet folder containing Molecular
%   Metrology MATLAB software with the version in 'zipfilename'.
%
%   Run UPGRADE from a batch file or command line with the command
%
%       matlab /r upgrade('zipfilename')
%
%   If you run UPGRADE from within a MATLAB session it will terminate your
%   MATLAB session when it finishes.

% Check the argument count.
if nargin < 1
    abort('Specify an installation zip archive file.')
end
% Make sure zip archive exists.
zipexists = exist(zippath, 'file');
if isequal(zipexists, 0)
    abort('The installation zip archive file is not here.')
end
[zipdir zipname zipext] = fileparts(zippath);
% Find a molmet directory.
molmetdir = findmolmet;
if isequal(molmetdir, 0)
    abort('You must select a folder to install Molecular Metrology MATLAB software.')
end
% Save the entire molmet directory tree.
savename = [molmetdir, filesep, 'before_', zipname, '.zip'];
if exist(savename, 'file')
    delete(savename)
end
try
    zip(savename, '*', molmetdir)
catch
    % Ignore errors.
end
% Extract files from the archive into the molmet directory.
unzip(zippath, molmetdir)
waitfor(msgbox(['Molecular Metrology MATLAB software installed.', ...
        '  If you created a new molmet folder, remember to add it to your', ...
        ' MATLAB path the next time you run MATLAB.  (Use the Set Path command', ...
        ' from the File menu.)'], ...
    'Molecular Metrology MATLAB software installation', 'modal'))
goodbye


function molmetdir = findmolmet
% Find molmet directory.  Return directory name or 0 if user cancels.
% PLAN -
% Use WHICH 'saxsgui.m' to locate molmet directory.
% Ask user to confirm molmet directory, or try an
% open-directory dialog box.
saxsguipath = which('saxsgui.m');
molmetdir = fileparts(saxsguipath);
% Note if saxsgui.m not found, saxsguipath and molmetdir are both ''.
molmetdir = uigetdir(molmetdir, 'Select molmet folder.');
if molmetdir
    [mdir mname mext] = fileparts(molmetdir);
    if ~ isequal(mname, 'molmet') || ~ isempty(mext)
        button = questdlg(['You should normally name your molmet folder', ...
                '''molmet''.  Do you really want to use the name ', ...
                mname, mext, '?'], ...
            'Nonstandard molmet folder name');
        switch button
            case 'Yes'
            case 'No'
                molmetdir = findmolmet;
            otherwise
                molmetdir = 0;
        end
    end
end


function abort(msg)
% Display msg, wait for acknowledgement, exit MATLAB.
waitfor(errordlg(msg, ...
    'Molecular Metrology MATLAB software installation', 'modal')')
goodbye


function goodbye
% Either exit MATLAB or, when debugging,
% generate an error to stop this M-file.
exit
% error('Normal debug termination.')
