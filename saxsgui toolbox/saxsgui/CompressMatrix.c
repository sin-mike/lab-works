/*
* =============================================================
* CompressMatrix.c
* The funtction is called with:
*								DataOutput=CompressMatrix(InputMatrix, OutputMatrix)
* This function compresses an X by Y matrix to a I by K (avergeing the cell contents)
* Note: This could be done in 2 steps, rows the columns but would require a large intermediate
*		 array which apears to be C limited to a maximum of 1M points
*
* This function first averages a number of rows, then repeats this for a number of columns
* giving a "cell" total (where a cell is the large matrix divided down. This average is then
* placed in the output memory using pointer notation (requirement by matlab). The process is then
* repeated, moving to the rows of the next cell (under the previous one). This is then repeated
* with the next cells to the right of the first...etc..etc.
* The final mexFunction is the mat lab requiremnt used to pass the variables to/from the function
* using pointers.
* ============================================================
*/

#include "mex.h"
#include "math.h"

void CompressMatrix(double *InputMatrix, int mrowsIn, int ncolsIn, double 
*OutputMatrix, int mrowsOut, int ncolsOut){ /* The return of th funcion is void as matlab recieves pointers to the function output*/

		int   RowCompressionF, ColCompressionF, RowN,ColN, RowNout, ColNout, ColNstart, RowNstart;
		double   ColSum, RowSum; /*Note maximum output array is of size 1M pixels!*/

		RowCompressionF=mrowsIn/mrowsOut;
		ColCompressionF=ncolsIn/ncolsOut; /*calculate how many columns/ rows to compress into one*/
		for (ColNstart=0, ColNout=0; ColNstart<ncolsIn; ColNstart=ColNstart+ColCompressionF, ColNout=ColNout+1){
			for (RowNstart=0,RowNout=0; RowNstart<mrowsIn; RowNstart=RowNstart+RowCompressionF, RowNout=RowNout+1) { 
                /*do a colum at a time compressing RCF rows into one and place in the CompressedRows array*/
				for (ColN=0, ColSum=0; ColN<ColCompressionF; ColN=ColN+1) {
					for (RowN=0, RowSum=0; RowN<RowCompressionF; RowN=RowN+1)			/*First sum the first number of rows*/
						RowSum=RowSum+*(InputMatrix+(ColNstart+ColN)*mrowsIn+RowNstart+RowN);
					ColSum=ColSum+RowSum/RowCompressionF;					/*Now Sum the Sum of Rows*/
				}
				*(OutputMatrix+ColNout*mrowsOut+RowNout)=ColSum/ColCompressionF ;/*Now assign this sum to the outpur matrix*/
			}
		}
		return;
	}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) /*This part takes care of passing the matlab information to and from the function using pointers*/
{
	int mrowsIn,ncolsIn, mrowsOut, ncolsOut;
	double *InputMatrix, *OutputMatrix;

	InputMatrix=mxGetPr(prhs[0]); /*Assign input matrix*/
	mrowsIn = mxGetM(prhs[0]);
	ncolsIn = mxGetN(prhs[0]); /*Need to know the number of rows and columns of input matrix*/

	mrowsOut = mxGetM(prhs[1]);
	ncolsOut = mxGetN(prhs[1]); /*Determine the size of the matrix to be output*/

	plhs[0] = mxCreateDoubleMatrix(mrowsOut, ncolsOut, mxREAL); /*Generate blank matrix for the output*/
	OutputMatrix=mxGetPr(plhs[0]); /* Assign pointer output*/

	CompressMatrix(InputMatrix, mrowsIn, ncolsIn, OutputMatrix, mrowsOut, ncolsOut); /* inputs and outputs to function*/
	return;
}

