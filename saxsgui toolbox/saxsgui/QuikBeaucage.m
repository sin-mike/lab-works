function Icalc=QuikBeacage(q,I,varargin)
% Description
% This function calculates a 1d Scattterin function according to the 
% Universal Scattering Function by Beaucage
% Description end
% Number of Parameters: 4
% parameter 1: G : Intensity extrapolated to 0
% parameter 2: Rg : Radius of Gyration
% parameter 3: B : Amplitude of high q-region
% parameter 4: P : Slope of high q-region% 
% parameter 5: backg : Constant Background
% q-region
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can put additional info
% 
%function written by Karsten Joensen, 2007.

i=1;
values=varargin{1};

G=values(1); % 
Rg=values(2);
B=values(3);
P=values(4);
if numel(values)==4
    backg=0;
else
    backg=values(5);
end

% This is the Beaucage Universal Fitting function function
qs=q./(erf(q*Rg/sqrt(6))).^3;
BeaucageA=G*exp(-(q.^2)*(Rg.^2)/3);
BeaucageB=B*(qs).^-P;

Icalc=BeaucageA+BeaucageB+backg;

