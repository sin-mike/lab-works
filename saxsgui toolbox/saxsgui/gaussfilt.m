function h = gaussfilt(size, std)
%GAUSSFILT Create a gaussian finite impulse response filter.
%   GAUSSFILT(SIZE, STD) returns a square matrix FIR filter suitable for
%   use with FILTER2.  The gaussian distribution of filter values, when
%   used with FILTER2, is good for removing higher-frequency information
%   (noise) from an image.  SIZE is the number of rows (or columns) in the
%   filter.  STD is the standard deviation of the gaussian falloff curve.

if fix(size / 2) == size / 2  % size is even
    size = size + 1;  % make it odd
end
r = (size - 1) / 2;

[x y] = meshgrid(-r : r, -r : r);
exponent = -(x .* x + y .* y) / (2 * std * std);
h = exp(exponent);
h(h < eps * max(h(:))) = 0;

total = sum(h(:));
if total ~= 0
    h  = h / total;
end
