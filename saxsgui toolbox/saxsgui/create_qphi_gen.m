function[Q,phi]=create_qphi_gen(M,rowcen,colcen,rowcal_at_0,colcal_at_0,wavelength)
% Example
% [Q,phi]=create_qphi_gen(M,rowcen,colcen,rowcal,colcal)
%
% Generalized (SAXS and WAXS) function to create matrices of R and phi,
% given an image, a center of the image, a q-calibration of the image
% and a wavelength.
% 
% Input parameters are
%		M               the input matrix
%		rowcen	the     center row
%		colcen	the     center column
%		rowcal_at_0     the calibrated R-value of the row at zero angle (fx. 85 A^-1/pixel)
% 		colcal_at_0     the calibrated R-value of the column at zero angle
%       wavelength      the wavelength 
% Output parameters are
%		Q			the output matrix giving the 
%					"q-coordinates" for every pixel
%		phi		    the angle (0,360 degrees) from the horizontal vector
% please note that MATLAB format is (Row,Column)
% Origin:
% This routine comes from an earlier version called create_rphi which 
% did the same as this routine, but was only valid for small angles (SAXS)


[rowmax,colmax]=size(M);
[col,row]=meshgrid(1:colmax,1:rowmax);

Q_star=sqrt(((col-colcen)*colcal_at_0).^2+((row-rowcen)*rowcal_at_0).^2);
if isempty(wavelength)
    Q=Q_star;
else
    twok=4*pi/wavelength;
    onek=twok/2;
    Q=twok*sin(0.5*atan(Q_star/onek));
end
phi=-atan2((row-rowcen)*rowcal_at_0,-(col-colcen)*colcal_at_0)/pi*180+180;
%qx=R.*cos(phi*360);
%qy=R.*sin(phi*360);
%mesh(qx,qy,M);
%phi=phi*360;
