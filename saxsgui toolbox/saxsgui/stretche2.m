function strimg = stretche2(img,xc,yc,theta,ap)
% STRETCHE2 Stretches an image in an elliptical pattern.
%   STRIMG = STRETCHE2(IMG, XC, YC, THETA, APELLIP);
%   stretches the two-dimensional image IMG and returns
%   the polar coordinates of the stretched image in STRIMG.
%   XC,YC specifies the center of the image.  THETA specifies
%   the number of degrees to rotate the image to align it
%   with the major axes.  APELLIP is the apparent ellipticity.
%
%   STRIMG contains the largest radius around the center
%   of IMG that does not go beyond the limits of IMG.
%
% SEE ALSO:  CENTER; FINDROT; FINDELLIP

[ymax xmax] = size(img);

% Find the maximum radius that doesn't go off the original image.
rmax = min([xc, xmax - xc, yc, ymax - yc]) - 1;  % subtract 1 to be safe

[x y] = meshgrid(1:xmax,1:ymax);
theta = theta*2*pi/360;  % get theta in radians
% Set coordinates in polar space.
[xip,yip] = meshgrid([1:rmax],theta+(1:360)*2*pi/360);
% Compute coordinates to stretch image elliptically.
[xi,yi] = ellip2cart2(xip,yip,ap);
% Interpolate values for the stretched polar coordinates.
strimg = interp2(x-xc,y-yc,img,xi,yi);
