function xy = xyclick(varargin)
%XYCLICK Return x,y coordinates of pointer click.
%
%   XYCLICK returns coordinates [x, y] of pointer click in current figure.
%   Until click, pointer will use full crosshairs.

%Not implemented:
%   XYCLICK(FIGURE) applies to the figure whose handle is specified.

%   XYCLICK('function', ARG, ...)
%   Private calling form for callbacks from the figure.

if nargin > 0
    if ischar(varargin{1})  % looks like a callback
        try
            feval(varargin{:})
        catch
            disp(lasterr)
        end
        return
    end
end

% We are being invoked, not called back.

fig = gcf;
ax = gca;

% Attach ourselves to an image or surface.
obj = findobj(ax, 'Type', 'image');
if isempty(obj)
    obj = findobj(ax, 'Type', 'surface');
    if isempty(obj)
        error('There is no image or surface here!')
    end
end
obj = obj(1);  % could there be more than one image or surface?

% Um, do we really need to concern ourselves with whether or not an image
% or surface is here?  Shouldn't we work as well on line plots?

prev_pointer = get(fig, 'Pointer');
prev_callback = get(obj, 'ButtonDownFcn');
set(fig, 'Pointer', 'fullcrosshair')
set(obj, 'ButtonDownFcn', 'xyclick(''click'', gcbo)')

% disp('Click within the current image or surface to return x,y coordinates.')

uiwait(fig)  % resume after click

% Restore the pointer and callback settings.
set(obj, 'ButtonDownFcn', prev_callback)
set(fig, 'Pointer', prev_pointer)

% Retrieve the x,y coordinates.
xy = getappdata(obj, 'xyclickxy');
rmappdata(obj, 'xyclickxy')


% --------------------------------------------------------------
function click(obj)
% Callback for click on image or surface.
% Set x,y coordinates of the pointer click as an object property
% for retrieval by the mother routine.
axes = get(obj, 'Parent');
position = get(axes, 'CurrentPoint');
setappdata(obj, 'xyclickxy', [position(1, 1), position(1, 2)]);
uiresume(get(axes, 'Parent'))