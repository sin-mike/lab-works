function x=calc_xaxis(xmin,xmax,xpts,xtype)

if (strcmp(qtype,'log'))
    xsteplog=(log(xmax)-log(xmin))/(xpoint-1);
    x=exp([log(xmin):xsteplog:log(xmax)]);
else  %default is linear binning
    xstep=(xmax-xmin)/(xpoint-1);
    x=[xmin:xstep:xmax];
end