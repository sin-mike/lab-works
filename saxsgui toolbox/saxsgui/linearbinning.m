function [Ibin,qbin,phibin]=linearbinning(I,q,phi,z)
%this is a binning function, simple binning...
%matrix I is binned so that it is reduced in size by a factor of z
%the q and phi values are averaged accordingly.

%first check whether input matrix I is square
% if size(I,1)~=size(I,2)
% 	error('binning input matrix not square!');
% %check whether the size of matrix a is a power of two
% elseif round(log2(size(I,1)))~=log2(size(I,1))
% 	error('size of input matrix is not a power if two');
% %check whether the factor z is a power of two (to prevent "leftovers")
% elseif round(log2(z))~=log2(z)
% 	error('reduction factor is not a power of two');
% end

%check whether the columns of I are divisible by z
if round(size(I,1)/z)~=size(I,1)/z
    error('error: the %d columns of the matrix are not divisible by the factor %d',size(I,1),z);
elseif round(size(I,2)/z)~=size(I,2)/z
    error('error: the %d rows of the matrix are not divisible by the factor %d',size(I,2),z);
end

Ibin=zeros(size(I,1)/z,size(I,2)/z);
qbin=Ibin;
phibin=qbin;
for i=1:(size(I,1)/z)
	for j=1:(size(I,2)/z)
		Ibin(i,j)=sum(sum(I(z*(i-1)+1:z*i,z*(j-1)+1:z*j)))/z^2;
        qbin(i,j)=sum(sum(q(z*(i-1)+1:z*i,z*(j-1)+1:z*j)))/z^2;
        %phibin(i,j)=sum(sum(phi(z*(i-1)+1:z*i,z*(j-1)+1:z*j)))/z^2; %this
        %causes problems when averaging over pixels of 360 and 0...
        %quickfix: max instead of sum. This will cause a slight rotation
        %though. 
        %fullfix: the qx and qy values should be averaged, together with
        %the intensities. Then, the q and phi values should be calculated.
        phibin(i,j)=max(max(phi(z*(i-1)+1:z*i,z*(j-1)+1:z*j)));
	end
end


