function    batch_timeseries_out=batch_timeseries_init
global batch_timeseries_temp
global last_open_how
% this routine provides initial parameters for the reduction parameter edit
% windows.
% Somewhere in the future it may be able to "know" what was previously
% entered in the last session...but for now it knows what was entered last
% in this session

if ~isempty(batch_timeseries_temp) 
    batch_timeseries_out=batch_timeseries_temp;
else
    
    a.UseIncrementalTime=1;
    a.UseTimeFromHeader=0;
    a.UseFiledSavedTime=0;

    if strcmp(last_open_how,'normal')
        a.UseMetaData=0;
    else
        a.UseMetaData=1;
    end
    
    a.sample_trans='1.0';
    a.Sample_thickness='1.0';
    
    a.EvalExpr1String='sum(Y(X>0.05 & X<0.2))'; 
    a.EvalExpr2String='max(Y(X>0.1 & X<0.2))';
    a.EvalExpr3String='sum(Y(X>0.002 & X<0.2))';
    a.EvalExpr4String='sum(Y(X>0.05 & X<0.2))';
    a.EvalExpr5String='sum(Y(X>0.05 & X<0.2))';
    
    a.EvalExpr1=0; 
    a.EvalExpr2=0;
    a.EvalExpr3=0;
    a.EvalExpr4=0;
    a.EvalExpr5=0;
    
    a.xyformat=0;
    a.xydeltayformat=1;
    a.ReducedDataToSameFile=0;
    a.ReducedDataToSeparateFiles=1;
    a.FileFormat='CSV';
    
    a.Save1DFigures=0;
    a.Save2DImages=0;
    
    a.DescriptiveTagString='Reduced';
    
    batch_timeseries_out=a;
    batch_timeseries_temp=batch_timeseries_out;
end
    