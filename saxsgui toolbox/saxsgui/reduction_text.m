function h=reduction_text(current_axes);

%now adding a text block describing the reduction state..but only
%if all reduction states are the same...
% first delete any prior
delete(findobj(current_axes,'Tag','Reduction Text'))
datalines=findobj(current_axes,'Tag','Data');
data_struct=get(datalines,'UserData');
same_reduction_state=1;
for ii=2:length(datalines)
    same_reduction_state=same_reduction_state*strcmp(data_struct{ii-1}.reduction_state,data_struct{ii}.reduction_state);
end
if same_reduction_state==1
    if length(datalines)==1
        s=data_struct;
    else
        s=data_struct{1};
    end
    redtext=actual_text(s);
else
    redtext=' Different Reductions applied \newline to the various lines';
end
x=get(current_axes,'Xlim');
y=get(current_axes,'Ylim');
xtextstart=x(1);
ytextstart=y(1);
h=text(xtextstart,ytextstart,redtext,'HorizontalAlignment','left','VerticalAlignment','bottom','FontSize',8);
set(h,'Tag','Reduction Text')

%also included a legend here
for ii=1:length(datalines)
    S=get(datalines(ii),'UserData');
    [pathstr,name,ext] = fileparts(S.raw_filename);
    legend_text{ii}=[name,ext];
end
lh=legend(datalines,legend_text);
%now setting the text-intepreter to be none...so that underscores are not
%interpreted by tex
set(findobj(get(lh,'Children'),'Type','text'),'Interpreter','none')


function redtext=actual_text(si);
% this function analyses the reduction state and constructs a text block
% that can then be displayed on the graphs.

redtext=' \newline';
if isempty(si.reduction_state)
    redtext=strcat(redtext,' No corrections applied');
end
if str2num(si.reduction_state(10))==1  % the mask was used
    redtext=strcat(redtext,' \bullet zingers removed \newline');
end

if str2num(si.reduction_state(9))==1  % the mask was used
    redtext=strcat(redtext,' \bullet readout-noise corrected \newline');
end

if str2num(si.reduction_state(8))==1  % the mask was used
    redtext=strcat(redtext,' \bullet masked \newline');
end

if str2num(si.reduction_state(7))==1 %if flatfield correction average was used.
    redtext=strcat(redtext,' \bullet flatfield corrected (averaged) \newline');
end

if str2num(si.reduction_state(6))==1 %if point-by-point flatfield correction average was used.
    redtext=strcat(redtext,' \bullet flatfield corrected (point-by-point) \newline');
end

if str2num(si.reduction_state(5))==1 % if sample transmission was used
    redtext=strcat(redtext,' \bullet transmission corrected \newline');
end

if str2num(si.reduction_state(4))==1 % if empty holder subtraction is to be used.
    redtext=strcat(redtext,' \bullet "emtpy holder" corrected \newline');
end
if str2num(si.reduction_state(11))==1 % if solvent subtraction is to be used.
    redtext=strcat(redtext,' \bullet "solvent" corrected \newline');
end

if str2num(si.reduction_state(3))==1 % if darkcurrent correction average is to be used.
    redtext=strcat(redtext,' \bullet dark current corrected \newline');
end

if str2num(si.reduction_state(2))==1 % if absolute intensity scaling factor is to be used
    redtext=strcat(redtext,' \bullet absolute intensity factor applied \newline');
end

if str2num(si.reduction_state(1))==1 % if sample thickness is to besed
    redtext=strcat(redtext,' \bullet thickness corrected \newline');
end

redtext=strcat(redtext,' \newline \newline');

