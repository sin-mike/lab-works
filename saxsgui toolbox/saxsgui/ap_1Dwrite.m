function fid=ap_1Dwrite(S)
% AP_1DWRITE(S,AP_FILE) write an autoprocessing file based on the information
% in the SAXS structure S
% created by KJ 080919

%first prompt for center file
[filename, pathname] = uigetfile('*.m;*.mat', 'Pick a previously saved center-file');
if isempty(filename)
   error('No center-file indicated or found')
else
   center_file=fullfile(pathname,filename);
   if ~exist(center_file,'file')
       error('No center-file found. Please create one from SAXSGUI main window')
   end
end

%then prompt for calibration file
[filename, pathname] = uigetfile('*.m;*.mat', 'Pick a previously saved calibration-file');
if isempty(filename)
   error('No auto-file indicated or found')
else
   calib_file=fullfile(pathname,filename);
   if ~exist(calib_file,'file')
       error('No calibration-file found. Please create one from SAXSGUI main window')
   end
end

%first prompt for a name for an autoprocessing file to save to
[filename, pathname] = uiputfile('*.m', 'Save AutoProcessing as..');
if isempty(filename)
    error('No Autoprocessing-file chosen')
else
    ap_filename=fullfile(pathname,filename);
    fid = fopen(ap_filename, 'wt');
    if fid == -1
        error('Could not open autoprocessing file.')
    end
end
       
      
fprintf(fid,'%s\n\n','% See full description of parameters at bottom of file');

% first reduction settings
fprintf(fid,'%s \n','%Reduction Info: if it has a value it will be included in the reduction');
fprintf(fid,'matlab_center_file = ''%s'' %%REQUIRED: must be created in SAXSGUI\n',center_file); 
fprintf(fid,'matlab_calib_file = ''%s'' %%REQUIRED: must be created in SAXSGUI\n',calib_file);

if isempty(S.flatfield_filename) || ~exist(S_template.flatfield_filename)
    presign='%';
else
    presign='';
end
fprintf(fid,'%sflatfield_filename= ''%s''\n',presign,S.flatfield_filename);

if isempty(S.abs_int_fact)
    presign='%';
else
    presign=''; 
end
fprintf(fid,'%sabs_int_fact= %d\n',presign,S.abs_int_fact);

if isempty(S.mask_filename)|| ~exist(S_template.mask_filename)
    presign='%';
else
    presign='';
end
fprintf(fid,'%smask_filename= ''%s''\n',presign,S.mask_filename);

if isempty(S.empty_filename) || exist(S_template.solvent_filename)
    presign='%';
else
    presign='';
end
fprintf(fid,'%sempty_filename= ''%s''\n',presign,S.empty_filename);
fprintf(fid,'%sempty_transfact= %d %%ideally should be included in the info file. \n',presign,S.empty_transfact);

if isempty(S.solvent_filename) || ~exist(S_template.solvent_filename)
    presign='%';
else
    presign='';
end
fprintf(fid,'%ssolvent_filename= ''%s''\n',presign,S.solvent_filename);
fprintf(fid,'%ssolvent_transfact= %d %%ideally should be included in the info file. \n',presign,S.solvent_transfact);
fprintf(fid,'%ssolvent_thickness= %d %%ideally should be included in the info file, or the info-files comment field \n',presign,S.solvent_thickness);

if isempty(S.transfact)
    presign='%';
else
    presign='';
end
fprintf(fid,'%stransfact= %d %%ideally should be included in the info file \n',presign,S.transfact);

if isempty(S.sample_thickness)
    presign='%';
else
    presign='';
end
fprintf(fid,'%ssample_thickness= %d %%ideally should be included in the info file \n\n',presign,S.sample_thickness);

%now for generic info
fprintf(fid,'%s\n','%Generic Info:\n');

if isempty(S.error_norm_fact)
    numval=1;
else
    numval=S.error_norm_fact;   
end
fprintf(fid,'error_calib= %d %%Should be 1 for photon counting detectors \n',numval);

if isempty(S.darkcurrent_filename)
    presign='%';
else
    presign='';
end
fprintf(fid,'%sdarkcurrent_filename= ''%s''\n',presign,S.darkcurrent_filename);

if isempty(S.readoutnoise_filename)
    presign='%';
else
    presign='';
end
fprintf(fid,'%sreadoutnoise_filename= ''%s''\n',presign,S.readoutnoise_filename);

if isempty(S.zinger_removal)
    numval=0;
else
    numval=S.zinger_removal;   
end
fprintf(fid,'zinger_removal= %d %%Should be 0 for Molmet detectors \n\n',numval);

%now averaging info
fprintf(fid,'%s\n','%Averaging Info - this section is optional:\n');

fprintf(fid,'do_average= %d \n',1); % default is 1

if strcmp(S.type,'r-ave')
    numval=1;
else
    numval=0;   
end
fprintf(fid,'I_vs_q= %d %% 1=do I_vs_q, 0=do I_vs_phi. Default is I_vs_q  \n',numval);


fprintf(fid,'phistart= %d %% azimuthal start angle in degrees \n',S.yaxis(1));
fprintf(fid,'phiend= %d %% azimuthal end angle in degrees\n',S.yaxis(2));
fprintf(fid,'qstart= %d %% radial start value \n',S.xaxis(1));
fprintf(fid,'qend= %d %% radial end value \n',S.xaxis(2));

fprintf(fid,'xaxis_type= ''%s'' %% can be ''lin'' or ''log''\n',S.xaxisbintype);
fprintf(fid,'num_xpoints= %d %% number of points on the x-axis \n',length(S.xaxisfull));
fprintf(fid,'reduction_type= ''%s'' %% can be default=''s'' or ''log''\n',S.reduction_type);
fprintf(fid,'reduction_text= 1 %% =1 means include reduction text, =0 means do not include \n\n');

fprintf(fid,'%%Output Info - This section is optional- Comment out to ''stop'' at 1D-plot figure window.\n');
fprintf(fid,'do_output=1       %% value=1 means output is desired, value=0 no output.\n');
fprintf(fid,'new = 1	       %% 1= plots in new window, 0=plots in existing window.\n');
fprintf(fid,'print= 0       %% 1= print result on the printer.\n');
fprintf(fid,'print1D2jpeg=1   %% 1= save 1D figure in a jpeg file.\n');
fprintf(fid,'%%print2D2jpeg=1   %% 1= save 2D saxsgui-window in a jpeg file. Does not work nicely.\n');
fprintf(fid,'save2fig=1     %% 1= save figure to matlab figure_file.\n');
fprintf(fid,'save2columns=0 %% 1= Saves data in commaseparated variable q/phi,I; 0= does not save.\n');
fprintf(fid,'save3columns=1 %% 1= Saves data in comma separated variable q/phi,I,dI; 0=does not save.\n');
fprintf(fid,'addon_name=''saxsgui'' %% saved files have these additional extensions.\n');
fprintf(fid,'%%output_dir=''C:\\reduced'' %% if not given output_dir will be directed to the current matlab directory .\n \n');

fprintf(fid,'%%---------------------------DESCRIPTION--------------------------------\n');
fprintf(fid,'%% This file contains the variables required to do various degrees of \n');
fprintf(fid,'%% auto-processing in SAXSGUI. It can be applied without changes to both\n');
fprintf(fid,'%% single file processing and multiple file processing(averaging only for\n');
fprintf(fid,'%% now)\n');
fprintf(fid,'%%\n');
fprintf(fid,'%% The variables are divided into 2 sections\n');
fprintf(fid,'%% 1) Reduction Info relevant for any particular setup of the existing SAXS\n');
fprintf(fid,'%% instrument. Will need to be changed when the setup is changed.\n');
fprintf(fid,'%% 2) Reduction parameters that might change for different SAXS systems.\n');
fprintf(fid,'%% \n');
fprintf(fid,'%% This file may optionally include information on\n');
fprintf(fid,'%% 3) How to average the data\n');
fprintf(fid,'%% 4) What to do with the averaged data\n');
fprintf(fid,'%%\n');
fprintf(fid,'%% Only q-calibration and centering are absolutely required. If other \n');
fprintf(fid,'%% reductions are desired they must be included herein. If they are excluded\n');
fprintf(fid,'%% the relevant reduction will not be performed. \n');
fprintf(fid,'%% \n');
fprintf(fid,'%% The information in this file complements the information that is saved\n');
fprintf(fid,'%% in the data info files. Any information given in the info-file always\n');
fprintf(fid,'%% overrides information given in this setup-file. \n');
fprintf(fid,'%% Presently only the mpa-info files are supported\n');
fprintf(fid,'%%------------------------ DESCRIPTION ----------------------------\n');
