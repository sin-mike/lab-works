function Icorr = net_corrected_scattering_intensity( ...
    I_s, ...        % measured scattering intensities (array) of sample
    I_empty, ...    % measured scattering intensities (array) of empty sample
    I_dc, ...       % measured scattering intensities (array) of dark current
    time_s, ...     % sample measurement time, sec
    time_empty, ... % empty sample measurement time, sec
    time_dc, ...    % dark current measurement time, sec
    trans_s, ...    % sample transmission coefficient (see TRANSCOEFF)
    thickness, ...  % sample thickness, mm
    Sens, ...       % pixel sensitivity correction factor (array)
    f_abs)          % absolute intensity conversion factor

%NET_CORRECTED_SCATTERING_INTENSITY Correct intensities.
%   Icorr = NET_CORRECTED_SCATTERING_INTENSITY( ...
%     I_s, ...        % measured scattering intensities (array) of sample
%     I_empty, ...    % measured scattering intensities (array) of empty sample
%     I_dc, ...       % measured scattering intensities (array) of dark current
%     time_s, ...     % measurement time, sec, of sample 
%     time_empty, ... % measurement time, sec, of empty sample 
%     time_dc, ...    % measurement time, sec, of dark current (no beam)
%     trans_s, ...    % sample transmission coefficient (see TRANSCOEFF)
%     thickness, ...  % sample thickness, mm
%     Sens, ...       % pixel sensitivity correction factor (array)
%     f_abs)          % absolute intensity conversion factor
%
%   ... incorporates all the array and scalar arguments into a formula to
%   produce an array of intensity values corrected for measurement time,
%   dark-current intensity, blank (empty sample) intensity, sample
%   transmissivity, detector pixel sensitivity, and an absolute intensity
%   conversion factor derived from standard samples.

Icorr = ((I_s - time_s / time_dc * I_dc) ...
    - (trans_s * (I_empty * time_s / time_empty - time_s / time_dc * I_dc))) ...
    / time_s ...
    ./ (trans_s * thickness * Sens * f_abs);
