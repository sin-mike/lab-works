function [Fitparam,Fitval,Info] = fitsaxs2D_smeared(si,fitpar,bounds)

%this function is virtually the same as its parent, fitsaxs2D.m However,
%here, for testing purposes, a smearing has been built in. modifications
%have been preambled by a %MOD tag. -- Brian Pauw, 11-01-07
%SAXS/FITSAXS Fits SAXS image intensities either in 1 or 2 D.
%   FITSAXS(S,fitpar,bounds) returns the fit-parameters in a vector, 
%   the Fit in a structure and any additional information 
%
%   The fitpar should have a structure of the form:
%fitpar = 
%
%        name: 'orientationdistribution.m'
%     binning: 1
%         fit: [1 1 1 1 1]
%       start: [1 2 3 4 5]
%     numiter: 100
%         tol: 1.0000e-003
%    graphics: 1
%
%   This routine uses an approach provided by Brian R. Pauw October 2006
%   and has as it's core the bounded minimum search mechanism 
%   (fminsearchbnd version 4) publicly
%   available at the Matlab Central and written by Author: John D'Errico
%   E-mail: woodchips@rochester.rr.com
%
%   Note: for now binning is not implemented.
%

%MOD1: get the smearing parameters and set them global so every function
%can use them.
global sigma_d1 sigma_d2 sigma_w sigma_c1 sigma_c2 q_limit npixels smear_shape A q_use
global error_norm_fact
[sigma_d1,sigma_d2,sigma_w,sigma_c1,sigma_c2,q_limit,npixels]=resolution_input();
error_norm_fact=si.error_norm_fact;
%ENDMOD

% Let's give some name we can understand
func=fitpar.name(1:end-2); %removing the .m
x0=fitpar.start;
for ii=1:length(fitpar.fit)
    if fitpar.fit(ii)==0  %values are completely constrained
        xl(ii)=x0(ii); %lower bounds
        xu(ii)=x0(ii); %higher bounds
    else
        xl(ii)=max(-Inf,fitpar.ll(ii)); %lower bounds
        xu(ii)=min(Inf,fitpar.ul(ii));  %higher bounds
    end
end
qmin=bounds(1);
qmax=bounds(1)+bounds(3);
phimin=bounds(2);
phimax=bounds(2)+bounds(4);

%convert image pixels locations to angle phi and angle q
[q,phi]=create_qphi_gen(si.pixels,si.raw_center(2),si.raw_center(1),si.kcal/si.pixelcal(2),si.kcal/si.pixelcal(1),si.wavelength);

%MOD5: linear binning integrated. Great..thanks Brian!
if fitpar.binning~=1    
    [I,q,phi]=linearbinning2(si.pixels,q,phi,fitpar.binning);
end

%determine fitting relevant data
%relevant_data_indices_qphi=find(qmin < q && q < qmax & phimin < phi && phi < phimax);;

q_use=zeros(size(q,1),size(q,2));
%MOD4: this keeps the shape of the matrices:  OK is this necessary?
for i=1:size(q,1)
    for j=1:size(q,2)
        if (qmin < q(i,j) && q(i,j) < qmax && phimin < phi(i,j) && phi(i,j) < phimax)
            q_use(i,j)=1;
        else
            q_use(i,j)=NaN;
        end
    end
end


%now confine the data to be only what we are interested in
% Iobs=si.pixels(relevant_data_indices_qphix,relevant_data_indices_qphiy);
% qs=q(relevant_data_indices_qphix,relevant_data_indices_qphiy);
% phis=phi(relevant_data_indices_qphix,relevant_data_indices_qphiy); 
%MOD4: also reconstruct the matrix.... time consuming...
%if Iobs is projected onto q_use, as well as Icalc, the merit function
%could still be calculated without problems. It is better, however, to crop
%the functions.
%         npixels=24;%this defines the number of pixels around all other pixels that contribute to the smearing. This is used instead of the cutoff value, to speed up the algorithm. to keep it square, use 8 or 24 pixels. values of 4, 8, 12, 20 and 24 are allowed.
Iobs=si.pixels;
qs=q;
phis=phi;

%MOD3: there is now enough information to calculate the smearing matrix if
%requested
if fitpar.incl_resolution==0
    smears=0;
else
    [smear_shape,q_use,A]=resolution_v3(qs,phis,sigma_d1,sigma_d2,sigma_w,sigma_c1,sigma_c2,q_limit,npixels,q_use);
    smears.sigma_d1=sigma_d1;
    smears.sigma_d2=sigma_d2;
    smears.sigma_w=sigma_w;
    smears.sigma_c1=sigma_c1;
    smears.sigma_c2=sigma_c2;
    smears.q_limit=q_limit;
    smears.npixels=npixels;
    smears.q_use=q_use;
    smears.smear_shape=smear_shape;
    smears.A=A;
end
%ENDMOD
%first create the appropriate handle definition of the Merit-function
%for now it is the unweighted chi-square merit

meritfunc=@(x) chisqr(func,Iobs,qs,phis,smears,fitpar,x) ; %MOD smears added


[Fitparam,Fitval,exitflag,output] = ...
    fminsearchbnd(meritfunc,x0,xl,xu,optimset('MaxFunEvals',fitpar.numiter,'MaxIter',fitpar.numiter,'Tolx',fitpar.tol));

fitfun=str2func(func);
Ifit=fitfun(qs,phis,Fitparam);
Info=[];
%if it's no good...
if exitflag==0
    %uiwait(warndlg('maximum number of iterations reached'));
    display(output.message);
end
clearstatus
hstatus=figstatus('Preparing to plot results....please be patient');

fitfig=plot2D_fitresults(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,qmin,qmax,phimin,phimax);
delete(hstatus)



function fitfig=plot2D_fitresults(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,qmin,qmax,phimin,phimax)

fitfig=findobj(get(0,'Children'),'Name','Results of 2D fitting');
if isempty(fitfig)
    fitfig=figure;
    set(gcf,'Name','Results of 2D fitting')
end
% outputmenu = uimenu('Label', 'Save...');
% figmenu=uimenu(outputmenu, 'Label', 'As Matlab figure', 'Callback', ...
%     {@SaveFig, fitfig});
% mfilemenu=uimenu(outputmenu, 'Label', 'As M-file', 'Callback', ...
%     {@SaveMfile, fitfig});
% mfilemenu=uimenu(outputmenu, 'Label', 'As Ascii-file', 'Callback', ...
%     {@SaveAscii, fitfig});

fitplotcontent(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)


% This function creates the plot window and plots the result.

function hfig=fitplotcontent(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax) 

% for speed we interpolate the data and fit on a smaller grid 
% 50x50 rather than the full grid (often 1k x 1k)
%griddata may produce NAN on the edges.. .therefore we start by making
% the matrixes larger by 1 value
qstep=(qmax-qmin)/50;
x=max(0,qmin-qstep):qstep:qmax+qstep;
phistep=(phimax-phimin)/50;
y=max(0,phimin-phistep):phistep:min(360,phimax+phistep);

[XI,YI]=meshgrid(x,y);
warning off all
%griddata cubic may produce negatively valued data so we use linear
ZIobs=griddata(qs,phis,Iobs,XI,YI,'linear');
ZIfit=griddata(qs,phis,Ifit,XI,YI,'linear');
ZIres=ZIobs-ZIfit;
warning on all
%griddata may produce NAN on the edges.. .therefore we remove the edges
ZIobs=ZIobs(2:end-1,2:end-1);
ZIfit=ZIfit(2:end-1,2:end-1);
ZIres=ZIres(2:end-1,2:end-1);
XI=XI(2:end-1,2:end-1);
YI=YI(2:end-1,2:end-1);

Zmax=max(max(ZIobs(:)),max(ZIfit(:)));
Zmin=min(min(ZIobs(:)),min(ZIfit(:)));
Zmin=max(Zmax/10000,Zmin);
Zresmin=min(ZIres(:));
Zresmax=max(ZIres(:));

figure(fitfig) 
subplot(2,2,1)
mesh(XI,YI,ZIobs)
set(gca,'Zlim',[Zmin Zmax])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Data')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Azimuthal Angle (degrees)')

subplot(2,2,3)
mesh(XI,YI,ZIfit)
set(gca,'Zlim',[Zmin Zmax])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Modelled data')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Azimuthal Angle (degrees)')

subplot(2,2,2)

mesh(XI,YI,ZIres)
set(gca,'Zlim',[Zresmin Zresmin+Zmax-Zmin])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Residuals (linear scale) ')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Azimuthal Angle (degrees)')

subplot(2,2,4)
contour(XI,YI,ZIres)
set(gca,'Zlim',[Zresmin Zresmin+Zmax-Zmin])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Residuals (linear scale) ')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Azimuthal Angle (degrees)')


%and here we define the function that needs to be minimized, basically
%differently weighted residual
function cs=chisqr(func,I,qs,phis,smears,fitpar,varargin)
global error_norm_fact

warning off all;
func=str2func(func);
warning on all;
fitvalues=varargin{1};
if iscell(fitvalues)
    fitvalues=fitvalues{1};
end
Icalc=func(qs,phis,fitvalues); %must be of similar shape as the original...

%MOD2: the Icalc will be smeared using the input parameters as obtained
%before. ----> SLOOOOOW!
if fitpar.incl_resolution==1
    Icalc=smear_v2(qs,phis, smears.sigma_d1, smears.sigma_d2, smears.sigma_w, smears.sigma_c1, smears.sigma_c2, smears.q_use, smears.smear_shape,Icalc, smears.A);
end
    
warning off all;
%MOD6: this will prevent too much from going wrong here. remember that
%"smear" actually NaN's all values of non-interest.
indextogo=find(isfinite(Icalc)==1);

if isfield(fitpar,'weighting')==0
    fitparam.weighting='Rel. Std. Dev.';
end
if strcmp(fitpar.weighting,'Std. Dev.')
    cs=sum(sum(Icalc(indextogo)-I(indextogo)).^2)/(size(Icalc(indextogo),1)*size(Icalc(indextogo),2)-length(fitvalues)); 
elseif strcmp(fitpar.weighting,'Rel. Std. Dev.');
    cs=sum(sum((Icalc(indextogo)-I(indextogo))./I(indextogo)).^2)/(size(Icalc(indextogo),1)*size(Icalc(indextogo),2)-length(fitvalues));
elseif strcmp(fitpar.weighting,'Weighted \Chi^2');
    cs=sum(sum((Icalc(indextogo)-I(indextogo)).^2./(max(I(indextogo),1)*error_norm_fact)))/(size(Icalc(indextogo),1)*size(Icalc(indextogo),2)-length(fitvalues));
end
countstatus(fitvalues,cs)
warning on all;

function countstatus(fitvalues,cs)
persistent ii oldtic
%this is a hack that will keep check on when the last calculation 
% was performed. If it is roughly more than 5 seconds ago we will assume ti
% was a previous optimization
newtic=tic;
if double(newtic)-double(oldtic)< 18000000
    ii=ii+1;
else
    ii=1;
end
oldtic=newtic;

clearstatus
hstatus=figstatus(['Function calculation #',num2str(ii),':  ',...
    num2str(fitvalues),'  ','Merit:',num2str(cs,3)]);
drawnow


function clearstatus
fig=findobj(get(0,'Children'),'Name','Fitting2D');
%delete any old status message.
hstatusold=findobj(fig,'Tag','statusline');
if ~isempty(hstatusold)
    delete(hstatusold)
end
