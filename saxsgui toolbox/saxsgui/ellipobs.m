function obs = ellipobs(eta,img,a,b,th,cut)
  strimg = stretche2(img,b,a,th,eta);
  ev = svd(strimg(:,cut:end));
  obs = -sum(ev.^4)/sum(ev)^4;
  %obs = -sum(ev(1:4))/sum(ev);
