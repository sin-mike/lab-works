function [R, kim_out, kbkg_out] = isubtract(im, bkg, kim, kbkg)
%ISUBTRACT Interactively subtract SAXS images.
%   ISUBTRACT(IM, BKG) returns a SAXS object after subtracting SAXS objects
%   IM minus BKG.  During subtraction you interact with a dialog window to
%   set scale factors, that is, Kim * IM - Kbkg * BKG.  The dialog shows both
%   images and the resulting subtraction.  If you close or cancel the
%   dialog, ISUBTRACT returns an empty array.
%
%   ISUBTRACT(IM, BKG, KIM, KBKG) sets the starting values for the scale
%   factors.  Otherwise they both start at 1.
%
%   [S, KIM, KBKG] = ISUBTRACT(...) returns the scale factors along with
%   the SAXS object resulting from the subtraction.

% Analyze arguments.
if nargin < 2 || ~(isa(im, 'saxs') && isa(bkg, 'saxs'))
    error('I need two SAXS image arguments.')
end
% Okay, IM and BKG are good.
if nargin > 2
    if ~(nargin == 4 && isscalar(kim) && isscalar(kbkg))
        error('Arguments 3 and 4 must be scale factors.')
    end
else  % Use default scale factors.
    kim = 1;
    kbkg = 1;
end

%setting scale parameters of bkg to the same as im
bkg=calibrate(bkg,im.kcal,im.pixelcal,im.koffset,[],[],[],'std');
bkg=setcenter(bkg,im.raw_center);

%
gui = makeisubtract(im, bkg, kim, kbkg);
guidata(gui.figure1, gui)
uiwait(gui.figure1)

% Is our figure still here?
try
    gui = guidata(gui.figure1);
    R =  gui.im * get(gui.k1, 'Value') ...
    - gui.bkg * get(gui.k2, 'Value');
    if nargout == 3
        kim_out = get(gui.k1, 'Value');
        kbkg_out = get(gui.k2, 'Value');
    end
    close(gui.figure1)
catch  % probably our figure was closed or cancelled
    R = [];
    if nargout == 3
        kim_out = [];
        kbkg_out = [];
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui = makeisubtract(im, bkg, kim, kbkg)
gui = guihandles(openfig('isubtract.fig', 'new'));
gui.im = im;
gui.bkg = bkg;
set(gui.k1, 'String', num2str(kim))
set(gui.k2, 'String', num2str(kbkg))
set(gui.k1, 'Value', kim)  % save values
set(gui.k2, 'Value', kbkg)
cmin = min(im(:));
cmax = max(im(:));
set(gui.cmin, 'String', num2str(cmin))
set(gui.cmax, 'String', num2str(cmax))
set(gui.cmin, 'Value', cmin)
set(gui.cmax, 'Value', cmax)
set(gui.imrealtime, 'String', num2str(im.realtime, '%.1f'))
set(gui.imlivetime, 'String', num2str(im.livetime, '%.1f'))
set(gui.bkgrealtime, 'String', num2str(bkg.realtime, '%.1f'))
set(gui.bkglivetime, 'String', num2str(bkg.livetime, '%.1f'))
isubimage(gui.axes1, im, gui)
isubimage(gui.axes3, bkg, gui)
difference(gui)
set([gui.k1 gui.k2 gui.cmin gui.cmax], 'Callback', @numedit_callback)
set(gui.OK, 'Callback', @OK_callback)
set(gui.Cancel, 'Callback', @Cancel_callback)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function is = isscalar(a)
is = isnumeric(a) && isfinite(a) && length(a) == 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function isubimage(ax, I, gui)
axes(ax)
cmin = get(gui.cmin, 'Value');
cmax = get(gui.cmax, 'Value');
% Hack alert!
if cmax <= cmin
    cmax = cmin + 1;
end
image(I, [cmin, cmax])
delete(cell2mat(get(ax, {'XLabel', 'YLabel', 'Title'})))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function difference(gui)
% Compute and show difference.
dif = gui.im * str2double(get(gui.k1, 'String')) ...
    - gui.bkg * str2double(get(gui.k2, 'String'));
isubimage(gui.axes2, dif, gui)
isubimage(gui.axes1,gui.im*str2double(get(gui.k1, 'String')),gui)
isubimage(gui.axes3,gui.bkg*str2double(get(gui.k2, 'String')),gui)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function numedit_callback(control, event)
% Check a numeric edit box and apply it.
gui = guidata(control);
k = str2double(get(control, 'String'));
if isnan(k)  % not a number
    errordlg('Scale factor must be a number.', 'Not a number', 'modal')
    set(control, 'String', num2str(get(control, 'Value')))
else
    set(control, 'Value', k)
    difference(gui)  % update the difference image
    set([gui.axes1, gui.axes2, gui.axes3], 'CLim', ...
        [get(gui.cmin, 'Value'), get(gui.cmax, 'Value')])
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function OK_callback(button, event)
gui = guidata(button);
uiresume(gui.figure1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Cancel_callback(button, event)
gui = guidata(button);
close(gui.figure1)
