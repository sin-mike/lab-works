function outnum=invoke_maskmaker_function
% this functions makes sure that all the "standard" 
% maskmaker functions are called. 

%standard maskmaker

create_circle_wbmf(-1)
create_slice_wbmf(-1)
edit_rect_wbdf(-1)
edit_rect_wbmf(-1)
edit_circle_wbdf(-1)
edit_circle_wbmf(-1)
edit_slice_wbdf(-1)
edit_slice_wbmf(-1)
outnum=1;




