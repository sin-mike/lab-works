function axy = im2axy(im, imrow)
%IM2AXY Convert image row numbers to y axis values.
%   AXY = IM2AXY(IM, IMROW) converts an array of row numbers
%   IMROW from image IM to an array of y axis values AXY.

% Assume our arguments are sane.

ydata = get(im, 'YData');
nrows = size(get(im, 'CData'), 1);
if length(ydata) == 2
    ydata = [ydata(1) : (ydata(2) - ydata(1)) / (nrows - 1) : ydata(2)];
end
axy = interp1([1 : nrows], ydata, imrow);
