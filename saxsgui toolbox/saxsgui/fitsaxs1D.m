function [Fitparam,Fitval,Info] = fitsaxs1D(si,bounds,handles)

%SAXS/FITSAXS Fits SAXS image intensities either in 1 
%   FITSAXS(S,fitpar,bounds) returns the fit-parameters in a vector,
%   the Fit in a structure and any additional information
%
%   The fitpar should have a structure of the form:
%fitpar =
%
%        name: 'orientationdistribution.m'
%     binning: 1
%         fit: [1 1 1 1 1]
%       start: [1 2 3 4 5]
%          ll: [0 1 2 3 4]
%          ul: [2 3 4 5 6]
%     numiter: 100
%         tol: 1.0000e-003
%    graphics: 1
%  param_name: alpha beta gamma chi omega
%
%   This routine uses an approach provided by Brian R. Pauw October 2006
%   and has as it's core the bounded minimum search mechanism
%   (fminsearchbnd version 4) publicly
%   available at the Matlab Central and written by Author: John D'Errico
%   E-mail: woodchips@rochester.rr.com
%
%   Note: for now binning is not implemented.
%
% Let's give some name we can understand
global fitpar
func=fitpar.name(1:end-2); %removing the .m
p0=fitpar.start;
pl(length(fitpar.fit))=0;
pu(length(fitpar.fit))=0;
for ii=1:length(fitpar.fit)
    if fitpar.fit(ii)==0  %values are completely constrained
        pl(ii)=p0(ii); %lower bounds
        pu(ii)=p0(ii); %higher bounds
    else
        pl(ii)=max(-Inf,fitpar.ll(ii)); %lower bounds
        pu(ii)=min(Inf,fitpar.ul(ii));  %higher bounds
    end
end
xmin=bounds(1);
xmax=bounds(2);

%determine fitting relevant data
relevant_data_indices_x=find(xmin < si.xaxisfull & si.xaxisfull < xmax);

%now confine the data to be only what we are interested in
Iobs=si.pixels(relevant_data_indices_x);
Eobs=Iobs.*si.relerr(relevant_data_indices_x);
xs=si.xaxisfull(relevant_data_indices_x);
fitfun=str2func(func);
if isnan(fitpar.start(1))  % this means we need to come up with estimates
    fitfun(xs,Iobs,fitpar.start);
    Fitparam=[];
    Fitval=[];
    Info=[];
else
    %first create the appropriate handle definition of the Merit-function
    %for now it is the weighted chi-square merit

    meritfunc=@(x) chisqr(func,Iobs,Eobs,xs,x) ;


    [Fitparam,Fitval,exitflag,output]=...
        fminsearchbnd(meritfunc,p0,pl,pu,optimset('MaxFunEvals',fitpar.numiter,'MaxIter',fitpar.numiter,'Tolx',fitpar.tol));

    Ifit=fitfun(xs,Iobs,Fitparam);
    Info=[];
    %if it's no good...
    if exitflag==0
        %output.message='maximum number of iterations reached';
        %uiwait(warndlg('maximum number of iterations reached'));
        display(output.message);
    end
    clearstatus
    if fitpar.graphics==1
        hstatus=figstatus('Preparing to plot results....please be patient');
        fitfig=plot1D_fitresults(handles,Fitparam,Fitval,exitflag,output,Ifit,Iobs,Eobs,xs,xmin,xmax);
        if ishandle(hstatus)
            delete(hstatus)
        end
        clearstatus
    end
end



function fitfig=plot1D_fitresults(handles,Fitparam,Fitval,exitflag,output,Ifit,Iobs,Eobs,xs,xmin,xmax);
warning off
%plotting first data and fit
if isfield(handles,'data')
    XScaleold=get(handles.data,'Xscale');
    YScaleold=get(handles.data,'Yscale');
else
    XScaleold=get(handles.fit,'Xscale');
    YScaleold=get(handles.fit,'Yscale');
end
fitfig=plot(handles.fit,xs,Iobs,xs,Ifit);
set(handles.fit,'XScale',XScaleold,'YScale',YScaleold)
%and now plotting the residuals
errorbar(handles.residuals,xs,Iobs-Ifit,Eobs)
set(handles.residuals,'XScale',XScaleold)
drawnow

axes(handles.residuals)
hold on
plot(xs,xs*0,'k-')
set(handles.residuals,'Xlim',get(handles.fit,'Xlim'))
hold off
warning on

%and here we define the function that needs to be minimized, basically an
%Chi-square function.
function cs=chisqr(func,I,deltaI,xs,varargin)
warning off all;
func=str2func(func);
warning on all;
fitvalues=varargin{1};
if iscell(fitvalues)
    fitvalues=fitvalues{1};
end
Icalc=func(xs,I,fitvalues); %Changed PMD 2007 to include I
warning off all;
cs=sqrt(sum(((Icalc-I)./deltaI).^2)/length(I));

%countstatus(fitvalues,cs)
warning on all;

function countstatus(fitvalues,cs)
persistent ii oldtic
%this is a hack that will keep check on when the last calculation
% was performed. If it is roughly more than 5 seconds ago we will assume ti
% was a previous optimization
newtic=tic;
if double(newtic)-double(oldtic)< 18000000
    ii=ii+1;
else
    ii=1;
end
oldtic=newtic;

clearstatus
hstatus=figstatus(['Function calculation #',num2str(ii),':  ',...
    num2str(fitvalues),'  ','Merit:',num2str(cs,3)]);
drawnow


function clearstatus
fig=findobj(get(0,'Children'),'Name','Fitting1D');
%delete any old status message.
hstatusold=findobj(fig,'Tag','statusline');
if ~isempty(hstatusold)
    delete(hstatusold)
end

