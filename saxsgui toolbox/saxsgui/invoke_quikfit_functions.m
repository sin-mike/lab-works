function outnum=invoke_quikfit_functions
% this functions makes sure that all the "standard" 
% quikfit functions are called. 

%standard quikfit

QuikGuinier(0,0,([0,0]));
QuickFract(0,0,([0,1,0]));
QuickPorodFluct(0,0,([0,10,3]));
QuickPorod(0,0,([0]));
QuikBeaucage(0,0,([0,100,0,1]));

outnum=1;




