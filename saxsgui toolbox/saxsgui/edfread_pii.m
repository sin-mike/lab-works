% function im=edfread(filename)
% reads an image in edf format into the matrix im
% returns -1 if reading was not successful
% uses the information in the header (Dim_1,Dim_2,DataType)
% please don't specify dimensions when calling edfread
% datatype can be UnsignedByte, UnsignedShort, UnsignedInteger=UnsignedLong, Float=Real, DoubleValue
% 
% origin: peter 
%
% see also: EDFWRITE

function [im,hd]=edfread(filename,varargin)

headerlength=1024;

if nargin>1
	rows = varargin{1};
	columns = varargin{2};
end

fid=fopen(filename,'r','l');

if fid==-1
	im=fid;
	disp(sprintf('!!! Error opening file %s !!!',filename))
else
	hd=readedfheader(fid);
    
    byteorder=edf_findheader(hd,'ByteOrder','string');
%     fidpos=ftell(fid); % store present position in file
%     fclose(fid); 
%     if strcmp(byteorder,'HighByteFirst')
%         byteorder='b';
%     else
%         byteorder='l';
%     end
%     fid=fopen(filename,'r',byteorder); % re-open with good format
%     fseek(fid,fidpos,0); % re-position at end of header
    
    date = edf_findheader(hd,'Date','whole_string');
    xsize=edf_findheader(hd,'Dim_1','integer');
	ysize=edf_findheader(hd,'Dim_2','integer');
	datatype=edf_findheader(hd,'DataType','string');
	switch datatype
		case 'UnsignedByte',
			datatype='uint8';
			nbytes=1;
		case 'UnsignedShort',
			datatype='uint16';
			nbytes=2;
		case {'SignedInteger','UnsignedInteger','UnsignedInt','UnsignedLong'}
			datatype='uint32';
			nbytes=4;
		case {'Float','FloatValue','Real'}
			datatype='float32';
			nbytes=4;
		case 'DoubleValue'
			datatype='float64';
			nbytes=8;
%etcetera
	end
	
	if isempty(who('rows'))
		rows=1:xsize;
	end
	if isempty(who('columns'))
		columns=1:ysize;
    end
	
    file_offset = nbytes*(rows(1)-1+(columns(1)-1)*xsize);
    if (file_offset ~= 0)
    	fseek(fid,file_offset,0);
    end
	im=fread(fid,[length(rows),length(columns)],sprintf('%d*%s',length(rows),datatype),nbytes*(xsize-length(rows)));
	im = flipud(im');
    st=fclose(fid);
end


function hd=readedfheader(fid)

headerlength = 1024;
closing = '}';
hd = fscanf(fid,'%c',headerlength);

while not(strcmp(hd(end-1),closing))
	hd = [hd fscanf(fid,'%c',headerlength)];
end