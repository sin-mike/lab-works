function str = scale(s)
%SAXS/SCALE Obtain image intensity scaling for SAXS image object.
%   SCALE(S) returns a string describing the image intensity scaling for
%   SAXS image object S.  Normal values are [] for linear, 'log', or 'log10'.

str = s.log;
