function s=calc_rel_error(s,NumOfPix)
% SAXS/CALC_REL_ERROR Calculate relative error
if s.detectortype=='FujiIP'
    warndlg('there is no error-calculation for fuji-imageplates')
    s.relerr=s.pixels*'NaN'; % we try with Nan and see what it does
else
    s.relerr=1./sqrt(NumOfPix.*s.pixels); %calculates relative error 
end