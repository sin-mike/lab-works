function s = plus(arg1, arg2) 
%SAXS/PLUS Add saxs image objects.
%   One or both of the arguments is a saxs image object.
%   The result is a saxs image object after array addition
%   of the pixels.  One argument may be a scalar value.
%   Otherwise the dimensions of the (pixel or other) arrays
%   must match.

sum = double(arg1) + double(arg2);

% If that worked we have valid arguments.

if isa(arg1, 'saxs') 
    s = arg1;
    pixdata1=s.pixels;
    realtime1=s.realtime;
    livetime1=s.livetime;
else 
    pixdata1=arg1;
    realtime1=0;
    livetime1=0;
end

if isa(arg2, 'saxs') 
    s = arg2;
    pixdata2=s.pixels;
    realtime2=s.realtime;
    livetime2=s.livetime;
else 
    pixdata2=arg2;
    realtime2=0;
    livetime2=0;
end
if isa(arg1, 'saxs')
    s.history = strvcat(s.history, logarg('plus', arg2, inputname(2)));
else  % arg2 must be a saxs object
    s.history = strvcat(s.history, logarg('plus', arg1, inputname(1)));
end

s.pixels = pixdata1+pixdata2;
s.raw_pixels = pixdata1+pixdata2;
s.realtime = realtime1+realtime2;
s.livetime = livetime1+livetime2;
