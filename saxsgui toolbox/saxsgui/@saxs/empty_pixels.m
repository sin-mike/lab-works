function so=empty_pixels(si,pixels); 
%SAXS/EMPTY_PIXELS this function simply inserts empty holder images 
%in the SAXS structure.
% so=empty_pixels(si,pixels)
so=si;
so.empty_pixels=pixels;

