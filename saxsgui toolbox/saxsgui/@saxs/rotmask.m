function obj_out = rotmask(obj)
%ROTMASK Mask a circular band for image rotation.
%   ROTMASK(S) zeroes out inner and outer areas of SAXS image object S,
%   leaving intensity data in a symmetrical, circular band around the
%   center.  The inner edge of the band is S.cmask_radius pixels away from
%   the center.  The outer edge is S.cmask_outer or, if s.cmask_outer is 
%   empty, as far away from the center as possible without the circle
%   extending beyond the image.

% Find the effective image center in row, column coordinates.
% Make it on the nearest half pixel so pixel radii are symmetric.
pixelcenter = pixelfrac(obj, [0 0]);
pixelcenter = fix(pixelcenter) + 0.5;

% Compute pixel coordinates relative to effective center.
[X Y] = meshgrid([1 : size(obj.pixels, 2)], [1 : size(obj.pixels, 1)]);
X = X - pixelcenter(1);
Y = Y - pixelcenter(2);

% Compute radii.
radii = sqrt(X .^ 2 + Y .^ 2);

% Figure outer radius of band.
outer = obj.cmask_outer;
if isempty(outer)  % figure max radius
    left = abs(X(1, 1));
    right = X(1, end);
    bottom = abs(Y(1, 1));
    top = Y(end, 1);
    outer = min([left, right, bottom, top]);
end

% Inner radius is just cmask_radius from the SAXS object.
inner = obj.cmask_radius;

% Mask (set to zero) pixels inside or outside the band.
mask = find(radii < inner | radii > outer);
obj.pixels(mask) = 0;

obj.history = strvcat(obj.history, 'mask rotation band');

obj_out = obj;