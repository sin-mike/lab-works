function argout = xaxisbintype(s, xtype)
%SAXS/XAXISBINTYPE Set or return the  binning type desired for the 1D
%   averagin 
%   XAXISBINTYPE = XAXISBINTYPE(S) returns the image type, 'log','lin' of
%   the 1D image 
%
%   S2 = XAXISBINTYPE(S1, TYPE) sets the xaxisbintype, returning the
%   SAXS image object.
%
% this function is called from files like averagex 

switch nargin
case 1
    % It must be a saxs object or we wouldn't be called.
    argout = s.xaxisbintype;
case 2
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    % Just believe whatever image type they tell us.
    argout = s;
    if xtype==1 
        argout.xaxisbintype='log' ;
    else
        argout.xaxisbintype='lin' ;
    end
    argout.history = strvcat(argout.history, ['X-Binning in ', argout.xaxisbintype]);
otherwise
    error('I need one or two arguments.')
end
