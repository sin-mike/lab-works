function obj_out = rotweight(obj)
%SAXS/ROTWEIGHT Weight pixel intensities for rotation tests.
%   ROTWEIGHT(S) returns SAXS image object S with pixel intensities
%   weighted for rotation tests.  Pixels furthest away from the center are
%   unchanged. Pixels closer to the center are weighted proportionally to
%   the square of the radius.

% Find the effective image center in row, column coordinates.
% Make it on the nearest half pixel so pixel radii are symmetric.
pixelcenter = pixelfrac(obj, [0 0]);
pixelcenter = fix(pixelcenter) + 0.5;

% Compute pixel coordinates relative to effective center.
[X Y] = meshgrid([1 : size(obj.pixels, 2)], [1 : size(obj.pixels, 1)]);
X = X - pixelcenter(1);
Y = Y - pixelcenter(2);

% Compute radii.
radii = sqrt(X .^ 2 + Y .^ 2);

% Figure weighting factors.
weight = radii .^ 2;
weight = weight ./ max(weight(:));

obj.pixels = obj.pixels .* weight;

obj = history(obj, 'weighted for rotation');
obj_out = obj;
