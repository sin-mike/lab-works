function argout = raw_filename(s, raw_filename)
%SAXS/RAW_FILENAME Set or return the name of the data file from which the SAXS image object was created.
%   raw_filename = raw_fiename(S) returns the filename,
%
% this function is called from files mparead.m and
% tiffread.m

switch nargin
case 1
    % It must be a saxs object or we wouldn't be called.
    argout = s.raw_filename;
case 2
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    % Just believe whatever image type they tell us.
    argout = s;
    argout.raw_filename = raw_filename;
otherwise
    error('I need one or two arguments.')
end
