function table = xyztable(S)
%XYZTABLE Convert a SAXS image to an XYZ tabular format.
%   XYZTABLE(S) returns a numeric array with X values down the left column,
%   Y values in the first row, and the image data in the tabular middle.
%   See XYZTABLE (the non-SAXS function) for more info.  This
%   function/method is only used with SAXS objects.

table = xyztable(xdata(S), ydata(S), double(S));
