function S = smooth(Sin, size, std)
%SAXS/SMOOTH Smooth a SAXS image.
%   SMOOTH(S, SIZE, STD) returns a SAXS image resulting from filtering S
%   with a Gaussian finite impulse response filter SIZE pixels square and a
%   Gaussian falloff of standard deviation STD.
%
%   See also:  GAUSSFILT; FILTER2; SAXS.

S = Sin;
S.pixels = filter2(gaussfilt(size, std), S.pixels);
S.history = strvcat(S.history, ['smooth, gaussian, size ', num2str(size), ', std ', num2str(std)]);
