function saxsnew=apply_reduction_parameters(s,handles)
%SAXS/APPLY_REDUCTION_PARAMETERS to SAXS Object
%   This routine puts the parameters, and the corresponding file content...
%   that have been added in in reduction
%   parameters panel into the SAXS variabel in the SAXSgui window.
%   As it proceeds it checks for reasonable consistency within the data-files
%   that are loaded.
% 
if get(handles.sample_check,'Value')
    s.transfact= str2num(get(handles.sample_trans,'String'));
else
    s.transfact= [];
end

if get(handles.mask_check,'Value')
    s.mask_filename=get(handles.maskname,'UserData');
    if exist(s.mask_filename)
        statusline= status('Loading mask file...', handles);
        a=load(s.mask_filename); %mask file contains 1's and 0's
        if ~isfield(a,'mask') 
            errordlg(['mask file:', s.mask_filename,' does not contain variable "mask". Please make a new mask file'])
            delete(statusline)
            return
        end
        if ~(size(a.mask)==size(s.raw_pixels)  )
            errordlg(['mask is the wrong size. Please choose a different file'])
            delete(statusline)
            return
        end
        if ~islogical(a.mask)
            errordlg(['mask is not a logical file. Make new mask file '])
            delete(statusline)
            return
        end
    else
        errordlg(['mask file:', s.mask_filename,' does not exist. Please correct'])
        return
    end
    warning off MATLAB:divideByZero
    s.mask_pixels=double(a.mask)./double(a.mask); % this transforms it into 1's and NaN's
    warning on MATLAB:divideByZero
    delete(statusline)
else
    s.mask_filename=[];
    s.mask_pixels=[];
end

if get(handles.flatfield_check,'Value')
    s.flatfield_filename=get(handles.flatfieldname,'UserData');
    if exist(s.flatfield_filename)
        statusline = status('Loading flatfield file...', handles);
        a=getsaxs(s.flatfield_filename);
        if ~(size(a.pixels)==size(s.raw_pixels)  )
            errordlg(['flatfield is the wrong size. Please choose a different file'])
            delete(statusline)
            return
        end
        s.flatfield_pixels=a.pixels;
        delete(statusline)
    else
        errordlg(['flatfield file:', s.flatfield_filename,' does not exist. Please correct'])
        return
    end
else
    s.flatfield_filename=[];
    s.flatfield_pixels=[];
end

if get(handles.empty_check,'Value')
    s.empty_filename=get(handles.emptyname,'UserData');
    if exist(s.empty_filename)
        statusline = status('Loading "empty holder" file...', handles);
        a=getsaxs(s.empty_filename);
        if ~(size(a.pixels)==size(s.raw_pixels)  )
            errordlg(['empty holder file is the wrong size. Please choose a different file'])
            delete(statusline)
            return
        end
        s.empty_pixels=a.pixels;
        s.empty_livetime = a.livetime;
        s.empty_transfact = str2num(get(handles.empty_trans,'String'));
        delete(statusline)
    else
        errordlg(['empty file:', s.empty_filename,' does not exist. Please correct'])
        return
    end
else
    s.empty_filename=[];
    s.empty_pixels=[];
    s.empty_livetime=[];
    s.empty_transfact=[];
end

%This one waits for the inclusion of the "solvent" background into the
%reduction panel.
% if get(handles.solvent_check,'Value')
%     s.solvent_filename=get(handles.solventname,'UserData');
%     if exist(s.solvent_filename)
%         statusline = status('Loading "Solvetn" file...', handles);
%         a=getsaxs(s.solvent_filename);
%         if ~(size(a.pixels)==size(s.raw_pixels)  )
%             errordlg(['empty holder file is the wrong size. Please choose a different file'])
%             delete(statusline)
%             return
%         end
%         s.solvent_pixels=a.pixels;
%         s.solvent_livetime = a.livetime;
%         s.solvent_transfact = str2num(get(handles.solvent_trans,'String'));
%         s.solvent_thickness = str2num(get(handles.solvent_thick,'String'));
%         delete(statusline)
%     else
%         errordlg(['Solvent file:', s.solvent_filename,' does not exist. Please correct'])
%         return
%     end
% else
%     s.solvent_filename=[];
%     s.solvent_pixels=[];
%     s.solvent_livetime=[];
%     s.solvent_transfact=[];
%     s.solvent_thickness=[];
% end

if get(handles.dark_check,'Value')
    s.darkcurrent_filename=get(handles.darkcurrentname,'UserData');
    if exist(s.darkcurrent_filename)
        statusline = status('Loading darkcurrent file...', handles);
        a=getsaxs(s.darkcurrent_filename);
        if ~(size(a.pixels)==size(s.raw_pixels)  )
            errordlg(['darkcurrent file is the wrong size. Please choose a different file'])
            delete(statusline)
            return
        end
        s.darkcurrent_pixels=a.pixels;
        s.darkcurrent_livetime = a.livetime;
        delete(statusline)
    else
        errordlg(['dark current file:', s.darkcurrent_filename,' does not exist. Please correct'])
        return
    end
else
    s.darkcurrent_filename=[];
    s.darkcurrent_pixels=[];
    s.darkcurrent_livetime=[];
end

if get(handles.readout_check,'Value')
    s.readoutnoise_filename=get(handles.readoutnoisename,'UserData');
    if exist(s.readoutnoise_filename)
        statusline = status('Loading readoutnoise file...', handles);
        a=getsaxs(s.readoutnoise_filename);
        if ~(size(a.pixels)==size(s.raw_pixels)  )
            errordlg(['readoutnoise file is the wrong size. Please choose a different file'])
            delete(statusline)
            return
        end
        s.readoutnoise=a.pixels;
        %s.readoutnoise=mean(mean(a.pixels));
        delete(statusline)
    else
        errordlg(['readout noise file:', s.readoutnoise_filename,' does not exist. Please correct'])
        return
    end
else
    s.readoutnoise_filename=[];
    s.readoutnoise=[];
end

if get(handles.zinger_check,'Value')
    s.zinger_removal=1;
else
    s.zinger_removal=[];
end

if 0
    %not available for water calib
else
    s.abs_int_fact=[];
    s.sample_thickness=[];
end

saxsnew=s;

% Record history.
entry = ['Reduction parameters entered '];
saxsnew = history(saxsnew, entry);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STATUS BAR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function h_out = status(message, gui, seconds)
% Display a status line message, returning its handle.
% If you the caller don't specify SECONDS, then you must delete the status
% text control via its handle.
if nargin == 2
    h = figstatus(message, gui.output);
elseif nargin == 3
    h = figstatusbrief(message, seconds, gui.output);
end
if nargout
    h_out = h;
end


