function argout = history(s, histin)
%SAXS/HISTORY Add or return history entries for a SAXS image object.
%   HIST = HISTORY(S) returns the complete history recorded in SAXS
%   image S.  The history is a two-dimensional character array.
%   Each row is a line of history.
%
%   S2 = HISTORY(S1, ADDHIST) adds the lines in the two-dimensional
%   character array ADDHIST to the SAXS objects history and returns
%   the modified SAXS object.

switch nargin
case 1
    % It must be a saxs object or we wouldn't be called.
    argout = s.history;
case 2
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    if ~ (ischar(histin) & ndims(histin) == 2)
        error('The second argument must be a two-dimensional character array.')
    end
    argout = s;
    argout.history = strvcat(argout.history, histin);
otherwise
    error('I need one or two arguments.')
end
