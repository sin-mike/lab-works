function argout = sample_thickness(s, t)
%SAXS/sample_thickness Set or retrieve sample thickness for SAXS image.
%   sample_thickness(S) returns the sample_thickness recorded for SAXS image S.
%
%   sample_thickness(S, T) returns a SAXS image object copied from S with
%   transmission factor T.

switch nargin
case 1  % return sample_thickness
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.sample_thickness;
case 2  % set sample_thickness
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if isempty(t), t=1; end
    if isnumeric(t) && length(t) == 1 && t>0
        argout.sample_thickness = t;
    else
        error('I do not understand this sample_thickness argument.')
    end
    argout.history = strvcat(argout.history, ['sample thickness ', num2str(t)]);
otherwise  % we don't understand
    error('I need one or two arguments.')
end
