function argout = relerr(s, re)
%SAXS/RELERR Set or retrieve Relative Error (STD) of data in SAXS image.
%   RELERR(S) returns the Relative Error (STD) recorded for SAXS image S.
%
%   RELERR(S, T) returns a SAXS image object copied from S with
%   Relative Error T. S cannot be of the type 'xy' and must have the same
%   size as s.pixels

switch nargin
case 1  % return relerr
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.relerr;
case 2  % set relerr
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if isnumeric(re) & length(re) == length(s.pixels) & s.type~'xy'
        argout.relerr = re;
    else
        error('The Relative Error RELERR does not have the correct length or is of the wrong SAXS object type ')
    end
    argout.history = strvcat(argout.history, ['Rel_error calculated']);
otherwise  % we don't understand
    error('I need one or two arguments.')
end
