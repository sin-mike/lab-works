function paramwrite(sin, filepath)
%SAXS/PARAMWRITE Write SAXS parameters to a text file.
%   PARAMWRITE(S, 'name') writes parameters of SAXS image S to file 'name'.

params = paramstr(sin);
file = fopen(filepath, 'wt');
if isequal(file, -1)    
    error(['Cannot open file for writing: ', filepath])
end
for row = 1 : size(params, 1)
    fprintf(file, '%s\n', params(row, :));
end

fclose(file);
