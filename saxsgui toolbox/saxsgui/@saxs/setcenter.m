function centered = setcenter(s, c)
%SAXS/SETCENTER Set the axes origin of a SAXS image at a specified center.
%   SETCENTER(S, C) returns a SAXS image object built from SAXS image S,
%   with axes adjusted so that the origin 0,0 is at the specified center C.
%   C is a vector [x, y], the new center in raw pixel coordinates.

[nrows ncols] = size(s.pixels);

% if isempty(s.xaxis)
%     s.xaxis = [1 ncols];
% end
% if isempty(s.yaxis)
%     s.yaxis = [1 nrows];
% end

if ~ isempty(s.kcal)
    s.xaxis = ([1 ncols] - c(1))*s.kcal/s.pixelcal(1);
    s.yaxis = ([1 nrows] - c(2))*s.kcal/s.pixelcal(2);
else
    s.xaxis = ([1 ncols] - c(1));
    s.yaxis = ([1 nrows] - c(2));
end

record = sprintf('set center %g,%g', c(1), c(2));
s.history = strvcat(s.history, record);
s.raw_center=[c(1) c(2)];
centered = s;
