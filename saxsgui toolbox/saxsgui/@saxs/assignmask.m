function [so] = assignmask(s, pixels, filename, state)
%SAXS/ASSIGNMASK passes mask-related data to the SAXS structure.

% Make sure the first argument is a SAXS object.
if ~ (nargin > 0 && isa(s, 'saxs'))
    error('The first argument must be a SAXS image object.')
end

so=s;
so.mask_pixels =   pixels;
so.mask_filename = filename;
so.reduction_state = state;


