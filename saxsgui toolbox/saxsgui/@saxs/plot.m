function h = plot(s, varargin)
%SAXS/PLOT Plot a collapsed (summed or averaged) SAXS image.

% Generate x data.  We'll only use one.
x = s.xaxisfull;

if s.kcal
    distance_units = 'q (A^-^1)';   %was originally K, changed by KJ April 12, 2005
    UnitsPerPixel=s.kcal/mean(s.pixelcal);
else
    distance_units = 'pixels';
    UnitsPerPixel=1;
end
%do we know the time? if not assume 1 second
if s.livetime
    if strcmp(s.reduction_state(1:2),'11') 
      ylabeltext='Absolute intensity (cm^-1)';
    else
      ylabeltext='average intensity (counts per mr^2 per second)';
    end
    livetime=s.livetime;
else
    ylabeltext='average intensity (per mr^2)';
    livetime=1;
end

%  dx=x(2)-x(1);
%  dy=y(2)-y(1);
%  xmin=x(1);
%  ymin=y(1);
%  xmax=x(len);
%  ymax=y(len);
 
type = s.type;
switch type
    case 'r-ave'
        if sum(s.pixels<=0)>0
            hplot = plot(x, s.pixels/livetime, varargin{:});
        else
            hplot = semilogy(x, s.pixels/livetime, varargin{:});% added by KJ April 12, 2005
        end
        % shows aveage number of counts per mr^2 per second
        % originally: hplot = plot(x, s.pixels, varargin{:});
        %lastline=findobj(gcf,'Type','line');  %This returns all the line in the plot but last is always element number 1
        set(hplot,'UserData',s); %saving saxsobj in data in the UserData variable
        set(hplot,'Tag','Data'); %so we can find it later when we want to change display
% this seems to work for both new and added lines.
        axes(get(hplot, 'Parent'));
        xlabel(['momentum transfer, ', distance_units])
        ylabel(ylabeltext)
    case 'th-ave'
        if sum(s.pixels<=0)>0
            hplot = plot(x, s.pixels/livetime, varargin{:});
        else
            hplot = semilogy(x, s.pixels/livetime, varargin{:});% added by KJ April 12, 2005
        end
        % shows aveage number of counts per mr^2 per second
        % originally: hplot = plot(x, s.pixels, varargin{:});                                          

        set(hplot,'UserData',s); %saving saxsobj in data in the UserData variable
        set(hplot,'Tag','Data'); %so we can find it later when we want to change display
% this seems to work for both new and added lines.
        axes(get(hplot, 'Parent'));
        xlabel(['azimuth, degrees'])
        ylabel(ylabeltext)
    case 'x-ave'
        hplot = plot(x, s.pixels/livetime, varargin{:});
        axes(get(hplot, 'Parent'));
        xlabel(['x, ', distance_units])
        ylabel(ylabeltext)
    case 'y-ave'
        hplot = plot(x, s.pixels/livetime, varargin{:});
        axes(get(hplot, 'Parent'));
        xlabel(['y, ', distance_units])
        ylabel(ylabeltext)
    otherwise
        error('This is the wrong type of SAXS image for PLOT.  Try IMAGE.')
end


if nargout
    h = hplot;
end


