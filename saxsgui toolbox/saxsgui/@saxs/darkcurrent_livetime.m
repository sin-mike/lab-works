function argout = darkcurrent_livetime(s, t)
%SAXS/ENMPTY_livetime Set or retrieve transmission factor for SAXS image.
%   darkcurrent_LIVETIME(S) returns the livetime recorded for SAXS image S.
%
%   darkcurrent_LIVETIME(S, T) returns a SAXS image object copied from S with
%   livetime T for the darkcurrent holder

switch nargin
case 1  % return livetime
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.darkcurrent_livetime;
case 2  % set livetime
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if ~isempty(t)
        if isnumeric(t) && length(t) == 1
            argout.darkcurrent_livetime = t;
            argout.history = strvcat(argout.history, ['livetime ', num2str(t)]);
        else
            error('I do not understand this darkcurrent_LIVETIME argument.')
        end
    else
        argout.darkcurrent_livetime=[];
    end
otherwise  % we don't understand
    error('I need one or two arguments.')
end
