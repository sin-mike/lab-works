function so=darkcurrent_pixels(si,pixels); 
%SAXS/darkcurrent_PIXELS this function simply inserts darkcurrent data 
%in the SAXS structure.
% so=mask_pixels(si,pixels)
so=si;
so.darkcurrent_pixels=pixels;

