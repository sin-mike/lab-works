function [argout, outer_radius] = cmask(s1, radius, outer)
%SAXS/CMASK Specify image areas to mask when searching for symmetries.
%   S2 = CMASK(S1, RADIUS) specifies a radius, expressed in pixels,
%   from the center of the image.  That area will not be considered
%   by symmetry searches.
%
%   The center search uses a square mask where a side of the square
%   is 2 * RADIUS pixels, centered over each prospective center point.
%   Rotation and ellipticity searches use a circular mask around
%   the calculated center.
%
%   S2 = CMASK(S1, RADIUS, OUTER) specifies an outer radius for the image
%   region to be used when searching for rotation angles to maximize
%   symmetry about the major axes.  To leave the inner RADIUS unchanged,
%   specify an empty array, [], for RADIUS.
%
%   RADIUS = CMASK(S1) returns the current mask radius.
%   [RADIUS, OUTER] = CMASK(S1) returns both radii.
%
%   See also:  SAXS.

% Determine our task by which arguments the caller specified.

% Make sure the first argument is a SAXS object.
if ~ (nargin > 0 && isa(s1, 'saxs'))
    error('The first argument must be a SAXS image object.')
end

if nargin == 1  % return cmask radius values
    argout = s1.cmask_radius;  % always return inner radius
    if nargout > 1  % return outer radius
        outer_radius = s1.cmask_outer;
    end
end

if nargin > 1  % set inner cmask radius
    argout = s1;
    if isempty(radius)  
        % means leave existing value alone
    elseif isnumeric(radius) & length(radius) == 1
        argout.cmask_radius = radius;
    else
        error('I do not understand this RADIUS argument.')
    end
    argout.history = strvcat(argout.history, ['cmask radius ', num2str(radius)]);
end

if nargin > 2  % set outer cmask radius
    % argout already contains a copy of s1, from above
    if isnumeric(outer) & length(outer) == 1
        argout.cmask_outer = outer;
    else
        error('I do not understand this OUTER RADIUS argument.')
    end
    argout.history = strvcat(argout.history, ['cmask outer radius ', num2str(outer)]);
end    

% We blithely ignore input arguments beyond the first two.
