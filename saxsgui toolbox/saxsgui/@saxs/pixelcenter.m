function c = pixelcenter(s)
%SAXS/PIXELCENTER Retrieve the pixel coordinates of a SAXS image center.
%   PIXELCENTER(S) returns a two-element vector containing the raw pixel x
%   and y coordinates of the center of the SAXS image object S.  In terms
%   of S's own coordinates, the center is at 0,0.  PIXELCENTER returns x,y of
%   the center if pixel coordinates are x = 1 : ncols and y = 1 : nrows.
%
%   Note if S has been cropped, the reported coordinates do not correspond
%   to the original image.  Also, if S has not been centered the raw pixel
%   center will probably be reported as [0, 0].

x = xaxis(s);  x = x(1);  x = -x + 1;
y = yaxis(s);  y = y(1);  y = -y + 1;

c = [x y];
