function so=mask_pixels(si,pixels); 
%SAXS/MASK_PIXELS this function simply inserts mask data 
%in the SAXS structure.
% so=mask_pixels(si,pixels)
so=si;
so.mask_pixels=pixels;

