function argout = xaxisfull(s, xaxisfull)
%SAXS/TYPE Set or return the xaxisfull parameter of a SAXS image object.
%   IMAGETYPE = XAXISFULL(S) returns the xaxisfull value.
%
%   S2 = XAXISFULL(S1, XAXISFULL) sets the image xaxsifull, returning the
%   SAXS image object.

switch nargin
case 1
    % It must be a saxs object or we wouldn't be called.
    argout = s.xaxisfull;
case 2
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    % Just believe whatever image type they tell us.
    argout = s;
    argout.xaxisfull = xaxisfull;
otherwise
    error('I need one or two arguments.')
end
