function so=insert_raw_saxs_pixels(si,pixels); 
%SAXS/INSERT_SAXS_PIXELS this function simply inserts 2D-image in the SAXS structure.
% so=insert_saxs_pixels(si,pixels)
so=si;
so.raw_pixels=pixels;

