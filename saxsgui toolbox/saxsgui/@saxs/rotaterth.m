function newobj = rotaterth(obj, degrees)
%SAXS/ROTATERTH Rotate an image in polar coordinates.
%   NEWOBJ = ROTATERTH(OBJ, DEGREES)
%
%   We rotate only the number of pixel rows that fit within the specified
%   DEGREES.  Thus our resolution for rotation is limited to the pixel
%   spacing in the image along the degrees axis.

%   We could rotate more finely if we interpolated between rows.

% A smattering of input argument checking:

if ~ isequal(type(obj), 'rth')
    error('SAXS object must be in polar coordinates.')
end

if nargin ~= 2 || ~isnumeric(degrees) || length(degrees) ~= 1
    error('Second argument must be degrees of rotation.')
end

% Roll the image down the theta axis, running lost rows back on the top.

degree_range = yaxis(obj);
nrows = size(obj.pixels, 1);
deg_per_row = (degree_range(2) - degree_range(1)) / (nrows - 1);
roll_rows = fix(degrees / deg_per_row);  % fix to whole row

% Leave old obj unchanged.  Roll rows into newobj.

newobj = obj;

roll_rows = rem(roll_rows, nrows);  % 0 <= abs(roll_rows) < nrows
if roll_rows < 0
    roll_rows = nrows + roll_rows;  % do positive equivalent
end

newobj.pixels(1 : roll_rows, :) = obj.pixels(nrows - roll_rows + 1 : nrows, :);
newobj.pixels(roll_rows + 1 : nrows, :) = obj.pixels(1 : nrows - roll_rows, :);

newobj = history(newobj, ['rotate ', num2str(roll_rows * deg_per_row), ' degrees']);
