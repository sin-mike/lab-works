function argout = absintfact(s, t)
%SAXS/ABSINTFACT Set or retrieve absolute intensity factor for SAXS image.
%   ABSINTFACT(S) returns the transmission factor recorded for SAXS image S.
%
%   ABSINTFACT(S, T) returns a SAXS image object copied from S with
%   transmission factor T.

switch nargin
case 1  % return transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.abs_int_fact;
case 2  % set transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if isnumeric(t) & length(t) == 1
        argout.abs_int_fact = t;
    else
        if isempty(t)
            argout.abs_int_fact=[];
            t=1;
        else
            error('I do not understand this ABSINTFACT argument.')
        end
    end
    argout.history = strvcat(argout.history, ['Abs. Int. fact ', num2str(t)]);
otherwise  % we don't understand
    error('I need one or two arguments.')
end
