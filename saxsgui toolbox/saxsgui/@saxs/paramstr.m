function pstr = paramstr(s)
%SAXS/PARAMSTR Create a character string list of parameter values.
%   STR = PARAMSTR(S) returns a multirow character array STR listing
%   parameter (field) values for SAXS image S.

% Sure, it'd be nice to do it this way...
%
%     for field = fieldnames(s)
%         value = getfield(s, field);
%         if isempty(value)
%             vdisplay = '[]';
%         elseif ischar(value)
%             vdisplay = value;
%         elseif isnumeric(value)
%             switch length(value)
%             case 1
%                 vdisplay = sprintf('%g', value);
%             case 2
%                 vdisplay = sprintf('[%g, %g]', value(1), value(2));
%             otherwise
%                 [m n] = size(value);
%                 vdisplay = sprintf('%dx%d numeric array', m, n);
%             end
%         else
%             vdisplay = class(value);
%         end
%         if ~ isempty(value)
%             switch field
%             case 'rotation'
%                 vdisplay = strcat(vdisplay, ' degrees');
%             end
%         end
%         disp(['   ', field, ':  ', vdisplay])
%     end

pstr = [];

pixels = s.pixels;
if isempty(pixels)
    str = '[]';
else
    [rows cols] = size(s.pixels);
    str = sprintf('%dx%d', rows, cols);
end
pstr = strvcat(pstr, oneline('pixels', str));

type = s.type;
if isempty(type)
    str = '[]';
else
    str = type;
end
pstr = strvcat(pstr, oneline('type', str));

relerr = s.relerr;
if isempty(relerr)
    str = '[]';
else
    [rows cols] = size(relerr);
    str = sprintf('%dx%d', rows, cols);
end
pstr = strvcat(pstr, oneline('relerr', str));

error_norm_fact= s.error_norm_fact;
if isempty(error_norm_fact)
    str = '[]';
else
    str = num2str(error_norm_fact);
end
pstr = strvcat(pstr, oneline('error_norm_fact', str));

datatype = s.datatype;
if isempty(datatype)
    str = '[]';
else
    str = datatype;
end
pstr = strvcat(pstr, oneline('datatype', str));

detectortype = s.detectortype;
if isempty(detectortype)
    str = '[]';
else
    str = detectortype;
end
pstr = strvcat(pstr, oneline('detectortype', str));

mradius = s.cmask_radius;
pstr = strvcat(pstr, oneline('cmask_radius', num2str(mradius)));

oradius = s.cmask_outer;
if isempty(oradius)
    str = '[]';
else
    str = num2str(oradius);
end
pstr = strvcat(pstr, oneline('cmask_outer', str));

xaxis = s.xaxis;
if isempty(xaxis)
    str = '[]';
else
    str = sprintf('%d %d', xaxis(1), xaxis(2));
end
pstr = strvcat(pstr, oneline('xaxis', str));

xaxisfull = s.xaxisfull;
if isempty(xaxis)
    str = '[]';
else
    [rows cols] = size(xaxisfull);
    str = sprintf('%dx%d', rows, cols);
end
pstr = strvcat(pstr, oneline('xaxisfull', str));

yaxis = s.yaxis;
if isempty(yaxis)
    str = '[]';
else
    str = sprintf('%d %d', yaxis(1), yaxis(2));
end
pstr = strvcat(pstr, oneline('yaxis', str));

xaxisbintype = s.xaxisbintype;
if isempty(xaxisbintype)
    str = '[]';
else
    str = xaxisbintype;
end
pstr = strvcat(pstr, oneline('xaxisbintype', str));

log = s.log;
if isempty(log)
    str = '[]';
else
    str = log;
end
pstr = strvcat(pstr, oneline('log', str));

realtime = s.realtime;
if isempty(realtime)
    str = '[]';
else
    str = num2str(realtime);
end
pstr = strvcat(pstr, oneline('realtime', str));

livetime = s.livetime;
if isempty(livetime)
    str = '[]';
else
    str = num2str(livetime);
end
pstr = strvcat(pstr, oneline('livetime', str));

transfact = s.transfact;
if isempty(transfact)
    str = '[]';
else
    str = num2str(transfact);
end
pstr = strvcat(pstr, oneline('transfact', str));

sample_thickness = s.sample_thickness;
if isempty(sample_thickness)
    str = '[]';
else
    str = num2str(sample_thickness);
end
pstr = strvcat(pstr, oneline('sample_thickness', str));

calibrationtype = s.calibrationtype;
if isempty(calibrationtype)
    str = '[]';
else
    str = calibrationtype;
end
pstr = strvcat(pstr, oneline('calibrationtype', str));

kcal = s.kcal;
if isempty(kcal)
    str = '[]';
else
    str = num2str(kcal);
end
pstr = strvcat(pstr, oneline('kcal', str));

pixelcal = s.pixelcal;
if isempty(pixelcal)
    str = '[]';
else
    str = num2str(pixelcal);
end
pstr = strvcat(pstr, oneline('pixelcal', str));

koffset = s.koffset;
if isempty(koffset)
    str = '[]';
else
    str = num2str(koffset);
end
pstr = strvcat(pstr, oneline('koffset', str));

wavelength = s.wavelength;
if isempty(wavelength)
    str = '[]';
else
    str = num2str(wavelength);
end
pstr = strvcat(pstr, oneline('wavelength', str));

pixelsize = s.pixelsize;
if isempty(pixelsize)
    str = '[]';
else
    str = num2str(pixelsize);
end
pstr = strvcat(pstr, oneline('pixelsize', str));

detector_dist = s.detector_dist;
if isempty(detector_dist)
    str = '[]';
else
    str = num2str(detector_dist);
end
pstr = strvcat(pstr, oneline('detector_dist', str));

reduction_type = s.reduction_type;
if isempty(reduction_type)
    str = '[]';
else
    str = num2str(reduction_type);
end
pstr = strvcat(pstr, oneline('reduction_type', str));

reduction_state = s.reduction_state;
if isempty(reduction_state)
    str = '[]';
else
    str = num2str(reduction_state);
end
pstr = strvcat(pstr, oneline('reduction_state', str));

raw_filename = s.raw_filename;
if isempty(raw_filename)
    str = '[]';
else
    str = raw_filename;
end
pstr = strvcat(pstr, oneline('raw_filename', str));

raw_date = s.raw_date;
if isempty(raw_date)
    str = '[]';
else
    str = raw_date;
end
pstr = strvcat(pstr, oneline('raw_date', str));

raw_pixels = s.raw_pixels;
if isempty(raw_pixels)
    str = '[]';
else
    [rows cols] = size(raw_pixels);
    str = sprintf('%dx%d', rows, cols);
end
pstr = strvcat(pstr, oneline('raw_pixels', str));

raw_center = s.raw_center;
if isempty(raw_center)
    str = '[]';
else
    str = sprintf('%d %d', raw_center(1), raw_center(2));
end
pstr = strvcat(pstr, oneline('raw_center', str));

mask_filename = s.mask_filename;
if isempty(mask_filename)
    str = '[]';
else
    str = mask_filename;
end
pstr = strvcat(pstr, oneline('mask_filename', str));

mask_pixels = s.mask_pixels;
if isempty(mask_pixels)
    str = '[]';
else
    [rows cols] = size(mask_pixels);
    str = sprintf('%dx%d', rows, cols);
end
pstr = strvcat(pstr, oneline('mask_pixels', str));

flatfield_filename = s.flatfield_filename;
if isempty(flatfield_filename)
    str = '[]';
else
    str = flatfield_filename;
end
pstr = strvcat(pstr, oneline('flatfield_filename', str));

flatfield_pixels = s.flatfield_pixels;
if isempty(flatfield_pixels)
    str = '[]';
else
    [rows cols] = size(flatfield_pixels);
    str = sprintf('%dx%d', rows, cols);
end
pstr = strvcat(pstr, oneline('flatfield_pixels', str));

flatfieldtype = s.flatfieldtype;
if isempty(flatfieldtype)
    str = '[]';
else
    str = flatfieldtype;
end
pstr = strvcat(pstr, oneline('flatfieldtype', str));

empty_filename = s.empty_filename;
if isempty(empty_filename)
    str = '[]';
else
    str = empty_filename;
end
pstr = strvcat(pstr, oneline('empty_filename', str));

empty_pixels = s.empty_pixels;
if isempty(empty_pixels)
    str = '[]';
else
    [rows cols] = size(empty_pixels);
    str = sprintf('%dx%d', rows, cols);
    
end
pstr = strvcat(pstr, oneline('empty_pixels', str));

empty_transfact = s.empty_transfact;
if isempty(empty_transfact)
    str = '[]';
else
    str = num2str(empty_transfact);
end
pstr = strvcat(pstr, oneline('empty_transfact', str));

empty_livetime = s.empty_livetime;
if isempty(empty_livetime)
    str = '[]';
else
    str = num2str(empty_livetime);
end
pstr = strvcat(pstr, oneline('empty_livetime', str));

solvent_filename = s.solvent_filename;
if isempty(solvent_filename)
    str = '[]';
else
    str = solvent_filename;
end
pstr = strvcat(pstr, oneline('solvent_filename', str));

solvent_pixels = s.solvent_pixels;
if isempty(solvent_pixels)
    str = '[]';
else
    [rows cols] = size(solvent_pixels);
    str = sprintf('%dx%d', rows, cols);
    
end
pstr = strvcat(pstr, oneline('solvent_pixels', str));

solvent_thickness = s.solvent_thickness;
if isempty(solvent_thickness)
    str = '[]';
else
    str = num2str(solvent_thickness);
end
pstr = strvcat(pstr, oneline('solvent_thickness', str));

solvent_transfact = s.solvent_transfact;
if isempty(solvent_transfact)
    str = '[]';
else
    str = num2str(solvent_transfact);
end
pstr = strvcat(pstr, oneline('solvent_transfact', str));

solvent_livetime = s.solvent_livetime;
if isempty(solvent_livetime)
    str = '[]';
else
    str = num2str(solvent_livetime);
end
pstr = strvcat(pstr, oneline('solvent_livetime', str));


darkcurrent_filename = s.darkcurrent_filename;
if isempty(darkcurrent_filename)
    str = '[]';
else
    str = darkcurrent_filename;
end
pstr = strvcat(pstr, oneline('darkcurrent_filename', str));

darkcurrent_pixels = s.darkcurrent_pixels;
if isempty(darkcurrent_pixels)
    str = '[]';
else
    [rows cols] = size(darkcurrent_pixels);
    str = sprintf('%dx%d', rows, cols);
end
pstr = strvcat(pstr, oneline('darkcurrent_pixels', str));

darkcurrent_livetime = s.darkcurrent_livetime;
if isempty(darkcurrent_livetime)
    str = '[]';
else
    str = num2str(darkcurrent_livetime);
end
pstr = strvcat(pstr, oneline('darkcurrent_livetime', str));

readoutnoise_filename = s.readoutnoise_filename;
if isempty(readoutnoise_filename)
    str = '[]';
else
    str = readoutnoise_filename;
end
pstr = strvcat(pstr, oneline('readoutnoise_filename', str));

readoutnoise = s.readoutnoise;
if isempty(readoutnoise)
    str = '[]';
else
    str = num2str(readoutnoise);
end
pstr = strvcat(pstr, oneline('readoutnoise', str));

zinger_removal = s.zinger_removal;
if isempty(zinger_removal)
    str = '[]';
else
    str = num2str(zinger_removal);
end
pstr = strvcat(pstr, oneline('zinger_removal', str));

abs_int_fact = s.abs_int_fact;
if isempty(abs_int_fact)
    str = '[]';
else
    str = num2str(abs_int_fact);
end
pstr = strvcat(pstr, oneline('abs_int_fact', str));

% loop for sample_condition and experiment configuration
name={'sample_condition.xpos','sample_condition.ypos','sample_condition.angle1','sample_condition.angle2','sample_condition.angle3',...
    'sample_condition.temp','sample_condition.pressure','sample_condition.strain','sample_condition.stress','sample_condition.shear_rate',...
    'sample_condition.sample_conc','exp_conf.d1','exp_conf.d2','exp_conf.d3',...
    'exp_conf.l1','exp_conf.l2','exp_conf.l3','exp_conf.l4','exp_conf.wavelength','exp_conf.deltawavelength','exp_conf.Izero',...
    'exp_conf.det_offx','exp_conf.det_offy','exp_conf.det_rotx','exp_conf.det_roty',...
    'exp_conf.det_pixsizex','exp_conf.det_pixsizey','exp_conf.det_resx','exp_conf.det_resy'};

for ii=1:length(name)
    txt=['if isempty(s.',name{ii},'),str=''[]''; else str=num2str(s.',name{ii},');end; pstr=strvcat(pstr,oneline(''',name{ii},''',str));'];
    eval(txt)
end

added_constant = s.added_constant ;
if isempty(added_constant )
    str = '[]';
else
    str = num2str(added_constant);
end
pstr = strvcat(pstr, oneline('added_constant ', str));

multiplication_fact = s.multiplication_fact;
if isempty(multiplication_fact)
    str = '[]';
else
    str = num2str(multiplication_fact);
end
pstr = strvcat(pstr, oneline('multiplication_fact', str));

userdata_cell = s.userdata_cell;
if isempty(userdata_cell)
    str = '{}';
else
    [rows cols] = size(s.userdata_cell);
    str = sprintf('%dx%d', rows, cols);
end
pstr = strvcat(pstr, oneline('userdata_cell', str));

metadata = s.metadata;
if isempty(metadata)
    str = '{}';
else
    [rows cols] = size(s.metadata);
    str = sprintf('%dx%d', rows, cols);
end
pstr = strvcat(pstr, oneline('metadata', str));

l1 = oneline('history', s.history(1,:));
colonpos = findstr(':', l1);
colonpos = colonpos(1);
pad = repmat(blanks(colonpos + 2), size(s.history, 1) - 1, 1);
lrest = [pad, s.history(2:end, :)];
pstr = strvcat(pstr, strvcat(l1, lrest));


function l = oneline(name, value)
% Format a one line field name, value pair.
colonpos = 29;  % adjust this for longest field name
nblanks = colonpos - length(name);
l = [blanks(nblanks), name, ':  ', value];
