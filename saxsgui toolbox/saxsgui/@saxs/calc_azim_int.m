function stemp=calc_azim_int(stemp,bounds,npts)
% SAXS/CALC_AZIM_INT calculates azimuthal intensity
stemp.type='xy';% resetting cartesian / polar flag
stemp.xaxis=[1-stemp.raw_center(1) length(stemp.pixels(1,:))-stemp.raw_center(1)]*stemp.kcal/stemp.pixelcal(1);
stemp.yaxis=[1-stemp.raw_center(2) length(stemp.pixels(:,1))-stemp.raw_center(2)]*stemp.kcal/stemp.pixelcal(2);
stemp=polar(stemp,bounds(4)/(npts));
% cropping the image       
stemp = crop(stemp, [bounds(1), bounds(2); bounds(1)+bounds(3), bounds(2)+bounds(4)]);
stemp.type = 'th-ave';
event = 'average vs theta';
stemp.pixels = meannum(stemp.pixels');  %averaging