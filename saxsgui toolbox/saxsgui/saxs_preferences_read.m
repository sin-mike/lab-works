%function saxs_preferences_read()
% program to read the saxs_preferences.m file
global water_abs_int glassy_carbon_abs_int water_attenuation_length
global cmask_radius smooth_size smooth_std
global fatpix_fontsize
global zinger_params
global averagex_prefs
global fujiimgrot90 fujiimgflip

sp_name='saxs_preferences.txt';
sp_file=which('saxs_preferences.txt');
if isempty(sp_file) 
    sp_file=sp_name;
end
fid=fopen(sp_file,'r');
if (fid<1)
    errordlg('saxs_prefences.txt file is not found or readable');
    pause(10)
else
    tline=fgetl(fid);%parameters for absolution calibration
    tline=fgetl(fid);%water_abs_int Units are in cm-1 at 25 degrees C.
    eval(tline)
    
    tline=fgetl(fid);%glassy_carbon_abs_int This is for a specific calibrated sample (1mm Glassy Carbon in Haifa
    eval(tline)
    
    tline=fgetl(fid);%water_attenuation_length Attenuation lenght of water (cm) must be entered for the energy in question
    eval(tline)
    
    tline=fgetl(fid);% parameters for auto-finding the center
    tline=fgetl(fid);%'cmask_radius
    eval(tline)    
    
    tline=fgetl(fid);%parameters for smoothing
    tline=fgetl(fid);%smooth_size
    eval(tline)    
    
    tline=fgetl(fid);% smooth_std
    eval(tline)    
    
    tline=fgetl(fid);%parameters for fatpix
    tline=fgetl(fid);%fatpix_fontsize
    eval(tline)    

    tline=fgetl(fid);% parameters for zingers : this is put in this since this is usually site specific
    tline=fgetl(fid);%zinger_params.smoothingsize, size of smoothing box (N x N)
    eval(tline)

    tline=fgetl(fid);%zinger_params.threshold, threshold for determining zingers (actual/average>threshold)
    eval(tline)
    
    tline=fgetl(fid);%zinger_params.zinger_masksize, size of mask around zinger will be 2M+1,2M+1
    eval(tline)
    
    tline=fgetl(fid);% parameters for averaging : this is put in here to allow site specific preferences
    tline=fgetl(fid);%averagex_prefs.avtype, Average type: "az" is azimuthal (I vs. q), "mo" is radial average (I vs. chi) 
    eval(tline)
    
    tline=fgetl(fid);%averagex_prefs.aznumpoints, default number of points in the az average
    eval(tline)
    
    tline=fgetl(fid);%averagex_prefs.azbintype, default binning type for az average 
    eval(tline)
    
    tline=fgetl(fid);%averagex_prefs.monumpoints, default number of points in the mo average
    eval(tline)
    
    tline=fgetl(fid);%averagex_prefs.reducttype, default reduction method: s is spectra-by-spectra, p is pixel-by-pixel
    eval(tline)
    
    tline=fgetl(fid);% parameter for loading fujiimgplates (controlling their orientation)
    tline=fgetl(fid);%fujiimgrot90, number of times to rotate raw image by 90 degrees 
    eval(tline)
    
    tline=fgetl(fid);%fujiimgflip, To flip or not to flip image (no flip=0, flip=1) 
    eval(tline)
    
end
fclose(fid);

