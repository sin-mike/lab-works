function [etamax,strimg] = findellip(img,b,a,th,cut)
%FINDELLIP Find ellipticity of image.
%  ETAMAX = FINDELLIP(IMG, XC, YC, THETA, IGNORE)
%   This function finds the apparent ellipticity of an XRD
%   image. It works by maximizing the variance in the 
%   eigenvalues of the image in the elliptical coordinate
%   system. The inputs are the image (IMG), center coordinates
%   (XC and YX), degrees of rotation (THETA), and
%   an optional number of pixels to ignore near the image
%   center (IGNORE, default = 40 if not specified). 
%   The function returns the ellipticity
%   parameter of the image (ETAMAX).
%
%  SEE ALSO:  
%   CENTER; XRD2POL; FINDROT; POL2ELLIP; IMAGEELLIP; ELLIPOVER.

plots = 0;  % no plots unless you stop here in the debugger

% Transpose the image [Why?  So STRETCHE2 works in the right direction?]
img = img';
% Get the image size
[m,n] = size(img);
% Check inputs for cut
if nargin < 5 || isempty(cut)
  cut = 40;
end
% Convert cut to number of pixels in new coordinates 
% [assuming 0.5 pixel steps]
cut = cut*2 + 1;
% Convert the image to elliptical with eta = 0
strimg = stretche2(img,b,a,th,0);
% Get eigenvalues from base image and calculate objective function
ev = svd(strimg(:,cut:end));
%vmlast = -sum(ev(1:4))/sum(ev);
vmlast = -sum(ev.^4)/sum(ev)^4;
% Set up search for optimal eta
vmc = vmlast*2;
eta = 0; k = 1;
vm = zeros(15,1);
xm = zeros(15,1);
vm(1) = vmlast;
inc = 5;
% Increase eta until objective starts to decrease
while vmc < vmlast  
  k = k+1;
  eta = eta + inc;
  xm(k) = eta;
  strimg = stretche2(img,b,a,th,eta);
  ev = svd(strimg(:,cut:end));
  vm(k) = -sum(ev.^4)/sum(ev)^4;
  %vm(k) = -sum(ev(1:4))/sum(ev);
  vmc = vm(k);
  vmlast = vm(k-1);
end
if k > 2
  xmin = xm(k-2);
  xmax = xm(k);
else
  xmin = xm(1);
  xmax = xm(2);
end
% Create an options vector
options = foptions;
% Set option on tolerance of etamax
options(2) = .1;
etamax = fmin('ellipobs',xmin,xmax,options,img,b,a,th,cut);

% Get image in elliptical coordinates if wanted
if nargout > 1 || plots ~= 0
  strimg = stretche2(img,b,a,th,etamax);
end
if plots ~= 0
  % Make a plot with the image in elliptical coordinates
  h2 = figure('position',[189 341 512 384]);
  imagesc(.5:.5:100,0:360,strimg)
  s = sprintf('Image in Elliptical Coordinates, eta = %g',etamax);
  title(s)
  xlabel('Radial Distance in Pixels (u)')
  ylabel('Angle in Degrees (v)') 
  sclx = (1:n) - a;
  scly = (1:m) - b;
  % Make a plot with elliptical coordinates overlay
  h1 = figure('position',[408 270 512 384]);
  imagesc(sclx,scly,img);
  [x,y] = meshgrid(1:10:100,[0:10:360]*2*pi/360);
  [xu,yu] = ellip2cart2(x,y,etamax);
  hold on
  plot(xu,yu,'-y')
  plot(xu',yu','-y')
  hold off
  axis([-100 100 -100 100])
  s = sprintf('Image with Elliptical Coordinates Overlay, eta = %g',etamax);
  title(s)
end
