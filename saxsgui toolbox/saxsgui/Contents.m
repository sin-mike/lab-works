% SAXSGUI Software package
% 
%
%   MATLAB programs for Small Angle X-ray Scattering (SAXS)
%                       Wide Angle X-Ray Scattering images.
%
%   The development of the software package has been sponsored by
%       Molecular Metrology Inc., 2002-2004
%       JJ X-Ray System Aps. 2004-2007
%       Rigaku IT, 2004-2007
%       and the time of personal contributors:
%           Brian Pauw
%           Milos Steinhart
%
%   Program Packages are copyrighted by their individual creators 
%       Copyright Molecular Metrology, Inc., 2002-2004
%       Copyright JJ X-Ray Systems ApS.,  2003-2007
%   and personal contributors
%       Brian Pauw, 2006-2007
%       Milos Steinhart, 2007
%   as well as some original programs by
%       Eigenvector Research, Inc.
%       Greg Aloe, Mathworks Inc. (uigetfiles)
%       John D'Errico (fminsearchbnd)
%       Francois Bouffard (draggable)
%   
%   See HELP saxsgui/license.dat for permissions and limitations.
%
%   Type HELP SAXS to learn more about SAXS objects,
%   or obtain a copy of the SAXSGUI Manual
%
