function Icalc=DoublePearsonPeakSlope(q,I,varargin)
% Description
% This function calculates 1D data for Two PearsonVII peak with a sloped
% background. The peaks have the same width and the same peakprofile
% Description end
% Number of Parameters: 8
% parameter 1: MainCentre : Centre of main PearsonVII Peak
% parameter 2: MainAmp : Amplitude of main PearsonVII Peak
% parameter 3: Width : FWHM of main PearsonVII peak
% parameter 4: MainTailing : Tailing parameter of main PearsonVII peak
% parameter 5: SideCentre : Centre of PearsonVII
% parameter 6: SideAmp : Amplitude of PearsonVII
% parameter 7: Backg : Background of PearsonVII
% parameter 8: Slope : Slope of the PearsonVII
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% PearsonPeakSlope
% This fits a PearsonVII function to the data, according to the Pearson
% description as given in the documentation of the CCP13 XFIT prorgam.  If
% Tailing =1 then the peak takes a lorentzian shape, if Tailing=inf, the
% curve becomes gaussian in nature. 
i=1;
global fitpar
values=varargin{1};
mainCentre=values(i);i=i+1;
mainAmp=values(i);i=i+1;
Width=values(i);i=i+1;
Tailing=values(i);i=i+1;
sideCentre=values(i);i=i+1;
sideAmp=values(i);i=i+1;
Backg=values(i);i=i+1;
Slope=values(i);i=i+1;


Icalc=Backg+Slope.*(q-mainCentre)+...
    mainAmp./(1+4.*((q-mainCentre)./Width).^2.*(2^(1./Tailing)-1)).^Tailing+...
    sideAmp./(1+4.*((q-sideCentre)./Width).^2.*(2^(1./Tailing)-1)).^Tailing;
Ipeak1=mainAmp./(1+4.*((q-mainCentre)./Width).^2.*(2^(1./Tailing)-1)).^Tailing;
Ipeak2=sideAmp./(1+4.*((q-sideCentre)./Width).^2.*(2^(1./Tailing)-1)).^Tailing;

%--------------------------------------------------------------------------

