function s = edfread(pathname)
%EDFREAD Read an image from a EDF file, returning a saxs image object.
%   SAXS_OBJECT = EDFREAD('pathname'); reads the file 'pathname' and attempts
%   to load a SAXS image, returning a saxs image object.
%
% EDF files are file formats used by the Pilatus detectors.
%
%   See also SAXS/SAXS, GETSAXS, SAXS/DISPLAY.


% Try to open the file for reading.
[im,hd]=edfread_pii(pathname);

% Extract starting date from header
filedate=edf_findheader(hd,'Date','whole_string');

xdim=size(im,2);
ydim=size(im,1);

livetime1=edf_findheader(hd,'count_time','float');

% Return a saxs object.
s = saxs(im, ['edfread file ''', pathname, ''' ', datestr(now)]);
s = type(s, 'xy');
s = realtime(s, livetime1);
s = livetime(s, livetime1);
s = datatype(s,'edf');
s = detectortype(s,'PILATUS');
s = raw_filename(s, pathname);
s = raw_date(s,filedate);


