function saxsobj=fujiimgread(pathname,silent)
global fujiimgrot90 fujiimgflip
fileroot=pathname(1:max(find(pathname=='.')-1));

%read in data from the fuji inf fil
try  % reading image-header from the inf-file
    fid=fopen([fileroot,'.inf'],'r');
    s=fread(fid,'uchar');
    I=find(s==10);
    s1=str2num(char(s(I(6):I(7)))'); % finding number of rows (or columns)
    s2=str2num(char(s(I(7):I(8)))'); % finding number of columns (or rows)
    s3=s(I(10):I(11)); % finding the date string
    % this format is of type Tue Jan 11 19:55:48 2005 but should be 
    % Jan 11 2005 19:55:48
    I=find(s3==32); % finding blanks
    filedate=char([s3(I(1)+1:I(3)-1)',' ',s3(I(4)+1:end-1)',' ',s3(I(3)+1:I(4)-1)']);
    fclose(fid);
catch
    caught = lasterr;  % error message
    error(['Error reading configuration file .inf for Fuji IP data', pathname, ':  ', caught])
end

try  % reading an image from the file
    fid=fopen([fileroot,'.img'],'r');
catch
    caught = lasterr;  % error message
    error(['Error reading image file .img for Fuji IP data', pathname, ':  ', caught])
end

%now read actual data
%im = fread(fid,[s1,s2],'uint16=>uint16');
im = fread(fid,[s1,s2],'uint16');
fclose(fid);

im=mod(double(im),256)*256+floor(double(im)/256.);
%toc
%Fuji stores files in a logarithmic (base 10 format)
%We believe they read them in linear mode and transfer them to logarithmic scale from 0 to 65535 along the following definition.
%       Ilog=*65536*log10(I+1)/log10(65536)
% consequently to obtain the original linear scale we need.
%    I=10.^(Ilog/65536*log10(65536))-1
latitude=5.0; % this parameter is the parameter given by the fuji image plate and is the dynamic range of the data
     % i.e. 5.0 is 100000 and 4 is 10000 etc
im=10.^(double(im)/65536*latitude)-1;
if (fujiimgflip==1)
    im=flipud(im);
end
im=rot90(im,fujiimgrot90);

saxsobj = saxs(im, ['imgread file ''', pathname, ''' ', datestr(now)]);
if ~silent
    %input exposure time in dialogbox
    prompt={'Enter the normalization factor (time or monitor counter) for the img-file (in seconds):'};
    def={'900'};
    dlgTitle='Input for Exposure Time';
    lineNo=1;
    answer=inputdlg(prompt,dlgTitle,lineNo,def);

    livetime1=str2num(char(answer));
    if isempty(livetime1)
        livetime1=1;
    end
else
    livetime1=1;
end
saxsobj = type(saxsobj, 'xy');
saxsobj = realtime(saxsobj, max(0,livetime1));
saxsobj = livetime(saxsobj, max(0,livetime1));
saxsobj = raw_filename(saxsobj, pathname);
saxsobj = raw_date(saxsobj,filedate);
saxsobj = datatype(saxsobj,'img');
saxsobj = detectortype(saxsobj,'FujiIP');
