function axx = im2axx(im, imcol)
%IM2AXX Convert image column numbers to x axis values.
%   AXX = IM2AXX(IM, IMCOL) converts an array of column numbers
%   IMCOL from image IM to an array of x axis values AXX.

% Assume our arguments are sane.

xdata = get(im, 'XData');
ncols = size(get(im, 'CData'), 2);
if length(xdata) == 2
    xdata = [xdata(1) : (xdata(2) - xdata(1)) / (ncols - 1) : xdata(2)];
end
axx = interp1([1 : ncols], xdata, imcol);
