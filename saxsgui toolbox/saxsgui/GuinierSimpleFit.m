function results=GuinierSimpleFit(input1)
% C***********************************************************************
% 
% [results]=GuinierSimpleFit(measured)
%
% Input parameter measured will be either a file name contain a matrix with
% the data (x,y,Delta(y)) or the matrix itself
%
% C***********************************************************************


if nargin==1
    Full=1; % will do full analysis
    NoGui=0; % will have gui interfaces
end
if nargin==2
    NoGui=0; % will have GUI interfaces
end

if nargin>0    
    if isstr(input1)
        %if it's a string we assume it is a filename
        aname=input1;
        fid_aname=fopen(aname,'r');
        %determine whether it is space or comma separated
        firstline=fscanf(fid_aname,'%s',120);
        fclose(fid_aname);
        fid_aname=fopen(aname,'r');
        if  isempty(findstr(',',firstline)) % not commaseparated
            IDATA=fscanf(fid_aname,'%g %g %g ',[3 inf]); % a more general input routine should be established in the future
        else
            IDATA=fscanf(fid_aname,'%g, %g, %g ',[3 inf]); 
        end
        
        xtemp=IDATA(1,:);
        ytemp=IDATA(2,:);
        sdtemp=IDATA(3,:);
        %should get an errormessage in here if the file is not found
    elseif size(input1,2)==3
        %if it's not a string then it should by x,y,DeltaY data in row format
        
        aname='saxsgui';
        xtemp=input1(:,1)';
        ytemp=input1(:,2)';
        sdtemp=input1(:,3)';
    elseif size(input1,1)==3 %maybe it has been transposed
        xtemp=input1(1,:);
        ytemp=input1(2,:);
        sdtemp=input1(3,:);
        aname='saxsgui';
    else
        %errormessage should be put in
        disp('To make a Guinier Fit we need to have either a filename(fullpath) or a data-set with errorbars (X,Y,DeltaY)')
    end
end

if (nargin==0)
    Full=1; % will do full analysis
    NoGui=0; % will have gui interfaces
    % if no input params are given than the routine requires entering a file with data
    [aname,adir]=uigetfile('*.*','Name of inputfile');
    fid_aname=fopen([adir,aname],'r');
    IDATA=fscanf(fid_aname,'%g %g %g ',[3 inf]); % this should be changed to accept either comma or space separated variables
    xtemp=IDATA(1,:);
    ytemp=IDATA(2,:);
    sdtemp=IDATA(3,:);
end
fclose('all');

ytemp=max(ytemp,0); % negative Intensity values are turned into zeroes
sdtemp=sdtemp.*(sdtemp>0)+max(sdtemp)*(1-(sdtemp>0)); % negative or zero standard deviations are given maximum errorbars

if nargin<4 && NoGui==0
    guinierfig=figure;
    semilogy(xtemp,ytemp)
    xlabel('q (1/A)')
    ylabel('intensity (a.u.)')
    set(gcf,'Name','Data for Guinier Fit')
end

%now enter init parameters
if ~NoGui
    h=figstatus('First choose q-range for the Guinier Fit',guinierfig);
end
if NoGui
    qmin=max(min(xtemp),1e-7);
    qmax=max(xtemp);
else
    prompt={'Enter q-min (1/A):','Enter q-max (1/A):'};
    def={num2str(min(xtemp)),num2str(max(xtemp)),'1'};
    dlgTitle='Input for q-range in Guinier Fit';
    lineNo=1;
    qanswer=inputdlg(prompt,dlgTitle,lineNo,def);
    qmin=max(str2num(qanswer{1}),0);
    qmax=min(str2num(qanswer{2}),100);
end

%taking out data that's below qmin and above qmax
gooddata=(xtemp>=qmin & xtemp<=qmax);
x=xtemp(gooddata);
y=ytemp(gooddata);
sd=sdtemp(gooddata);
sigy=sdtemp(gooddata); %since sd changes (is squared) later on we use sigy to plot the errorbars on data later on
mtot=max(size(x));
sd=sd.^2; 

%End of Data input

% This next section tries to come up with a good fit using the 
% beaucage expression. 


%first guess followed by fitting the first 3rd of the data to a Gunier
%function
p=[50,0.3,0];
[bx,by]=guiniercalc(p,x,y,sigy);
p(2)=p(2)*sum(y)/sum(by); %normalizing to get a better multiplication factor (P(2))
%halflength=floor(length(x)/3);
halflength=floor(length(x));
p=fminsearch(@guinierchi2,p,optimset('MaxIter',10000,'MaxFunEvals',10000),x(1:halflength),y(1:halflength),sd(1:halflength));
rg=p(1); % this is the first estimate of Rg
[qdata,Guinier]=guiniercalc(p,x,y,sd)


results=p