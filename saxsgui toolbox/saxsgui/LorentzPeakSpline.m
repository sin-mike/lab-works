function Icalc=LorentzPeakSpline(q,I,varargin)
% Description
% This function calculates 1D data for a gaussian peak with a sloped
% background
% Description end
% Number of Parameters: 5
% parameter 1: Centre : Centre of Lorentzian
% parameter 2: Amp : Amplitude of Lorentzian
% parameter 3: Width : FWHM of Lorentzian
% parameter 4: Inten : Background intensity of Lorentzian
% parameter 5: B_exp : exponential slope of the background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
%LorentzPeakSlope: This function fits a Lorentziean function with an
%exponential background to the peak . The description for the
%Lorentzian function originates from http://mathworld.wolfram.com/LorentzianFunction.html

global fitpar
values=varargin{1};
Centre=values(1);
Amp=values(2);
Width=values(3);
Inten=values(4);
B_exp=values(5);
equation_amp=Amp*pi*Width/2;


Icalc=equation_amp/(2.*pi).*Width./((q-Centre).^2+(0.5.*Width).^2)+Inten.*exp(B_exp.*q.^2);
%--------------------------------------------------------------------------
