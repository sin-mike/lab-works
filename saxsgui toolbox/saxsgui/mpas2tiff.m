function  mpas2tiff
%MPAS2TIFF convert many mpa files to a tiff files 
%     mpas2tiff; loads, converts and save a user designated list of mpa
%     files to tiff files with the same name root but with .tif extensions
%   
%   This routine works best for Windows versions where one can choose
%   individual files..for linux versions one has to choose all the files in
%   a directory.

[path,files]=getfiles('Choose MPA files you want to convert');
for ii=1:length(files)
    pathname=fullfile(path,files{ii});
    mpa2tiff(pathname,'same')
end

    
    

