function [Ibin,qbin,phibin]=linearbinning(si,z)
%this is a binning function, simple binning...
%matrix I is binned so that it is reduced in size by a factor of z
%the q and phi values are averaged accordingly.
%as opposed to the first version (linearbinning.m), this one calculates the q and phi
%afterwards, and uses a faster interpolating algorithm for the actual
%binning.

%check whether the columns of I are divisible by z
if round(size(si.pixels,1)/z)~=size(si.pixels,1)/z
    error('error: the %d columns of the matrix are not divisible by the factor %d',size(I,1),z);
elseif round(size(si.pixels,2)/z)~=size(si.pixels,2)/z
    error('error: the %d rows of the matrix are not divisible by the factor %d',size(I,2),z);
elseif size(si.pixels,1)/z ~=size(si.pixels,2)/z
    error('image not square, can be solved, contact the team.');
end

%determine the reduction factor.
%assuming square images.
range=z/2:z:max(size(si.pixels));

%create thumbnail
Ibin=interp2(si.pixels,range,range','linear');
%raw center wrong?? I don't understand the (z-1)/(z*2) term, but it appears
%to bring it closest to the result of linearbinning.m (the first version).
%[qbin,phibin]=create_qphi_gen(Ibin,si.raw_center(2)/z+(z-1)/(2*z),si.raw_center(1)/z+(z-1)/(2*z),si.kcal/si.pixelcal(2)*z,si.kcal/si.pixelcal(1)*z,si.wavelength);
[qbin,phibin]=create_qphi_gen(Ibin,(si.raw_center(2))/z+0.5,(si.raw_center(1))/z+0.5,si.kcal/si.pixelcal(2)*z,si.kcal/si.pixelcal(1)*z,si.wavelength);

