function argout = meannum(X)
%MEANNUM Mean numeric values, leaving out NaNs.
%   MEANNUM(X), where X is a vector, returns the arithmetic mean of numeric
%   elements of X, disregarding NaN elements.  In other words, MEANNUM([1
%   2], MEANNUM([1 nan 2]), and MEANNUM([1 nan nan 2]) are all 1.5.
%   When X is an array, MEANNUM returns a row vector containing MEANNUM of
%   each column in X.
%
%   If all elements of the vector in question are NaN, MEANNUM returns NaN.

% I can't think of a 'matrix way' to do this.  I'll just do it by columns.

if ~ (isnumeric(X) || ischar(X))
    error('Argument must be numeric or character array.')
end

if ndims(X) > 2
    error('Sorry, I only work on vectors and 2-dimensional arrays.')
end

if any(size(X) == 1)  % we have a vector
    X(isnan(X)) = [];  % disappear NaNs
    if length(X)  % if any numbers at all
        argout = sum(X) / length(X);
    else
        argout = nan;
    end
else  % average each column of array
    argout = [];
    for col = X  % each column
        col(isnan(col)) = [];  % disappear NaNs
        if length(col)  % if any numbers
            m = sum(col) / length(col);
        else
            m = nan;
        end
        argout = [argout, m];  % accumulate column means
    end
end
