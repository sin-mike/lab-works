function imrow = ax2imy(im, axy)
%AX2IMY Convert an axis x value to the nearest image pixel rowumn.
%   IMCOL = AX2IMY(IM, AXY) converts an y axis value AXY to the nearest
%   image pixel row IMrow in the image IM.  AXY is an y value in
%   the axes containing IM.
%
%   If AXY is a vector or array, AX2IMY returns a like-sized
%   array of image row numbers.

% Assume our arguments are sane.

ydata = get(im, 'YData');
nrows = size(get(im, 'CData'), 1);
if length(ydata) == 2
    ydata = [ydata(1) : (ydata(2) - ydata(1)) / (nrows - 1) : ydata(2)];
end
imrow = interp1(ydata, [1 : nrows], axy, 'nearest');
