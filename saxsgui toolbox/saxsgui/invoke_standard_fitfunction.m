function outnum=invoke_standard_fitfunction

% this functions makes sure that all the "standard" 
% fit functions are called. 

var=[1,1,1,1,1,1,1,1,1,1];
q=0.1;
I=0.1;
%standard 1d fit functions
outnum=JJ_cylinder_dilute_monodisp(q,I,var);
outnum=JJ_cylinder_elliptical_dilute_monodisp(q,I,var);
outnum=JJ_ellipsoid_dilute_monodisp(q,I,var);
outnum=JJ_ellipsoid_of_rev_dilute_monodisp(q,I,var);
outnum=JJ_fractal_teixera(q,I,var);
outnum=JJ_inf_thin_disk_dilute_monodisp(q,I,var);
outnum=JJ_inf_thin_rod_dilute_monodisp(q,I,var);
outnum=JJ_polymers_flexible_gaussian_dilute(q,I,var);
outnum=JJ_spheres_dilute_monodisp(q,I,var);
outnum=JJ_spheres_dilute_polydisp_gaussian(q,I,var);
outnum=JJ_spheres_dilute_polydisp_gaussian_not_volume_weighted(q,I,var);
outnum=JJ_spheres_dilute_polydisp_SZ(q,I,var);
outnum=JJ_spheres_hard_monodisp(q,I,var);
outnum=JJ_spheres_stickyhard_monodisp(q,I,var);
outnum=JJ_spherical_2shell_dilute_monodisp(q,I,var);
outnum=JJ_spherical_3shell_dilute_monodisp(q,I,var);
outnum=JJ_spherical_shell_dilute_monodisp(q,I,var);
outnum=JJ_toroid_dilute_monodisp(q,I,var);
%standard 2d fit functions -not so useful
outnum=template2D(q,I,var);
outnum=orientationdistribution(q,I,var);
outnum=orientationdistributionbool(q,I,var);
outnum=orientationdistribution2(q,I,var);
%standard function for flatfield
outnum=ff_cosine(q,I,var);



