function    reduc_par_out=watercalib_par_init
global watercalib_par_temp abs_int_fact_last
% this routine provides initial parameters for the reduction parameter edit
% windows.
% Somewhere in the future it may be able to "know" what was previously
% entered in the last session...but for now it knows what was entered last
% in this session

if ~isempty(watercalib_par_temp) 
    reduc_par_out=watercalib_par_temp;
else
    
    a.sample_check=0;
    a.sample_trans=1.0;
    
    a.mask_check=0;
    a.maskname='filename';
    
    a.flatfield_check=0;
    a.flatfieldname='filename';
    
    a.empty_check=0;
    a.empty_trans=1.0;
    a.emptyname='filename';
    
    a.dark_check=0;
    a.darkcurrentname='filename';
    
    a.readout_check=0;
    a.readoutnoisename='filename';
    
    a.zinger_check=0;
    
    b(1)=a;
    b(2)=a;
    b(3)=a;
    b(4)=a;
    b(5)=a;
    b(1).saved=0;
    b(2).saved=0;
    b(3).saved=0;
    b(4).saved=0;
    b(5).saved=0;
    b(1).description='Description';
    b(2).description='Description';
    b(3).description='Description';
    b(4).description='Description';
    b(5).description='Description';
    
    a.saved=b;

    
    reduc_par_out=a;
    watercalib_par_temp=reduc_par_out;
end
    