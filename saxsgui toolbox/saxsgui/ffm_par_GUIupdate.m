function    ffm_par_GUIupdate(handles)
global ffm_par_temp
% this routine updates the window where Flatfield Maker parameters can be entered
% to reflect the changes that are made interactively
% It is built on the reduct_par_GUIupdate routine develope for entering
% reduction parameters

% hObject is the handle for the figure window
% handles are all the handles referred to by name f.eks. handle.sample
%ffm_par_temp is a structure that holds all the relevant values
% 
% this routine only changes the GUI it does not change any underlying
% parameter values in the structure
rdt=ffm_par_temp; %

if (rdt.lowint_check==1)
    set(handles.lowint_check,'Value',1)
    set(handles.text9,'Enable','On')
    set(handles.lowint_trans,'Enable','On')
    set(handles.lowintname,'Enable','On')
    set(handles.lowintfffile_get,'Enable','On')
else
    set(handles.lowint_check,'Value',0)
    set(handles.text9,'Enable','Off')
    set(handles.lowint_trans,'Enable','Off')
    set(handles.lowintname,'Enable','Off')
    set(handles.lowintfffile_get,'Enable','Off')
end
set(handles.lowintname,'UserData',rdt.lowintname)
[PATHSTR,NAME,EXT] = fileparts(rdt.lowintname);
set(handles.lowintname,'String',NAME)
set(handles.lowint_trans,'String',num2str(rdt.lowint_trans))

if (rdt.mask_check==1)
    set(handles.mask_check,'Value',1)
    set(handles.maskname,'Enable','On')
    set(handles.maskfile_get,'Enable','On')
else
    set(handles.mask_check,'Value',0)
    set(handles.maskname,'Enable','Off')
    set(handles.maskfile_get,'Enable','Off')
end
set(handles.maskname,'UserData',rdt.maskname)
[PATHSTR,NAME,EXT] = fileparts(rdt.maskname);
set(handles.maskname,'String',NAME)


if (rdt.highint_check==1)
    set(handles.highint_check,'Value',1)
    set(handles.highintname,'Enable','On')
    set(handles.highintfile_get,'Enable','On')
    set(handles.highintcenname,'Enable','On')
    set(handles.highintcentfile_get,'Enable','On')    
else
    set(handles.highint_check,'Value',0)
    set(handles.highintname,'Enable','Off')
    set(handles.highintfile_get,'Enable','Off')
    set(handles.highintcenname,'Enable','Off')
    set(handles.highintcentfile_get,'Enable','Off')    
end
set(handles.highintname,'UserData',rdt.highintname)
[PATHSTR,NAME,EXT] = fileparts(rdt.highintname);
set(handles.highintname,'String',NAME)
set(handles.highintcenname,'UserData',rdt.highintcenname)
[PATHSTR,NAME,EXT] = fileparts(rdt.highintcenname);
set(handles.highintcenname,'String',NAME)


if (rdt.empty_check==1)
    set(handles.empty_check,'Value',1)
    set(handles.text17,'Enable','On')
    set(handles.empty_trans,'Enable','On')
    set(handles.emptyname,'Enable','On')
    set(handles.emptyfile_get,'Enable','On')
else
    set(handles.empty_check,'Value',0)
    set(handles.text17,'Enable','Off')
    set(handles.empty_trans,'Enable','Off')
    set(handles.emptyname,'Enable','Off')
    set(handles.emptyfile_get,'Enable','Off')
end
set(handles.emptyname,'UserData',rdt.emptyname)
[PATHSTR,NAME,EXT] = fileparts(rdt.emptyname);
set(handles.emptyname,'String',NAME)
set(handles.empty_trans,'String',num2str(rdt.empty_trans))

if (rdt.dark_check==1)
    set(handles.dark_check,'Value',1)
    set(handles.darkcurrentname,'Enable','On')
    set(handles.darkcurrentfile_get,'Enable','On')
else
    set(handles.dark_check,'Value',0)
    set(handles.darkcurrentname,'Enable','Off')
    set(handles.darkcurrentfile_get,'Enable','Off')
end
set(handles.darkcurrentname,'UserData',rdt.darkcurrentname)
[PATHSTR,NAME,EXT] = fileparts(rdt.darkcurrentname);
set(handles.darkcurrentname,'String',NAME)




