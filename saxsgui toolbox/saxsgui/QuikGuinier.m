function Icalc=QuikGuinier(q,I,varargin)
% Description
% This function calculates a 1d Scattterin function according to the Guinier equation 
% Description end
% Number of Parameters: 3
% parameter 1: Io : Intensity extrapolated to 0
% parameter 2: Rg : Radius of Gyration
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can put additional info
% 
%function written by Karsten Joensen, 2007.

i=1;
values=varargin{1};
Io=values(i);i=i+1;
Rg=values(i);


%this is the Debye fitting function, that describes the decay at
%intermediate angles.
Icalc=Io*exp(-1/3*Rg^2*q.^2);