function Icalc=PearsonPeakSpline(q,I,varargin)
% Description
% This function calculates 1D data for a PearsonVII peak with a sloped
% background
% Description end
% Number of Parameters: 6
% parameter 1: Centre : Centre of PearsonVII
% parameter 2: Amp : Amplitude of PearsonVII
% parameter 3: Width : FWHM of PearsonVII
% parameter 4: Tailing : Tailing parameter of PearsonVII
% parameter 5: Backg : Background of PearsonVII
% parameter 6: Slope : Exponential Slope of the PearsonVII
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% PearsonPeakSpline
% This fits a PearsonVII function to the data, according to the Pearson
% description as given in the documentation of the CCP13 XFIT prorgam.  If
% Tailing =1 then the peak takes a lorentzian shape, if Tailing=inf, the
% curve becomes gaussian in nature. 
global fitpar
values=varargin{1};
Centre=values(1);
Amp=values(2);
Width=values(3);
Tailing=values(4);
Inten=values(5);
B_exp=values(6);


Icalc=Amp./(1+4.*((q-Centre)./Width).^2.*(2^(1./Tailing)-1)).^Tailing+Inten.*exp(B_exp.*q.^2);
%--------------------------------------------------------------------------
