%SAXSGUI_settings 
%   SAXSGUI_settings creates global variables providing information about
%   the specific installation
%
%   To change these parameters, edit saxs_settings.m.

% Enter each parameter, an equal sign with optional spaces around it,
% the parameter value, and finally a semicolon.  Enclose character string
% value within single quotes.
%
% DO NOT DELETE ANY PARAMETERS.  SAXS-GUI programs depend on the variables in
% this file.

global saxsgui_locale
global use_saxsgui_mex
global spec_installation  spec_saxsimage_dir
global fitfunction_dir_2D fitfunction_dir_1D
global wavelength_default
global executableversion allow_SAXSGUI_beta

%versiontype tags
executableversion=1; %This is to flag whether the version is an executable or not
allow_SAXSGUI_beta=0;  %This is to flag whether beta-version features are to be shown
                %1= show betaversion feature; 0= do not show
                
%site tag
saxsgui_locale='SAXSLAB'; %this is to flag special facility conventions (f.x. lund)
wavelength_default=1.5405; %this is the wavelength default for the site (1.54 is CuKalpha)

%c and mex files related issues
use_saxsgui_mex=0;  % this is a flag to use the provided mex files 0 = No!
                    % NB! not yet implemented fully
                    % using them will speed up multiple file routines

%spec related parameters
spec_installation='no'; % can be 'yes' or 'no'
spec_saxsimage_dir='./data'; % this specifies the directory where the 
%image files are placed when running with SPEC

%this is the location of the fitfunctions
%We used to have these hardwired but now that's only done for the
%executable version (which may still not work) for the non-executable
%version we have them located dynamically based on where the 
% JJ_spheres_hard_monodisp is found in the matlab path
if executableversion==1
    fitfunction_dir_1D='../saxsgui_ff_1d';
    fitfunction_dir_2D='../saxsgui_ff_2d';
else
    fitfunction_dir_1D=fileparts(which('JJ_spheres_hard_monodisp'));
    fitfunction_dir_2D=fileparts(which('template2D'));
end

    
    