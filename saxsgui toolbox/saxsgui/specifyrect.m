function varargout = specifyrect(varargin)
%SPECIFYRECT Interactively specify a rectangle within a SAXS image.
%   ** This code is inactive for now.  It may be broken.  Use at your own
%   risk! **
%
%   RECT = SPECIFYRECT(S, AX) displays a dialog window where you specify
%   the coordinates of a rectangle within the SAXS image S.  AX is the
%   handle of an axes object displaying S.  You set coordinates by dragging
%   within the axes and by adjusting the coordinates in the dialog window.
%   SPECIFYRECT returns the coordinates in a four-element vector, [xmin,
%   ymin; xmax, ymax]. Coordinates are in terms of the coordinate system in
%   effect for S, which could be x,y pixels, K values, or radius pixels and
%   theta degrees.  If you cancel the dialog [or we encounter an error?], 
%   SPECIFYRECT returns an empty array.
%
%   See also:  SAXS; SAXS/IMAGE; SAXSGUI; DRAGBOX.

%      SPECIFYRECT, by itself, creates a new SPECIFYRECT or raises the existing
%      singleton*.
%
%      H = SPECIFYRECT returns the handle to a new SPECIFYRECT or the handle to
%      the existing singleton*.
%
%      SPECIFYRECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPECIFYRECT.M with the given input arguments.
%
%      SPECIFYRECT('Property','Value',...) creates a new SPECIFYRECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before specifyrect_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to specifyrect_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help specifyrect

% Last Modified by GUIDE v2.5 09-Apr-2003 10:39:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @specifyrect_OpeningFcn, ...
                   'gui_OutputFcn',  @specifyrect_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before specifyrect is made visible.
function specifyrect_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to specifyrect (see VARARGIN)

% Choose default command line output for specifyrect
handles.output = [];

% Get the arguments we require
for iarg = 1:length(varargin)
    arg = varargin{iarg};
    if isa(arg, 'saxs')
        handles.S = arg;
    elseif ishandle(arg) && isequal(get(arg,'type'),'axes')
        handles.im = arg;
        % Note to self:  Change handles.im to handles.ax.
    end
end
if ~ (isfield(handles, 'S') && isfield(handles, 'im'))
    error('I want a SAXS object and an AXES handle for arguments.')
end

% Label axis units
switch handles.S.type
    % Note to self:  Don't bother.  This doesn't cover all bases.
    case 'xy'
        set(handles.xlabel,'String','x, pixels')
        set(handles.ylabel,'String','y, pixels')
    case 'rth'
        set(handles.xlabel,'String','r, pixels')
        set(handles.ylabel,'String','theta, degrees')
end

% Set rectangle to full image for starters
xax = handles.S.xaxis;
yax = handles.S.yaxis;
set(handles.x1text,'String',num2str(xax(1)))
set(handles.x2text,'String',num2str(xax(2)))
set(handles.y1text,'String',num2str(yax(1)))
set(handles.y2text,'String',num2str(yax(2)))

% Calculate increments equivalent to 1 pixel
[nrows ncols] = size(handles.S.pixels);
handles.xinc = (xax(2) - xax(1)) / (ncols - 1);
handles.yinc = (yax(2) - yax(1)) / (nrows - 1);

% Update handles structure
guidata(hObject, handles);

% Wait for user to act
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = specifyrect_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isstruct(handles)  % gui still here?  (not if window closed)
    varargout{1} = handles.output;
    delete(handles.figure1)  % send the window away now
else
    varargout{1} = [];
end



% --- Executes during object creation, after setting all properties.
function x1text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x1text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function x1text_Callback(hObject, eventdata, handles)
% hObject    handle to x1text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of x1text as text
%        str2double(get(hObject,'String')) returns contents of x1text as a double
%          or NaN if the string doesn't convert to a number



% --- Executes during object creation, after setting all properties.
function x1slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x1slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background, change
%       'usewhitebg' to 0 to use default.  See ISPC and COMPUTER.
usewhitebg = 1;
if usewhitebg
    set(hObject,'BackgroundColor',[.9 .9 .9]);
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

% Slider should always be ranged 0 to 1 and valued 0.5.
% See the callback function.
set(hObject,'Value',0.5)


% --- Executes on slider movement.
function x1slider_Callback(hObject, eventdata, handles)
% hObject    handle to x1slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

if get(hObject,'Value') > 0.5
    incr(handles.x1text,handles.xinc)
else
    decr(handles.x1text,handles.xinc)
end
set(hObject,'Value',0.5)  % restore watchpoint


function incr(text_control,inc)
set(text_control,'String',str2double(get(text_control,'String'))+inc)


function decr(text_control,inc)
set(text_control,'String',str2double(get(text_control,'String'))-inc)


% --- Executes during object creation, after setting all properties.
function y2text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y2text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function y2text_Callback(hObject, eventdata, handles)
% hObject    handle to y2text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of y2text as text
%        str2double(get(hObject,'String')) returns contents of y2text as a double


% --- Executes during object creation, after setting all properties.
function y2slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y2slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background, change
%       'usewhitebg' to 0 to use default.  See ISPC and COMPUTER.
usewhitebg = 1;
if usewhitebg
    set(hObject,'BackgroundColor',[.9 .9 .9]);
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

% Slider should always be ranged 0 to 1 and valued 0.5.
% See the callback function.
set(hObject,'Value',0.5)


% --- Executes on slider movement.
function y2slider_Callback(hObject, eventdata, handles)
% hObject    handle to y2slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

if get(hObject,'Value') > 0.5
    incr(handles.y2text,handles.yinc)
else
    decr(handles.y2text,handles.yinc)
end
set(hObject,'Value',0.5)  % restore watchpoint


% --- Executes during object creation, after setting all properties.
function y1text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y1text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function y1text_Callback(hObject, eventdata, handles)
% hObject    handle to y1text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of y1text as text
%        str2double(get(hObject,'String')) returns contents of y1text as a double


% --- Executes during object creation, after setting all properties.
function y1slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y1slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background, change
%       'usewhitebg' to 0 to use default.  See ISPC and COMPUTER.
usewhitebg = 1;
if usewhitebg
    set(hObject,'BackgroundColor',[.9 .9 .9]);
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

% Slider should always be ranged 0 to 1 and valued 0.5.
% See the callback function.
set(hObject,'Value',0.5)


% --- Executes on slider movement.
function y1slider_Callback(hObject, eventdata, handles)
% hObject    handle to y1slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

if get(hObject,'Value') > 0.5
    incr(handles.y1text,handles.yinc)
else
    decr(handles.y1text,handles.yinc)
end
set(hObject,'Value',0.5)  % restore watchpoint


% --- Executes during object creation, after setting all properties.
function x2text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x2text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function x2text_Callback(hObject, eventdata, handles)
% hObject    handle to x2text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of x2text as text
%        str2double(get(hObject,'String')) returns contents of x2text as a double


% --- Executes during object creation, after setting all properties.
function x2slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x2slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background, change
%       'usewhitebg' to 0 to use default.  See ISPC and COMPUTER.
usewhitebg = 1;
if usewhitebg
    set(hObject,'BackgroundColor',[.9 .9 .9]);
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

% Slider should always be ranged 0 to 1 and valued 0.5.
% See the callback function.
set(hObject,'Value',0.5)


% --- Executes on slider movement.
function x2slider_Callback(hObject, eventdata, handles)
% hObject    handle to x2slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

if get(hObject,'Value') > 0.5
    incr(handles.x2text,handles.xinc)
else
    decr(handles.x2text,handles.yinc)
end
set(hObject,'Value',0.5)  % restore watchpoint


% --- Executes on button press in DragButton.
function DragButton_Callback(hObject, eventdata, handles)
% hObject    handle to DragButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Bring the target figure window up and instruct user
figure(get(get(handles.im,'Parent'),'Parent'))
hint = imoverlay(handles.im, 'Drag out a rectangle.');

try
    box = dragbox;
catch
    box = [];
end
delete(hint)

if box
    set(handles.x1text,'String',num2str(box(1,1)))
    set(handles.x2text,'String',num2str(box(2,1)))
    set(handles.y1text,'String',num2str(box(1,2)))
    set(handles.y2text,'String',num2str(box(2,2)))
end

% Bring ourselves back up
figure(handles.figure1)


function h = imoverlay(im, msg)
ax = get(im,'Parent');
h = text('String', msg, 'EraseMode', 'xor', 'Units', 'normalized', ...
    'Position', [0.1, 0.1]);


% --- Executes on button press in OKButton.
function OKButton_Callback(hObject, eventdata, handles)
% hObject    handle to OKButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.output = [ ...
        str2double(get(handles.x1text,'String')), ...
        str2double(get(handles.y1text,'String')); ...
        str2double(get(handles.x2text,'String')), ...
        str2double(get(handles.y2text,'String')) ];
guidata(handles.figure1,handles)
uiresume(handles.figure1)


% --- Executes on button press in CancelButton.
function CancelButton_Callback(hObject, eventdata, handles)
% hObject    handle to CancelButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1)
