function reduc_par_temp=clean_reduc_par_temp(reduc_par_temp)
% this simply transform values called 'filename' (which is a flag for
% empty) into empty values
if strcmp(reduc_par_temp.maskname,'filename')
    reduc_par_temp.maskname='';
end
if strcmp(reduc_par_temp.emptyname,'filename')
    reduc_par_temp.empty_filename='';
end
%if strcmp(reduc_par_temp.solventname,'filename')
%    reduc_par_temp.solvent_filename='';
%end
if strcmp(reduc_par_temp.darkcurrentname,'filename')
    reduc_par_temp.darkcurrentname='';
end
if strcmp(reduc_par_temp.readoutnoisename,'filename')
    reduc_par_temp.readoutnoisename='';
end
if strcmp(reduc_par_temp.flatfieldname,'filename')
    reduc_par_temp.flatfieldname='';
end
