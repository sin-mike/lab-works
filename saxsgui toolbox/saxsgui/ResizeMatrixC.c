/*
* =============================================================
* ResizeMatrix.c
* This Function resizes an input matrix from an X by Y to an I by J where 
I/X and Y/J are not necessarily integers.
* The funtction is called with:
*								DataOutput=ResizeMatrix(InputMatrix, OutputMatrix, 
lcm(size(InputMatrix),size(OutputMatrix)) )
*
* This allows the Lowest Common Multiple (lcm) of the number of matrix rows 
/ cols. to be passed into the function.
* Doing it this way prevents the need for having a seperate MatLab calling 
routine.
* The way it works:
* The function creates a "Cell" matrix (done in the first nest of for 
loops). This matrix is generated from the input matrix
* and is an expanded version of the rows and colums. This Cell matrix is 
then taken by the second nest of for loops and
* recompressed into the output matrix.
*
* To illustrate: Say we wish to take an 8x8 matix and convert it into a 6x6.
* The lcm() function tells us that the lowest common multipe of rows / cols 
is 24. We then work out the minimum size of our
* cell matrix this is given by the CellRowsMax / CellColsMax variables in 
this case: CellRowsMax=24/8 * 24/6 =>12x12 Cell Matrix
* One input matrix entry must then be expanded to fill a portion of the Cell 
matrix, the size of this 'portion' is given by
* lcm()/(input matrix size) in this case 24/8=3x3. We can now populate the 
whole cell matrix.
* We now calculate how many entries in this matrix must be compressed to 
produce one entry inthe output matrix:
* lcm()/(output matrix size) in this case 24/6=4x4. It is here where the use 
of the lcm is evident. This has give us the smallest
* "expanded" matrix which has an integer number of input entires AND output 
entries.... ie. an exact number of output entries
* are given in one "Cell" matrix. AND even if we go to say a 80x80 input 
matrix, =.60x60 output the Cell matrix is still the same size
* reducing memory use and processing time.
* Once the first cell matrix has been dealt with we then move on to the next 
(in this case there are 4 to evaluate):
* (lcm()/cellsize()  rows/ colums of Cell matrices)
* an example is shown below the Cell matrix is the last one evaluated 
corresponding to the bottom right hand quarter of the input matrix
* so to evaluate the ver last output data point for example we calculate the 
average of the bottom right hand 4x4 matrix of this cell matrix
* i.e. (12*9+33*3+15*3+34)/16=17.875

InputMatrix =

    77    86    29    86    70    91    92    49
    64    54    27    29    12    57     0    55
    12    28    66    54    54    70     7    32
    70    13     3    29    96    67     7    79
    53    91    93    81     4    38    23    27
    66     5    25    13     3    91    53    38
    44    68     4    97    92    72    34    15
    37    15    95    44    50    32    33    12


(last) Cell Matrix =

     4     4     4    38    38    38    23    23    23    27    27    27
     4     4     4    38    38    38    23    23    23    27    27    27
     4     4     4    38    38    38    23    23    23    27    27    27
     3     3     3    91    91    91    53    53    53    38    38    38
     3     3     3    91    91    91    53    53    53    38    38    38
     3     3     3    91    91    91    53    53    53    38    38    38
    92    92    92    72    72    72    34    34    34    15    15    15
    92    92    92    72    72    72    34    34    34    15    15    15
    92    92    92    72    72    72    34    34    34    15    15    15
    50    50    50    32    32    32    33    33    33    12    12    12
    50    50    50    32    32    32    33    33    33    12    12    12
    50    50    50    32    32    32    33    33    33    12    12    12


DataOutput =

   74.8125   53.2500   60.9375   62.2500   75.7500   55.1250
   38.7500   43.7500   42.7500   40.6250   33.5000   33.5000
   45.8125   17.7500   31.1250   81.0625   37.3750   52.1875
   59.5625   72.7500   67.0000   15.6250   40.8750   29.9375
   50.3750   25.5000   44.8750   56.0000   62.5000   30.7500
   36.1250   50.2500   61.0000   55.8750   37.6250   17.8750

* Note:In the case of the pointers used a 2D array is actually represented 
by a 1D array listed column ontop of column.
* The final mexFunction is the mat lab requiremnt used to pass the variables 
to/from the function
* using pointers.
* ============================================================
*/

#include "mex.h"
#include "math.h"

void ResizeMatrixC(double *InputMatrix, int mrowsIn, int ncolsIn, double 
*LCM, double *OutputMatrix, int mrowsOut, int ncolsOut){ // The return of th 
funcion is void as matlab recieves pointers to the function output
//void CompressMatrixOdd(double *InputMatrix, int mrowsIn, int ncolsIn, 
double *LCM, double *OutputMatrix, int mrowsOut, int ncolsOut, double 
*CheckMatrix){

		int  NCellsHorMax, NCellsVertMax, InputMatrixCol,InputMatrixRow, 
CellColsMax, CellRowsMax, CellRow, CellCol, RowCompressionF, 
ColCompressionF, RowN,ColN, RowNout, ColNout, ColNstart, RowNstart;
		int NextVertCell, NextHorCell,OutputMatrixCol,OutputMatrixRow;
		double   ColSum, RowSum, CellMatrix[500][500];

		CellRowsMax=*LCM/mrowsIn * *LCM/mrowsOut ;			//Calculate number of rows 
and cols in the cell matrix
			if (mrowsOut>mrowsIn)
				CellRowsMax=mrowsOut;

		CellColsMax=*(LCM+1)/ncolsIn * *(LCM+1)/ncolsOut;
			if (ncolsOut>ncolsIn)
			CellColsMax=ncolsOut;//Expand the Matrix if necessary

		NCellsHorMax=*(LCM+1)/CellColsMax;
		NCellsVertMax=*LCM/CellRowsMax; //Calculate how many Cell matrices 
horizontaly and verticaly must be calculated

		RowCompressionF=*LCM/mrowsOut;
		ColCompressionF=*(LCM+1)/ncolsOut; //calculate how many columns/ rows to 
compress (from the Cell matrix) into one output Col / Row

		for (NextHorCell=0; NextHorCell<NCellsHorMax; NextHorCell=NextHorCell+1) {
			for (NextVertCell=0; NextVertCell<NCellsVertMax; 
NextVertCell=NextVertCell+1) {

				for (CellRow=0; CellRow<CellRowsMax; CellRow=CellRow+1) {
					for (CellCol=0; CellCol<CellColsMax; CellCol=CellCol+1) {
						InputMatrixCol=(floor(CellCol/(*(LCM+1)/ncolsIn))); //Calculate which 
of the input elements corresponds to this Cell element
						InputMatrixCol=InputMatrixCol+NextHorCell*(CellColsMax/(*(LCM+1)/ncolsIn)); 
//shift the cell to take values from the next elements
						InputMatrixRow=(floor(CellRow/(*LCM/mrowsIn)));     //(Duplicating the 
original elements)
						InputMatrixRow=InputMatrixRow+NextVertCell*(CellRowsMax/(*LCM/mrowsIn)); 
//shift the cell to take values from the next elements
						CellMatrix[CellCol][CellRow]=*(InputMatrix+InputMatrixCol*mrowsIn+InputMatrixRow);
//						*(CheckMatrix+(CellCol*CellRowsMax)+CellRow)=CellMatrix[CellCol][CellRow]; 
allows checking of the Cell matrix
					}
				} //Populate the Cell matrix

				for (ColNstart=0, ColNout=0; ColNstart<CellColsMax; 
ColNstart=ColNstart+ColCompressionF, ColNout=ColNout+1){
					for (RowNstart=0,RowNout=0; RowNstart<CellRowsMax; 
RowNstart=RowNstart+RowCompressionF, RowNout=RowNout+1) { //do a colum at a 
time compressing RCF rows into one and place in the CompressedRows array
						for (ColN=0, ColSum=0; ColN<ColCompressionF; ColN=ColN+1) {
							for (RowN=0, RowSum=0; RowN<RowCompressionF; RowN=RowN+1)			//First 
sum the first number of rows
								RowSum=RowSum+CellMatrix[ColNstart+ColN][RowNstart+RowN];
							ColSum=ColSum+RowSum/RowCompressionF;					//Now Sum the Sum of Rows
						}
						OutputMatrixRow=RowNout+NextVertCell*(CellRowsMax/(*LCM/mrowsOut));
						OutputMatrixCol=ColNout+NextHorCell*(CellColsMax/(*(LCM+1)/ncolsOut));
						*(OutputMatrix+OutputMatrixCol*mrowsOut+OutputMatrixRow)=ColSum/ColCompressionF 
;//Now assign this sum to the outpur matrix
					}
				}
			} //Do it all again for the next cell matrix (moving verticaly)
		} //Do it all again for the next cell matrix (moving horizontaly)

		return;
	}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) //This part takes care 
of passing the matlab information to and from the function using pointers
// Call this function with:
//	MatrixOdd(InputMatrix, OutputMatrix, 
[lcm(size(InputMatrix),size(OutputMatrix))] );

{

	int mrowsIn,ncolsIn, CellColsMax, CellRowsMax, mrowsOut, ncolsOut;
	double *InputMatrix, *OutputMatrix , *LCM, *CheckMatrix;

	InputMatrix=mxGetPr(prhs[0]); //Assign input matrix
	mrowsIn = mxGetM(prhs[0]);
	ncolsIn = mxGetN(prhs[0]); //Need to know the number of rows and columns of 
input matrix

	mrowsOut = mxGetM(prhs[1]);
	ncolsOut = mxGetN(prhs[1]); //Determin the size of the matrix to be outputv

	LCM=mxGetPr(prhs[2]); // Retrieve the LCM of the 2 matrices ie size of 
smallest matrix which will fit into the data and the result a while number 
of times

		/* reinsert to allow the check matrix to be passed to the output for 
debugging
		CellRowsMax=*LCM/mrowsIn * *LCM/mrowsOut ;
			if (mrowsOut>mrowsIn)
				CellRowsMax=mrowsOut;

		CellColsMax=*(LCM+1)/ncolsIn * *(LCM+1)/ncolsOut;
			if (ncolsOut>ncolsIn)
				CellColsMax=ncolsOut;
		plhs[0] = mxCreateDoubleMatrix(CellRowsMax, CellColsMax, mxREAL);
		*/

	plhs[0] = mxCreateDoubleMatrix(mrowsOut, ncolsOut, mxREAL); //Generate 
blank matrix for the output
	OutputMatrix=mxGetPr(plhs[0]); // Assign pointer output
//	CheckMatrix=mxGetPr(plhs[1]); // Assign pointer output
//	CompressMatrixOdd(InputMatrix, mrowsIn, ncolsIn, LCM, OutputMatrix, 
mrowsOut, ncolsOut,CheckMatrix); // inputs and outputs to function

	ResizeMatrixC(InputMatrix, mrowsIn, ncolsIn, LCM, OutputMatrix, mrowsOut, 
ncolsOut); // inputs and outputs to function

	return;
}

