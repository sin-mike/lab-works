function    ffm_par_struct_update(handles)
global ffm_par_temp
% this routine updates the structure based on the changes that have gone on in 
% in the GUI. 
%
% hObject is the handle for where the changes are taking place
%
% this routine only changes the structure  it does not change GUI 

rdt=ffm_par_temp; %ffm_par_temp was too long to write

rdt.lowint_check=get(handles.lowint_check,'Value');
rdt.lowint_trans=str2num(get(handles.lowint_trans,'String'));
if isempty(rdt.lowint_trans), rdt.lowint_trans=1; end
if rdt.lowint_trans<=0, warndlg('Negative Transmission is garbage', 'Reduction Parameter Warning'); end
if rdt.lowint_trans>1, warndlg('Transmission > 1 is non-physical', 'Reduction Parameter Warning'); end
rdt.lowintname=get(handles.lowintname,'UserData');

rdt.mask_check=get(handles.mask_check,'Value');
rdt.maskname=get(handles.maskname,'UserData');

rdt.highint_check=get(handles.highint_check,'Value');
rdt.highintname=get(handles.highintname,'UserData');
rdt.highintcenname=get(handles.highintcenname,'UserData');

rdt.empty_check=get(handles.empty_check,'Value');
rdt.empty_trans=str2num(get(handles.empty_trans,'String'));
if isempty(rdt.empty_trans), rdt.empty_trans=1; end
if rdt.empty_trans>1, warndlg('Transmission > 1 is non-physical', 'Reduction Parameter Warning'); end
if rdt.empty_trans<=0, warndlg('Negative Transmission is garbage', 'Reduction Parameter Warning'); end
rdt.emptyname=get(handles.emptyname,'UserData');

rdt.dark_check=get(handles.dark_check,'Value');
rdt.darkcurrentname=get(handles.darkcurrentname,'UserData');


ffm_par_temp=rdt;