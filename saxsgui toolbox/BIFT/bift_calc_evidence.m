function evidence=bift_calc_evidence(alpha,s,c,B,uoffset,mtot)


ntot=max(size(B));

U=B/alpha+eye(size(B))-0.5*uoffset;  %Matrix A+B/alpha in article

rlogdet=log(det(U));

rnorm=0.5*(log(ntot+1.)+ntot*log(0.5));

evidence=rnorm+(alpha*s-0.5*c*mtot)-0.5*rlogdet-log(alpha);  %this is log P((D|Hi,alpha.d)/P(alpha)) eq 18,20
