function minevidence=bift_simplex(p0,ntot0,searchparam,x,y,sd)

dmax=p0(1);
alpha=exp(p0(2));

verboseparam.on=0;
verboseparam.viewstep=50;

mtot=max(size(x));
ntot=ntot0;
dfx=dmax/ntot;

[M,xf]=bift_prior(ntot,y,dfx);
[A]=bift_trans(x,xf);

ysum=(y./sd)*A;

onetemp=1+0*(1:ntot);
SDMAT=sd'*onetemp;
B=A'*(A./SDMAT);
clear SDMAT 
clear onetemp

%This calculates the special determinant matrix.
Bsize=max(size(B));
%creating matrix with "next-to-diagonal"=1 and diagonal=0
uoffset=circshift(eye(Bsize,Bsize),[1,0])+circshift(eye(size(B)),[0,1]);
uoffset(1,Bsize)=0;
uoffset(Bsize,1)=0;

%this calculates the intensity close to 0 for normalization
fm=(A*M')';
summ=sum((y(1:3)./sd(1:3)))/sum(fm(1:3)./sd(1:3));

M=summ.*M;
fprior=1.0005*M;

sigma=(1:ntot)*0+1;
evmax=-1.e10;
searchparam.omega=searchparam.omegaorig;
verboseparam.on=0;

origmaxit=searchparam.maxit;
searchparam.maxit=1000;
[f,c,s,dotsp,fm]=bift_search(alpha,dmax,fprior,M,A,B,ysum,searchparam,verboseparam,x,y,sd,xf);
searchparam.maxit=origmaxit;

%         C************************************************************************
%         C     Estimate of p(r) written to file
%         C     and the probability for this particular solution is calculated
%         C****fv********************************************************************
%         
cnst=1.;
alpha=alpha/(cnst*2.);
s=s*cnst;
evidence=bift_calc_evidence(alpha,s,c,B,uoffset,mtot);
disp([num2str(alpha),' ',num2str(dmax),' ',num2str(evidence)])
if isnan(evidence)
    evidence=-inf;
end
minevidence=-evidence;
