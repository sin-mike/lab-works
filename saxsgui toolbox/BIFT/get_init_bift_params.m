 function [qmax,qmin,ntot0]= get_init_bift_params(qarray,NoGui)

 %now get initial parameters q-range and number of point in pr.
if NoGui
    ntot0=50;
    qmin=max(min(qarray),1e-7);
    qmax=max(qarray);
else
    
    prompt={'Enter q-min (1/A):','Enter q-max (1/A):',...
            'Enter minimum number of points in p(r) (at least 10)'};
    def={num2str(min(qarray)),num2str(max(qarray)),'50','1'};
    dlgTitle='Input for q-range and p(r)';
    lineNo=1;
    qanswer=inputdlg(prompt,dlgTitle,lineNo,def);
    qmin=max(str2num(qanswer{1}),0);
    qmax=min(str2num(qanswer{2}),100);
    ntot0=max(str2num(qanswer{3}),10);
end