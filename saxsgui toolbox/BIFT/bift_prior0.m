
%**************************************************************************
%*     Definition of the prior
%************************************************************************
function [M,xf]=bift_prior(ntot,y,dfx)
xf=dfx*[0:ntot-1];
diam=dfx*(ntot-1);
[M]=bift_distphere(ntot,dfx,y(1),diam);

%                         DO 12 J=1,NTOT
%                         XF(J)=DFX*J
%                         12 CONTINUE
%                         
%                         PPMIN=0.005
%                         DIAM=dfx*ntot
%                         555 CALL DISTSPHERE(M,NTOT,DFX,Y(1),PPMIN,DIAM)
%                         
%                         999  RETURN
%                         END

%********************************************************************* 
%*     Distance distribution for Sphere
%*********************************************************************
function [M]=bift_distphere(ntot,dr,rx,d)
% sum1=0;
% sum2=0;
% sum3=0;
pmin=0.002; %this is inconsistent with the prior routine
av=rx/ntot;
psum=d.^3/24.;          
fact=rx/psum*dr;
dmax=(ntot-1)*dr;
R=dr*(0:ntot-1);
M=R.^2.*(1-1.5*(R/d)+.5*(R/d).^3).*fact;
M=M.*(R<=d);
sum1=sum(M);

avm=pmin*max(M);
M=M.*(M>avm)+avm.*(M<=avm);
sum2=sum(M);

M=M*sum1/sum2;

%                             SUBROUTINE DISTSPHERE(M,NTOT,DR,RX,PMIN,D)
%                             REAL M(NTOT)
%  SUBROUTINE DISTSPHERE(M,NTOT,DR,RX,PMIN,D)
%       REAL M(NTOT)
%       SUM1=0
%       SUM2=0
%       sum3=0
%       pmin=0.002
%       AV=RX/NTOT
%       PSUM=D**3/24.          
%       FACT=RX/PSUM*DR
%       DMAX=(NTOT-1)*DR
%       DO 20 J=1,NTOT
%       R=DR*(J-1)
%       IF(R.GT.D) THEN
%       M(J)=0.
%       GOTO 20
%       ENDIF
%       M(J)=R**2*(1-1.5*(R/D)+.5*(R/D)**3)*FACT
%       if(M(j).gt.sum3) sum3=m(j)
%       SUM1=SUM1+M(J)
%    20 CONTINUE

%       AVM=PMIN*sum3
%       DO 30 J=1,NTOT
%       IF(M(J).LE.AVM) M(J)=AVM
%       SUM2=SUM2+M(J)

%    30 CONTINUE
%       FACT=SUM1/SUM2
%       DO 40 J=1,NTOT
%       M(J)=M(J)*FACT
%    40 CONTINUE
%       RETURN
%       END                           


