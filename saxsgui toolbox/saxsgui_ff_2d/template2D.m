function Icalc=template(q,phi,varargin)
% Description
% This function calculates 2D data for a model system
% of holes, descripbed Wang,Cohen and Ruland 1993 and 
% elaborated by B Pauw, 2006
% Description end
% Number of Parameters: 3
% parameter 1: a : real : Description of a
% parameter 2: b : real : Description b
% parameter 3: c : bool : Description c
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can be put addition info
% 
% Orientationdistribution
% This function takes that evaluates function based on the input values as
% parameters. The sequence of values is very important, and should match
% that of the sequence of identifiers described by the variables cell array.
% I do not know yet how to convert the names in variables to proper
% variables for use in the function.
% this orientation distribution is described by Wang, Cohen and Ruland 1993, Tillinger
% and Ruland, and elaborated by Brian R. Pauw, 2006.
%function written by Brian R. Pauw, 12th of October, 2006.
values=varargin{1};
a=values(1);
b=values(2);
c=values(3);

Icalc=a+b*q.*sin(c*phi);