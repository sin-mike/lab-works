function Icalc=orientationdistribution(q,phi,varargin)
% Description
% This function calculates 2D data for a model system
% of holes, descripbed Wang,Cohen and Ruland 1993 and 
% elaborated by B Pauw, 2006
% Description end
% Number of Parameters: 5
% parameter 1: mu : orientation distribution center: between 90 and 270!
% parameter 2: Bphi : orientation distribution width
% parameter 3: L3 : length of the scatterers
% parameter 4: A : intensity schaling factor
% parameter 5: lc : Correlation length
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can be put additional info
% 
% Orientationdistribution
% This function takes that evaluates function based on the input values as
% parameters. The sequence of values is very important, and should match
% that of the sequence of identifiers described by the variables cell array.
% I do not know yet how to convert the names in variables to proper
% variables for use in the function.
% this orientation distribution is described by Wang, Cohen and Ruland 1993, Tillinger
% and Ruland, and elaborated by Brian R. Pauw, 2006.
%function written by Brian R. Pauw, 12th of October, 2006.

    
values=varargin{1};
mu=values(1);
Bphi=values(2);
L3=values(3);
A=values(4);
lc=values(5);

if sum(sum(phi<mu-90))>0 || sum(sum(phi>mu+90))>0 %if there is a symmetrical peak to be computed (i.e. the pattern repeats itself)
    if mu>270
        mu=mu-180;
    elseif mu<90
        mu=mu+180;
    end
    Uppen=phi<mu-90;
    Lower=phi>mu+90;
    phi=phi+Uppen*180;% I think this is one of the fastest methods to do it --> circumvent "find"
    phi=phi-Lower*180;
end

Bobs=sqrt(4*pi^2./(q.^2.*L3^2)+Bphi^2); %this is given by Ruland et al.
Iphi=1./(Bobs.*sqrt(2*pi)).*exp(-(phi-mu).^2./(2.*Bobs.^2)); %and this is the gaussian distribution function
%Iphi=exp(-(phi-mu).^2./(2.*Bobs.^2)); %and this is the gaussian distribution function
%this is the porod fitting function, that describes the decay at higher
%angles
%Icalc=Iphi.*(k./(2.*pi.*(q./(2.*pi)).^4.*lp));
Icalc=Iphi.*(A./((1+q.^2.*lc^2).^2));
