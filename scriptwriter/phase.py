from uuid import uuid1
from jinja2 import Template
import datetime
import sys
import csv


def main():
    coordinatesPath = sys.argv[1]
    with open(coordinatesPath, 'r') as coordinatesFile:
        coordinates = csv.reader(coordinatesFile, delimiter=',')
        samples = []
        xtable = []
        ytable = []
        for c in coordinates:
            samples.append(c[0])
            xtable.append(float(c[1]))
            ytable.append(float(c[2]))
        print samples
        print xtable
        print ytable

    repeat = 100
    username = "SINTSOV"
    experiment_name = "PHASE_TRANSITION_10"
    REALTIME = 30

    uuid = str(uuid1())
    LIVETIME = REALTIME + 2
    BIAS = 13.2

    head = """;; SCRIPT UUID = {{UUID}}
;; ESTIMATED RUNTIME = {{RUNTIME}}
SCRIPT START
REALTIME({{REALTIME}})
LIVETIME({{LIVETIME}})

"""
    head = Template(head)

    section = """
TEMP({{TEMPERATURE}})
WAIT({{WAIT}})

"""
    section = Template(section)

    body = """
;; {{PREFIX}}
MPA3 CLEAR
DATA_INFO({{DATANAME}},{{USERNAME}},,,,,,,,,)
MM({{POSX}},{{POSY}})
MPA3 START
WAIT FOR MPA3
MPA3 STORE()

"""
    body = Template(body)

    tail = """
SCRIPT END

"""
    tail = Template(tail)

    script = ""
    ert = 0

    for r in range(repeat):
        for j in range(len(samples)):
            # moving relative across the sample in Y axis
            script += body.render({
                "PREFIX": "%d of %d" % (
                    r * len(xtable) + j + 1,
                    repeat * len(xtable)),
                "DATANAME": experiment_name +
                " Sample %s SC1 SOURCE %s" % (
                    samples[j], uuid),
                "USERNAME": username,
                "POSX": xtable[j],
                "POSY": ytable[j]
            })
            ert += LIVETIME + BIAS
            print r * len(xtable) + j + 1

    script = head.render({
        "UUID": uuid,
        "RUNTIME": datetime.timedelta(seconds=ert),
        "REALTIME": REALTIME,
        "LIVETIME": LIVETIME
        }) + script

    script += tail.render()
    with open("./scripts/" +
              str(datetime.datetime.now().strftime('%Y%m%d')) +
              "_" +
              experiment_name +
              ".dat", "w") as f:
        f.write(script)

if __name__ == '__main__':
    main()
