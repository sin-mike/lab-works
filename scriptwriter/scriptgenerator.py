from uuid import uuid1
from jinja2 import Template
import datetime
import sys
import csv
import os


class ScriptGenerator(object):

    """docstring for ScriptGenerator"""

    user_name = None
    script_name = None
    experiment_name = None
    livetime = None
    realtime = None
    servicetime = None
    repeat = None
    uuid = None
    blankx = None
    blanky = None
    chamber = None
    temperature = None
    temperature_wait = None
    estimatedRunTime = None

    scriptHead = None
    scriptBody = None
    scriptTail = None

    headTemplate = None
    bodyTemplate = None
    sectionTemplate = None
    tailTemplate = None

    estimatedRunTime = None

    def __init__(self, settings=None, debug=False):
        super(ScriptGenerator, self).__init__()

        self.debug = debug

        self.resetParameters()

        if settings is not None:
            self.updateSettings(settings)

        self.updateTemplate(
            os.path.join(os.path.dirname(__file__),
                         'templates')
        )

    def resetParameters(self):
        self.user_name = "PYWRITER"

        self.experiment_name = "NONAME"
        self.script_name = self.experiment_name
        self.realtime = 10
        self.livetime = self.realtime + 2
        self.servicetime = 13.2
        self.repeat = 1
        self.temperature_wait = 600
        self.temperature = 666
        self.blankx = 0.0
        self.blanky = 0.0
        self.chamber = "SC1"

        self.scriptHead = ""
        self.scriptBody = ""
        self.scriptTail = ""

        self.estimatedRunTime = 0.0

    def clearParameters(self):
        self.user_name = None
        self.script_name = None
        self.experiment_name = None
        self.livetime = None
        self.realtime = None
        self.repeat = None
        self.uuid = None
        self.temperature_wait = None
        self.scriptHead = ""
        self.scriptBody = ""
        self.scriptTail = ""
        self.estimatedRunTime = 0.0

    def scriptParameters(self):
        pars = self.__dict__.copy()
        del pars['bodyTemplate']
        del pars['headTemplate']
        del pars['sectionTemplate']
        del pars['tailTemplate']
        del pars['debug']
        return pars

    def updateTemplate(self, rootdir=None):
        if rootdir is None:
            rootdir = './'
        try:
            fname = os.path.join(rootdir, 'head.txt')
            with open(fname, 'r') as f:
                self.head = Template(f.read())
        except Exception, e:
            if self.debug:
                print "ScriptGenerator failed to load head template %s" % fname, e
            raise e

        try:
            fname = os.path.join(rootdir, 'section.txt')
            with open(fname, 'r') as f:
                self.section = Template(f.read())
        except Exception, e:
            if self.debug:
                print "ScriptGenerator failed to load section template %s" % fname, e
            raise e

        try:
            fname = os.path.join(rootdir, 'body.txt')
            with open(fname, 'r') as f:
                self.body = Template(f.read())
        except Exception, e:
            if self.debug:
                print "ScriptGenerator failed to load body template %s" % fname, e
            raise e

        try:
            fname = os.path.join(rootdir, 'tail.txt')
            with open(fname, 'r') as f:
                self.tail = Template(f.read())
        except Exception, e:
            if self.debug:
                print "ScriptGenerator failed to load tail template %s" % fname, e
            raise e

    def updateSettings(self, settings=None):
        if settings is not None:
            self.user_name = str(settings.get("user_name", self.user_name))
            self.script_name = str(
                settings.get("script_name", self.script_name))
            self.experiment_name = str(
                settings.get("experiment_name", self.experiment_name))
            self.servicetime = float(
                settings.get("servicetime", self.servicetime))
            self.repeat = int(settings.get('repeat', self.repeat))
            self.temperature_wait = int(
                settings.get('temperature_wait', self.temperature_wait))
            self.blankx = float(settings.get("blankx", self.blankx))
            self.blanky = float(settings.get("blanky", self.blanky))
            self.chamber = str(settings.get("chamber", self.chamber))

            if "exposure" in settings:
                self.realtime = int(settings["exposure"])
                self.livetime = self.realtime + 2
            if "realtime" in settings:
                self.realtime = int(settings["realtime"])
            if 'livetime' in settings:
                self.livetime = int(settings["livetime"])

    def refresh(self):
        # recalculate ert and stuff
        self.addScriptHead()

    def getScriptText(self):
        self.refresh()
        script = self.scriptHead + "\n\n" + \
            self.scriptBody + "\n\n" + self.scriptTail
        return script

    def addScriptHead(self, pars=None):
        self.uuid = str(uuid1())

        if pars is None or type(pars) != dict:
            pars = {
                "UUID": self.uuid,
                "RUNTIME": self.datetime.timedelta(seconds=self.estimatedRunTime),
                "REALTIME": self.realtime,
                "LIVETIME": self.livetime
            }

        head = self.headTemplate.render(pars)

        # head can be only one
        self.head = head
        return head

    def addScriptTail(self, pars=None):
        if pars is None or type(pars) != dict:
            pars = {}
        tail = self.tailTemplate.render(pars)

        # tail can be only one
        self.tail = tail
        return tail

    def addScriptBody(self, pars=None):
        if pars is None or type(pars) != dict or len(pars) == 0:
            return ""

        joint = self.bodyTemplate.render(pars)
        self.scriptBody += "\n\n" + joint

        self.estimatedRunTime += min(self.livetime,
                                     self.realtime) + self.servicetime

        return joint

    def addScriptSection(self, pars=None):
        if pars is None or type(pars) != dict:
            pars = {"WAIT": self.temperature_wait,
                    "TEMP": "%.2f" % self.temperature}
        section = sectionTemplate.render(pars)

        # section is summed with script body
        self.scriptBody += section
        return section

    def generateSimpleScript(self, data=None):
        if data is None:
            if self.debug:
                print "None to write"
            return None

        sampleNumber = len(data)
        if sampleNumber < 1:
            if self.debug:
                print "0 samples to write"
            return None

        if self.debug:
            print "%d samples to write" % sampleNumber

        repeat = self.repeat
        username = self.user_name
        experiment_name = self.experiment_name
        REALTIME = self.realtime
        LIVETIME = self.livetime
        BIAS = self.servicetime

        ert = 0

        for r in range(repeat):
            for j, d in enumerate(data):
                sampleName, sampleX, sampleY = d
                try:
                    body += "\n\n"
                    body += bodyTemplate.render({
                        "PREFIX": "%d of %d" % (
                            r * sampleNumber + j + 1,
                            repeat * sampleNumber),
                        "DATANAME": "_EXPERIMENT " + experiment_name +
                        " _SAMPLE %s _CHAMBER SC1 _CYCLE %d _SCRIPTID %s" % (
                            sampleName, r + 1, uuid),
                        "USERNAME": username,
                        "POSX": sampleX,
                        "POSY": sampleY
                    })
                    ert += LIVETIME + BIAS
                    if self.debug:
                        print r * sampleNumber + j + 1, d
                except Exception, e:
                    if self.debug:
                        print "failed to write sample", j, d
                        print e
                    raise e

        self.addScriptHead()
        self.addScriptTail()

        self.estimatedRunTime = ert

        return getScriptText()

    def generateTemperatureScript(self, data=None):
        if data is None:
            if self.debug:
                print "None to write"
            return None

        sampleNumber = len(data)
        if sampleNumber < 1:
            if self.debug:
                print "0 samples to write"
            return None

        if self.debug:
            print "%d samples to write" % sampleNumber

        repeat = self.repeat
        username = self.user_name
        experiment_name = self.experiment_name
        REALTIME = self.realtime
        uuid = self.uuid = str(uuid1())
        LIVETIME = self.livetime
        BIAS = self.servicetime
        WAIT = self.temperature_wait

        headTemplate, bodyTemplate, tailTemplate, sectionTemplate = self.headTemplate, self.bodyTemplate, self.tailTemplate, self.sectionTemplate
        currentTemperature = 666  # as hot as hell
        script = ""
        ert = 0

        for r in range(repeat):
            for j, d in enumerate(data):
                sampleName, sampleX, sampleY, sampleT = d
                if sampleT != currentTemperature:
                    try:
                        script += sectionTemplate.render({
                            "WAIT": WAIT,
                            "TEMP": "%.2f" % sampleT
                        })
                        currentTemperature = sampleT
                        ert += WAIT
                    except Exception, e:
                        if self.debug:
                            print "failed to write section", j, d
                            print e
                        raise e
                try:
                    script += bodyTemplate.render({
                        "PREFIX": "%d of %d" % (
                            r * sampleNumber + j + 1,
                            repeat * sampleNumber),
                        "DATANAME": experiment_name +
                        " %s CYCLE %d TEMP %.2f SC1 SOURCE %s" % (
                            sampleName, r + 1, sampleT, uuid),
                        "USERNAME": username,
                        "POSX": sampleX,
                        "POSY": sampleY
                    })
                    ert += LIVETIME + BIAS
                    if self.debug:
                        print r * sampleNumber + j + 1, d
                except Exception, e:
                    if self.debug:
                        print "failed to write sample", j, d
                        print e
                    raise e

        script = headTemplate.render({
            "UUID": uuid,
            "RUNTIME": datetime.timedelta(seconds=ert),
            "REALTIME": REALTIME,
            "LIVETIME": LIVETIME
        }) + script

        script += tailTemplate.render()

        self.script = script
        self.estimatedRunTime = ert

        return script


def main():
    """
    This program reads names and coordinates of samples from csv file and
    generates script for it.

    UPD: added temperature information as 4th column in csv file
    """

    coordinatesPath = sys.argv[1]
    with open(coordinatesPath, 'r') as coordinatesFile:
        coordinates = csv.reader(coordinatesFile, delimiter=',')
        coordinates = list(coordinates)
        data = []
        tempStatus = "OFF"
        if len(coordinates[0]) == 4:
            tempStatus = "ON"
        for c in coordinates:
            try:
                sampleName = c[0]
                sampleX = float(c[1])
                sampleY = float(c[2])
                if tempStatus == "OFF":
                    data.append((sampleName, sampleX, sampleY))
                elif tempStatus == "ON":
                    sampleTemp = float(c[3])
                    data.append((sampleName, sampleX, sampleY, sampleTemp))
            except Exception, e:
                print "unable to append row", c, e

    coordName = os.path.split(coordinatesPath)[-1]
    coordName = os.path.splitext(coordName)[0]
    print coordName

    scriptSettings = {
        "user_name": "SINTSOV",
        "experiment_name": "TEMPERATURE MOSAICITY",
        "exposure": 60,
        "repeat": 10,
        "temperature_wait": 600
    }

    if scriptSettings.get("experiment_name", None) is None:
        scriptSettings['experiment_name'] = coordName

    scrGen = ScriptGenerator(scriptSettings)

    if tempStatus == "OFF":
        script = scrGen.generateSimpleScript(data)
    elif tempStatus == "ON":
        script = scrGen.generateTemperatureScript(data)

    scriptPath = ("./scripts/" +
                  str(datetime.datetime.now().strftime('%Y%m%d')) +
                  "_" +
                  scrGen.script_name +
                  ".dat")
    print 'writing script to %s' % scriptPath
    with open(scriptPath, "w") as f:
        f.write(script)

if __name__ == '__main__':
    main()
