SELECT DISTINCT
XCenter, 
YCenter, 
RowLetter,
ColumnNumber, 
[RockMaker].[dbo].[ImagingTask].DateImaged
  FROM [RockMaker].[dbo].[CaptureResult] 
  INNER JOIN [RockMaker].[dbo].[Region] 
  ON [RockMaker].[dbo].[CaptureResult].[RegionID]=[RockMaker].[dbo].[Region].[ID]
  INNER JOIN [RockMaker].[dbo].[WellDrop] 
  ON [RockMaker].[dbo].[Region].[WellDropID]=[RockMaker].[dbo].[WellDrop].[ID]
  INNER JOIN [RockMaker].[dbo].[Well] 
  ON [RockMaker].[dbo].[WellDrop].[WellID]=[RockMaker].[dbo].[Well].[ID]
  INNER JOIN [RockMaker].[dbo].[CaptureProfileVersion]  
  ON [RockMaker].[dbo].[CaptureResult].[CaptureProfileVersionID]=[RockMaker].[dbo].[CaptureProfileVersion].[ID]
  INNER JOIN [RockMaker].[dbo].[CaptureProfile]
  ON [RockMaker].[dbo].[CaptureProfileVersion].[CaptureProfileID]=[RockMaker].[dbo].[CaptureProfile].[ID]
  INNER JOIN [RockMaker].[dbo].[ImageBatch] 
  ON [RockMaker].[dbo].[ImageBatch].[ID]=[RockMaker].[dbo].[CaptureResult].[ImageBatchID]
  INNER JOIN [RockMaker].[dbo].[ImagingTask]  
  ON [RockMaker].[dbo].[ImagingTask].[ID]=[RockMaker].[dbo].[ImageBatch].[ImagingTaskID]
  where 
  (PlateID={{PlateID}}) and ([RockMaker].[dbo].[CaptureProfile].[ID]={{CaptureProfileID}}) and (ABS(XCenter)+ABS(YCenter)>2)
  order by DateImaged ASC, RowLetter ASC, ColumnNumber ASC