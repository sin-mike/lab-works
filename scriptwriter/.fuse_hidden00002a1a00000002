#!/usr/bin/python
import matplotlib.pyplot as plt
from optparse import OptionParser
import csv
import ConfigParser
from datetime import datetime as dt
import os
import shutil
import datetime
import pprint
import scriptgenerator


def getCalibration(Config):
    print "configuration sections:",
    print Config.sections()
    xf1 = float(Config.get("FRAP", "x1"))
    yf1 = float(Config.get("FRAP", "y1"))
    xf2 = float(Config.get("FRAP", "x2"))
    yf2 = float(Config.get("FRAP", "y2"))
    xf3 = float(Config.get("FRAP", "x3"))
    yf3 = float(Config.get("FRAP", "y3"))

    xs1 = float(Config.get("SAXS", "x1"))
    ys1 = float(Config.get("SAXS", "y1"))
    xs2 = float(Config.get("SAXS", "x2"))
    ys2 = float(Config.get("SAXS", "y2"))
    xs3 = float(Config.get("SAXS", "x3"))
    ys3 = float(Config.get("SAXS", "y3"))

    a = -((-(xs2 * yf1) + xs3 * yf1 + xs1 * yf2
           - xs3 * yf2 - xs1 * yf3 + xs2 * yf3) /
          (xf2 * yf1 - xf3 * yf1 - xf1 * yf2
           + xf3 * yf2 + xf1 * yf3 - xf2 * yf3))
    b = -((xf2 * xs1 - xf3 * xs1 - xf1 * xs2
           + xf3 * xs2 + xf1 * xs3 - xf2 * xs3) /
          (-(xf2 * yf1) + xf3 * yf1 + xf1 * yf2
           - xf3 * yf2 - xf1 * yf3 + xf2 * yf3))
    c = -((-(xf3 * xs2 * yf1) + xf2 * xs3 * yf1
           + xf3 * xs1 * yf2 - xf1 * xs3 * yf2
           - xf2 * xs1 * yf3 + xf1 * xs2 * yf3) /
          (-(xf2 * yf1) + xf3 * yf1 + xf1 * yf2
           - xf3 * yf2 - xf1 * yf3 + xf2 * yf3))
    d = -((yf2 * ys1 - yf3 * ys1 - yf1 * ys2
           + yf3 * ys2 + yf1 * ys3 - yf2 * ys3) /
          (xf2 * yf1 - xf3 * yf1 - xf1 * yf2
           + xf3 * yf2 + xf1 * yf3 - xf2 * yf3))
    e = -((-(xf2 * ys1) + xf3 * ys1 + xf1 * ys2
           - xf3 * ys2 - xf1 * ys3 + xf2 * ys3) /
          (xf2 * yf1 - xf3 * yf1 - xf1 * yf2
           + xf3 * yf2 + xf1 * yf3 - xf2 * yf3))
    f = -((-(xf3 * yf2 * ys1) + xf2 * yf3 * ys1
           + xf3 * yf1 * ys2 - xf1 * yf3 * ys2
           - xf2 * yf1 * ys3 + xf1 * yf2 * ys3)
          / (xf2 * yf1 - xf3 * yf1 - xf1 * yf2
             + xf3 * yf2 + xf1 * yf3 - xf2 * yf3))

    def fx(x, y):
        return a * x + b * y + c

    def fy(x, y):
        return d * x + e * y + f
    return fx, fy


def sort(data, preference=None):
    if preference is None:
        preference = "SHORT_TRAVEL"

    def order(rw):
        row = ord(rw[0]) - 65
        col = int(rw[1:]) - 1

        if preference == "SHORT_TRAVEL":
            if row % 2 == 0:
                return row * 12 + col
            else:
                return (row + 1) * 12 - col
        elif preference == "ROW_FIRST":
            return row * 12 + col
        elif preference == "COLUMN_FIRST":
            return col * 8 + row

    print "sorting data for %s" % preference
    data = sorted(data, key=lambda d: order(d[0]))
    print "sorted data:"
    pprint.pprint(data)
    return data

if __name__ == '__main__':
    stime = dt.now()
    parser = OptionParser()
    parser.add_option("-o", "--output",
                      dest="output",
                      help="write output to FILE",
                      metavar="FILE")
    parser.add_option("-i", "--input",
                      dest="input",
                      help="take coordinates from FILE",
                      metavar="FILE")
    parser.add_option("-s", "--settings",
                      dest="settings",
                      help="take settings from FILE",
                      metavar="FILE")
    parser.add_option("-c", "--script",
                      dest="script",
                      help="write script to FILE",
                      metavar="FILE")
    parser.add_option("-p", "--plate",
                      dest="plateID",
                      help="specify the ID of plate to fetch",
                      metavar="INT")
    (opts, args) = parser.parse_args()

    if opts.settings is None:
        settingsPath = 'default.txt'
    else:
        settingsPath = opts.settings
    print("settings path is %s" % settingsPath)
    Config = ConfigParser.ConfigParser()
    Config.read(settingsPath)

    if opts.input is None and opts.plateID is None:
        print("no input file specified")
        exit(-1)

    if opts.input is not None and opts.plateID is not None:
        print("both input file and plateID specified")
        exit(-1)

    if opts.input is not None:
        plateID = None
        print("processing data for %s" % opts.input)

    if opts.plateID is not None:
        plateID = int(opts.plateID)
        print("processing data for plate %d" % plateID)

    if opts.output is None:
        outpath = "coordinates.csv"
        print("output file is %s" % outpath)

    if opts.script is None:
        if opts.input is not None:
            postfix = os.path.split(opts.input)[-1]
            postfix = os.path.splitext(postfix)[0]
        elif opts.plateID is not None:
            postfix = "plateID " + str(opts.plateID)
        else:
            postFix = ''

        scriptName = (str(dt.now().strftime('%Y%m%d'))
                      + "_" + Config.get("Script", "name")
                      + "_" + postfix + "_autowriter.dat")

        destinationFolder = Config.get("General", "scriptLocalFolder")
        if destinationFolder.startswith('.'):
            destinationFolder = os.path.join(
                os.path.dirname(os.path.abspath(__file__)),
                destinationFolder
            )

        scriptPath = os.path.join(destinationFolder, scriptName)
        print("script file is %s" % scriptPath)
    else:
        scriptPath = opts.script

    fx, fy = getCalibration(Config)

    scale = float(Config.get("General", "scale"))
    print("scale = %f" % scale)
    repeat = int(Config.get("General", "repeat"))
    print("repeat = %d" % repeat)

    fig = plt.figure()
    plt.axis("equal")
    plt.axis([-20, 120, -75, 10])

    if plateID is None:
        try:
            plateID = int(Config.get("General", "plateID"))
        except Exception, e:
            print "Config PlateID unknown: %s" % e
            print "Please enter plateID:",
            plateID = int(raw_input())

    ax = float(Config.get("FRAP", "ax"))
    ay = float(Config.get("FRAP", "ay"))

    print("FRAP steps ax = %f, ay = %f" % (ax, ay))

    coordSAXS = []
    alphabet = {"A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "H": 8}

    if opts.input is not None:
        with open(opts.input, 'r') as csvfile:
            coordFRAP = csv.reader(csvfile, delimiter=',')
        coordFRAP = [(c[0], float(c[1]), float(c[2]))
                     for c in coordFRAP
                     if len(c[0]) > 0 and len(c[1]) > 0 and len(c[2]) > 0]

    elif opts.plateID is not None:
        import sqlreader
        sql = sqlreader.SQLReader(
            ip=Config.get("SQL", "ip"),
            login=Config.get("SQL", "login"),
            password=Config.get("SQL", "password")
        )
        coordFRAP = sql.getPlateCoordinates(plateID=plateID)

    # calibration
    for row in coordFRAP:
        rw = row[0]
        if len(rw) == 2:
            pos = (int(rw[1]) - 1, alphabet[rw[0]] - 1)
        else:
            pos = (int(rw[1]) * 10 + int(rw[2]) - 1, alphabet[rw[0]] - 1)
        abspos = (float(row[1]) * scale + ax * pos[0],
                  float(row[2]) * scale - ay * pos[1])
        coordSAXS.append([row[0],
                          fx(abspos[0], abspos[1]),
                          fy(abspos[0], abspos[1])])
        circle = plt.Circle(abspos, .2, color='r', clip_on=True)
        fig.gca().add_artist(circle)

    # postprocessing of data
    for row in coordSAXS:
        row[1] = round(row[1], 3)
        row[2] = round(row[2], 3)
    coordSAXS = sort(coordSAXS, preference="SHORT_TRAVEL")

    # writing graphics canvas
    for i in range(8):
        for j in range(12):
            circle = plt.Circle((j * ax, -i * ay),
                                2.5, edgecolor="k",
                                facecolor="None",
                                clip_on=True)
            fig.gca().add_artist(circle)
    
    # presenting graphics
    fig.show()
    fig.savefig("out.png", dpi=220)

    print("absolute position for SAXS:")
    pprint.pprint(coordSAXS)

    # generating script here
    scriptSettings = {
        "user_name": Config.get("Script", "name"),
        "experiment_name": postfix,
        "exposure": Config.get("Script", "livetime"),
        "repeat": Config.get("Script", "repeat")
    }
    scrGen = scriptgenerator.ScriptGenerator(scriptSettings)
    script = scrGen.generateScript(coordSAXS)
    with open(scriptPath, 'w') as scriptFile:
        scriptFile.write(script)

    # Copying script file to destination folder here:
    try:
        shutil.copyfile(scriptPath, os.path.join(
            str(Config.get("General", "scriptDestinationFolder")), scriptName))
        print "script file %s was successfully copied to %s" % (
            scriptPath, Config.get("General", "scriptDestinationFolder"))
    except Exception, e:
        print "ERROR: unable to copy file to destination folder:",
        print e

    # some postinformation
    print "finished, time elapsed %s" % str(dt.now() - stime)
    print "estimated script timerun",
    print str(datetime.timedelta(seconds=scrGen.estimatedRunTime))
    raw_input("Press any key")
