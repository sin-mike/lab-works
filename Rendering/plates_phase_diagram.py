import csv
import diag
from operator import itemgetter
import numpy as np

data_screens = csv.reader(open("/home/leonid/data/amph/screens.csv", 'rb'))
data_out = csv.reader(open("/home/leonid/data/out.csv", 'rb'))
data_compound = csv.reader(open("/home/leonid/data/amph/compound.csv", 'rb'))

data_compound.next()
compound = dict((int(r[1]), [float(r[12]), float(r[7])])
                for r in data_compound if r[1] != '')


def well(s):
    return (ord(s[0]) - 65) * 12 + int(s[1:]) - 1

arr = []


def screen_mapper(r):
    comp2 = ''
    if r[3] != '':
        comp2 = r[3] + ' ' + r[4] + '; '
    if r[2] == 'Na/K phosphate' and float(r[1]) <= 2. and float(r[1]) >= 1. and float(r[5]) <= 6. and float(r[5]) >= 5.2:
        arr.append(well(r[0]))
    return well(r[0]), r[1] + ' ' + r[2] + '; ' + comp2 + 'ph' + r[5]

data_screens.next()
screens = dict(screen_mapper(r) for r in data_screens)

results = [[] for i in range(96)]

phase = {'Pn3m': 0, 'Ia3d': 1, 'Im3m': 2,
         'La': 3, 'Sponge': 4, 'H': 5, 'Flat': 6}

cur = ['', '']
for row in data_out:
    if row[1] != cur[1]:
        cur = row
        info = cur[1].split()
        if info[0] != 'Mike' and info[0] != 'PHASE_TRANSITION' and cur[3] != 'H' and cur[3] != 'Flat':
            try:
                if int(info[3]) not in [946, 943, 945, 935]:
                    results[well(info[0])].append(
                        (compound[int(info[3])], phase[cur[3]]))
            except:
                pass


for i in range(96):
    # if i in arr:
    print 'processing well #' + repr(i + 1)
    X = []
    y = []
    for r in results[i]:
        X.append(r[0])
        y.append(r[1])
    diag.render(np.array(X), np.array(y), screens[
                i], '/home/leonid/phase diagrams/' + 'well ' + repr(i + 1) + '.png', font='large')
