import csv
import diag
from operator import itemgetter
import numpy as np


def render(fn1, fn2, title, fout):
    data_1 = csv.reader(open(fn1, 'rb'))
    data_2 = csv.reader(open(fn2, 'rb'))
    data_1.next()

    phases = {'Pn3m': 0,
              'Ia3d': 1,
              'Im3m': 2,
              'La': 3,
              'Sponge': 4,
              'H': 5,
              'Flat': 6}

    def conv1(a):
        return a[0], float(a[21]), float(a[20])

    def conv2(a):
        return a[1].split()[2], phases[a[2]]

    data1 = map(conv1, data_1)
    data2 = map(conv2, data_2)

    def filterr(a, out):
        if len(a) > 0:
            p = a[0][0]
            b = [i for i in a if i[0] == p]
            for i in b:
                a.remove(i)
            ph = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}
            for i in b:
                ph[i[1]] += 1
            b = sorted(ph.items(), key=itemgetter(1), reverse=True)
            phases = [i[0] for i in b if i[1] == b[0][1]]
            for q in ph.keys():
                if q in phases:
                    out.append((p, q))
                    filterr(a, out)
                    break

    data = []
    filterr(data2, data)

    X = []
    y = []

    for i in data1:
        for j in data:
            if j[0] == i[0]:
                y.append(j[1])
                X.append([i[1], i[2]])
                break

    diag.render(np.array(X), np.array(y), title, fout)

if __name__ == '__main__':
    render("/home/leonid/data/PCR Tubes/2014_10_13 PCR AMP-OG protocol - Sheet1.csv",
           "/home/leonid/data/PCR Tubes/output_after_precipitant_corrected.csv",
           "PCR 1.5M prec",
           "/home/leonid/phase diagrams/pcr_after.png")

    render("/home/leonid/data/PCR Tubes/2014_10_13 PCR AMP-OG protocol - Sheet1.csv",
           "/home/leonid/data/PCR Tubes/output_before_precipitant_corrected.csv",
           "PCR 0.02M prec",
           "/home/leonid/phase diagrams/pcr_before.png")
