import numpy as np
from matplotlib import pyplot
from phase_diagram_render_2d import phase_diagram_render_2d
from phase_diagram_render_2d import mergeOutput
from phase_diagram_render_2d import resampleLabel

if __name__ == '__main__':
    import csv
    import pprint
    outputPath = "../test/leo/Leo PCR 1st part before precipitant.csv"
    sampleDescriptionPath = "../test/leo/2014_10_13 PCR AMP-OG protocol - Sheet2.csv"

    with open(outputPath, 'r') as f:
        output = list(csv.reader(f))

    with open(sampleDescriptionPath, 'r') as f:
        sampleDescription = list(csv.reader(f))

    c1, p1, l1 = mergeOutput(
        output, sampleDescription, sampleKey="SAMPLE")

    outputPath = "../test/leo/Leo PCR 2nd part before precipitant.csv"
    sampleDescriptionPath = "../test/leo/2014_11_30 PCR AMP-OG protocol - Sheet2.csv"

    with open(outputPath, 'r') as f:
        output = list(csv.reader(f))

    with open(sampleDescriptionPath, 'r') as f:
        sampleDescription = list(csv.reader(f))

    c2, p2, l2 = mergeOutput(
        output, sampleDescription)

    coords = c1 + c2
    phases = p1 + p2
    lattices = l1 + l2

    resamplingNumber = 10
    coords, phases = resampleLabel(coords, phases, num=resamplingNumber)

    pprint.pprint(zip(coords, phases))

    phaseDiag = phase_diagram_render_2d(X=np.array(coords),
                                        labels=phases,
                                        n_neighbors=9 * resamplingNumber,
                                        xlabel=r"OG/MO, % w/w",
                                        ylabel=r"AMP/MO, % w/w",
                                        scatter=True,
                                        sigma=0.2)

    pyplot.show(phaseDiag)
