import csv
import diag
from operator import itemgetter
import numpy as np
import matplotlib
import matplotlib.pyplot as pyplot
from matplotlib.colors import ListedColormap
from sklearn import neighbors, datasets
import numpy as np
from math import exp, sqrt, pi

s = 4.


def foo(a):
    res = []
    for i in range(len(a)):
        res.append([])
        for j in range(len(a[i])):
            res[i].append(
                exp(-a[i][j] ** 2 / (2 * s ** 2)) / (s * sqrt(2 * pi)))
    return np.array(res)


def render(fn1, fn2, title, fout):
    data_1 = csv.reader(open(fn1, 'rb'))
    data_2 = csv.reader(open(fn2, 'rb'))
    data_1.next()

    phases = {'Pn3m': 0,
              'Ia3d': 1,
              'Im3m': 2,
              'La': 3,
              'Sponge': 4,
              'H': 5,
              'Flat': 6}

    def conv1(a):
        return a[0], float(a[21]), float(a[20])

    def conv2(a):
        return a[1].split()[2], phases[a[2]], float(a[3])

    data1 = map(conv1, data_1)
    data2 = map(conv2, data_2)

    def filterr(a, out):
        if len(a) > 0:
            p = a[0][0]
            b = [i for i in a if i[0] == p]
            for i in b:
                a.remove(i)
            ph = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}
            for i in b:
                ph[i[1]] += 1
            z = sorted(ph.items(), key=itemgetter(1), reverse=True)
            phases = [i[0] for i in z if i[1] == z[0][1]]
            for q in ph.keys():
                if q in phases:
                    mm = 0.
                    for j in b:
                        if j[1] == q and j[2] > mm:
                            mm = j[2]
                    out.append((p, mm))
                    filterr(a, out)
                    break

    data = []
    filterr(data2, data)

    X = []
    y = []

    for i in data1:
        for j in data:
            if j[0] == i[0]:
                y.append(j[1])
                X.append([i[1], i[2]])
                break
    X = np.array(X)
    y = np.array(y)
    font = 'large'
    n_neighbors = 5
    weights = 'uniform'

    h = 0.1

    # we create an instance of Neighbors Classifier and fit the data.
    clf = neighbors.KNeighborsRegressor(n_neighbors, foo)
    clf.fit(X, y)

    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
    x_min, x_max = X[:, 0].min(), X[:, 0].max()
    y_min, y_max = X[:, 1].min(), X[:, 1].max()
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)

    fig = pyplot.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)

    #box = ax.get_position()
    #ax.set_position([0.3, 0.4, box.width*0.3, box.height])
    # you can set the position manually, with setting left,buttom, witdh, hight of the axis
    # object
    ax.set_position([0.1, 0.1, 0.7, 0.8])

    qqq = ax.pcolormesh(xx, yy, Z, cmap=matplotlib.cm.Blues)
    ax.grid()
    ax.scatter(X[:, 0], X[:, 1], c=y, cmap=matplotlib.cm.Blues, s=80)
    ax.set_xlim(x_min - (x_max - x_min) * 0.1, x_max + (x_max - x_min) * 0.1)
    ax.set_ylim(y_min - (y_max - y_min) * 0.1, y_max + (y_max - y_min) * 0.1)
    ax.set_title("%s (k=%i, sig=%d)" %
                 (title, n_neighbors, int(s)), size=font, weight='heavy')
    ax.set_ylabel("AMP/MO, %", size='large')
    ax.set_xlabel("OG/MO, %", size='large')
    cbar = fig.colorbar(qqq, ticks=[50, 60, 70, 80, 90, 100, 110, 120, 130])
    fig.savefig(fout)
    pyplot.close(fig)


render("/home/leonid/data/PCR Tubes/2014_10_13 PCR AMP-OG protocol - Sheet1.csv",
       "/home/leonid/data/PCR Tubes/output_after_precipitant_corrected.csv",
       "PCR 1.5M prec",
       "/home/leonid/phase diagrams/pcr_after_2.png")
render("/home/leonid/data/PCR Tubes/2014_10_13 PCR AMP-OG protocol - Sheet1.csv",
       "/home/leonid/data/PCR Tubes/output_before_precipitant_corrected.csv",
       "PCR 0.02M prec",
       "/home/leonid/phase diagrams/pcr_before_2.png")
