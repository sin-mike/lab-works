'''
Author: Leonid Vlasenkov
Mail:   leo.vlasenkov@gmail.com
Date:   Sep 9 2014

'''

import csv
import matplotlib.pyplot as plt

def convert(list):
	return (float(list[2]), float(list[3]), list[4], list[8], float(list[7]))

phases = ['Im3m', 'Pn3m', 'La', 'Ia3d', 'Sponge']
clrs = ['red', 'green', 'blue', 'cyan', 'yellow']
colors = dict(zip(phases, clrs))
	
def render(data, salt,ph):
	points = dict(zip(phases, [([],[]), ([],[]), ([],[]), ([],[]), ([],[])]))
	for row in data:
		if row[2] == salt and row[4] == ph:
			points[row[3]][0].append(row[0])
			points[row[3]][1].append(row[1])
	plt.axis([-5, 100, 0, 3])
	plt.ylabel('Conc')
	plt.xlabel('HTAC')
	plt.title(salt)
	for p in phases:
		plt.scatter(points[p][0], points[p][1], c=colors[p], s=300, label=p, alpha=1, edgecolors='none')
	#plt.legend(bbox_to_anchor=(1, 1), loc=2)
	plt.grid(True)

def render_all(data,ph):
	points = dict(zip(phases, [([],[]), ([],[]), ([],[]), ([],[]), ([],[])]))
	for row in data:
		if row[4] == ph:
			points[row[3]][0].append(row[0])
			points[row[3]][1].append(row[1])
	plt.axis([-5, 100, 0, 3])
	plt.ylabel('Conc')
	plt.xlabel('HTAC')
	plt.title('All')
	for p in phases:
		plt.scatter(points[p][0], points[p][1], c=colors[p], s=300, label=p, alpha=1, edgecolors='none')
	plt.grid(True)


with open('data.csv', 'rb') as input:
	data_reader = csv.reader(input)
	data = [convert(row) for row in data_reader]
	phs = []
	for row in data:
		if not (row[4] in phs):
			phs.append(row[4])
	for ph in phs:	
		plt.clf()		
		plt.subplot(2,2,1)
		render(data, "Na/K phosphate", ph)
		plt.legend(bbox_to_anchor=(1, 1), loc=2)
		plt.subplot(2,2,2) 
		render(data, "Ammonium phosphate", ph)
		plt.subplot(2,2,3)
		render(data, "Ammonium sulfate",ph)
		plt.subplot(2,2,4)
		render_all(data,ph)
		plt.tight_layout()
		plt.savefig(repr(ph)+'.png')