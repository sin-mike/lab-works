                            OLS Regression Results                            
==============================================================================
Dep. Variable:            Cryst 274 d   R-squared:                       0.448
Model:                            OLS   Adj. R-squared:                  0.417
Method:                 Least Squares   F-statistic:                     14.41
Date:                Fri, 12 Sep 2014   Prob (F-statistic):           1.16e-08
Time:                        17:50:15   Log-Likelihood:                -397.21
No. Observations:                  76   AIC:                             804.4
Df Residuals:                      71   BIC:                             816.1
Df Model:                           4                                         
==============================================================================
                 coef    std err          t      P>|t|      [95.0% Conf. Int.]
------------------------------------------------------------------------------
const        -18.9471     49.660     -0.382      0.704      -117.966    80.072
Fraction       5.3465     20.352      0.263      0.794       -35.233    45.926
OG             3.5585      2.585      1.376      0.173        -1.596     8.713
salt         -69.4347     23.891     -2.906      0.005      -117.072   -21.798
68d-d          1.3239      0.546      2.423      0.018         0.235     2.413
==============================================================================
Omnibus:                       15.368   Durbin-Watson:                   1.675
Prob(Omnibus):                  0.000   Jarque-Bera (JB):               33.157
Skew:                           0.619   Prob(JB):                     6.31e-08
Kurtosis:                       5.990   Cond. No.                     1.03e+03
==============================================================================

Warnings:
[1] The condition number is large, 1.03e+03. This might indicate that there are
strong multicollinearity or other numerical problems.




                            OLS Regression Results                            
==============================================================================
Dep. Variable:            Cryst 274 d   R-squared:                       0.465
Model:                            OLS   Adj. R-squared:                  0.434
Method:                 Least Squares   F-statistic:                     15.40
Date:                Fri, 12 Sep 2014   Prob (F-statistic):           4.09e-09
Time:                        17:50:15   Log-Likelihood:                -396.06
No. Observations:                  76   AIC:                             802.1
Df Residuals:                      71   BIC:                             813.8
Df Model:                           4                                         
==============================================================================
                 coef    std err          t      P>|t|      [95.0% Conf. Int.]
------------------------------------------------------------------------------
const        342.5483    113.930      3.007      0.004       115.379   569.718
Fraction      -6.8989     21.682     -0.318      0.751       -50.131    36.333
OG             1.4409      2.910      0.495      0.622        -4.361     7.243
salt         -56.2155     25.170     -2.233      0.029      -106.404    -6.027
1/d68      -1.974e+04   6877.025     -2.870      0.005     -3.35e+04 -6025.364
==============================================================================
Omnibus:                       20.379   Durbin-Watson:                   1.715
Prob(Omnibus):                  0.000   Jarque-Bera (JB):               49.485
Skew:                           0.819   Prob(JB):                     1.80e-11
Kurtosis:                       6.597   Cond. No.                     1.15e+04
==============================================================================

Warnings:
[1] The condition number is large, 1.15e+04. This might indicate that there are
strong multicollinearity or other numerical problems.




                            OLS Regression Results                            
==============================================================================
Dep. Variable:            Cryst 274 d   R-squared:                       0.466
Model:                            OLS   Adj. R-squared:                  0.436
Method:                 Least Squares   F-statistic:                     15.47
Date:                Fri, 12 Sep 2014   Prob (F-statistic):           3.79e-09
Time:                        17:50:15   Log-Likelihood:                -395.97
No. Observations:                  76   AIC:                             801.9
Df Residuals:                      71   BIC:                             813.6
Df Model:                           4                                         
==============================================================================
                 coef    std err          t      P>|t|      [95.0% Conf. Int.]
------------------------------------------------------------------------------
const        257.1916     86.362      2.978      0.004        84.990   429.393
Fraction      -8.6738     22.015     -0.394      0.695       -52.570    35.223
OG             0.9108      3.038      0.300      0.765        -5.146     6.968
salt         -53.4319     25.680     -2.081      0.041      -104.636    -2.228
1/(d68)2   -1.047e+06   3.61e+05     -2.901      0.005     -1.77e+06 -3.27e+05
==============================================================================
Omnibus:                       24.495   Durbin-Watson:                   1.732
Prob(Omnibus):                  0.000   Jarque-Bera (JB):               63.247
Skew:                           0.991   Prob(JB):                     1.85e-14
Kurtosis:                       7.006   Cond. No.                     6.04e+05
==============================================================================

Warnings:
[1] The condition number is large, 6.04e+05. This might indicate that there are
strong multicollinearity or other numerical problems.




