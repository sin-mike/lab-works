import csv
import numpy
from scipy.ndimage import gaussian_filter1d


def read1d(path,
           forced2columns=False,
           sigma=None,
           boundaryBox=None,
           resampleNumber=None):
    data = None

    with open(path, "r") as f:
        data = []
        reader = csv.reader(f)

        for row in reader:
            irow = [float(r) for r in row]
            data.append(irow)
        data = numpy.array(data)

    # this stands for smoothing of curve
    if sigma:
        data = numpy.transpose(data)
        data[1] = gaussian_filter1d(data[1], sigma)
        data = numpy.transpose(data)

    # in case we have more than two columns for data, e.g. (x, y, y_err)
    if forced2columns:
        data = data[:, :2]

    # in case we want to get data only in certain range
    if boundaryBox:
        qmin = boundaryBox[0]
        qmax = boundaryBox[1]
        data = [d for d in data if d[0] >= qmin and d[0] <= qmax]

    # in case we want to change number of points in data
    if resampleNumber:

        raise NotImplementedError

    return data


def main():
    pass


if __name__ == '__main__':
    main()
